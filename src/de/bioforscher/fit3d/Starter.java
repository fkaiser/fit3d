/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.biojava.bio.structure.StructureException;

import com.google.common.collect.Lists;

import de.bioforscher.fit3d.classification.ClassifierException;
import de.bioforscher.fit3d.classification.HitClassifier;
import de.bioforscher.fit3d.classification.HitEcClassifier;
import de.bioforscher.fit3d.classification.HitPfamClassifier;
import de.bioforscher.fit3d.classification.utils.AsaStorage;
import de.bioforscher.fit3d.clustering.ClusterException;
import de.bioforscher.fit3d.clustering.HitClusterer;
import de.bioforscher.fit3d.core.MotifSearch;
import de.bioforscher.fit3d.io.ResultHandler;
import de.bioforscher.fit3d.io.ResultPrinter;
import de.bioforscher.fit3d.io.parser.InputListParser;
import de.bioforscher.fit3d.io.session.SessionSaver;
import de.bioforscher.fit3d.io.utils.InputOutputUtils;
import de.bioforscher.fit3d.mapping.utils.EcStorage;
import de.bioforscher.fit3d.mapping.utils.PfamStorage;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.statistics.FofanovModel;
import de.bioforscher.fit3d.statistics.StarkModel;
import de.bioforscher.fit3d.statistics.StatisticalModelException;
import de.bioforscher.fit3d.utils.MotifExtractor;
import de.bioforscher.fit3d.utils.MotifExtractorException;

public class Starter {

	// global constants
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH-mm");

	public static final DateFormat LOG_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static final int LIST_PARTITION = 1000;

	public static final Locale LOCALIZATION = Locale.US;

	public static final String NEWLINE = System.lineSeparator();
	public static final String[] PDB_ATOMS = new String[] { " N  ", " ND ", " ND1", " ND2", " ND3", " NE ", " NE1",
			" NE2", " NE3", " NH ", " NH1", " NH2", " NZ ", " C  ", " CA ", " CB ", " CG ", " CG1", " CG2", " CG3",
			" CD ", " CD1", " CD2", " CD3", " CE ", " CE1", " CE2", " CE3", " CZ ", " CZ1", " CZ2", " CZ3", " CH ",
			" CH1", " CH2", " CH3", " O  ", " OD ", " OD1", " OD2", " OD3", " OE ", " OE1", " OE2", " OE3", " OG ",
			" OG1", " OG2", " OG3", " OH ", " S  ", " SA ", " SB ", " SD ", " SG " };
	public static final long TIMESTAMP = System.currentTimeMillis();
	public static final String TMP_DIR = System.getProperty("java.io.tmpdir");
	public static final SessionSaver SESSION_SAVER = new SessionSaver();

	public static final double SIGNIFICANCE_CUTOFF = 0.001;

	// initialize logger as final variable
	public static final Logger LOG = initLogger();

	public static void main(String[] args) {

		// set default localization
		Locale.setDefault(LOCALIZATION);

		// override output streams
		overrideOutputStreams();

		System.out.println(NEWLINE
				+ "__/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\_________________________/\\\\\\\\\\\\\\\\\\\\___/\\\\\\\\\\\\\\\\\\\\\\\\____"
				+ NEWLINE
				+ "_\\/\\\\\\///////////________________________/\\\\\\///////\\\\\\_\\/\\\\\\////////\\\\\\__"
				+ NEWLINE
				+ " _\\/\\\\\\______________/\\\\\\_____/\\\\\\_______\\///______/\\\\\\__\\/\\\\\\______\\//\\\\\\_"
				+ NEWLINE
				+ "  _\\/\\\\\\\\\\\\\\\\\\\\\\_____\\///___/\\\\\\\\\\\\\\\\\\\\\\_________/\\\\\\//___\\/\\\\\\_______\\/\\\\\\_"
				+ NEWLINE
				+ "   _\\/\\\\\\///////_______/\\\\\\_\\////\\\\\\////_________\\////\\\\\\__\\/\\\\\\_______\\/\\\\\\_"
				+ NEWLINE
				+ "    _\\/\\\\\\_____________\\/\\\\\\____\\/\\\\\\________________\\//\\\\\\_\\/\\\\\\_______\\/\\\\\\_"
				+ NEWLINE
				+ "     _\\/\\\\\\_____________\\/\\\\\\____\\/\\\\\\_/\\\\___/\\\\\\______/\\\\\\__\\/\\\\\\_______/\\\\\\__"
				+ NEWLINE
				+ "      _\\/\\\\\\_____________\\/\\\\\\____\\//\\\\\\\\\\___\\///\\\\\\\\\\\\\\\\\\/___\\/\\\\\\\\\\\\\\\\\\\\\\\\/___"
				+ NEWLINE + "       _\\///______________\\///______\\/////______\\/////////_____\\////////////_____"
				+ NEWLINE + "               Copyright (C) 2013-2015 bioinformatics group Mittweida" + NEWLINE);

		// get session restore options
		final Options restoreOptions = OptionFactory.INSTANCE.getRestoreOptions();

		// get Fit3D options
		final Options clOptions = OptionFactory.INSTANCE.getClOptions();
		// add input option group
		clOptions.addOptionGroup(OptionFactory.INSTANCE.getInputOptions());
		// add target option group
		clOptions.addOptionGroup(OptionFactory.INSTANCE.getTargetOptions());
		// add verbosity option group
		clOptions.addOptionGroup(OptionFactory.INSTANCE.getVerbosityOptions());

		try {

			// parse command line with restore options and ignore unknown
			// options
			final CommandLineParser clp = new BasicParser();
			CommandLine cl = clp.parse(restoreOptions, args, true);

			// restore session
			if (cl.hasOption('R')) {

				final String sessionFilePath = cl.getOptionValue('R');
				LOG.info("trying to restore session " + sessionFilePath);

				// initialize parameters from session
				ParameterProvider.initializeFromSession(sessionFilePath);

				LOG.info("restored options: "
						+ InputOutputUtils.getFormattedCommandline(ParameterProvider.getInstance().getCl()));

			} else {

				// parse command line with standard options and do not
				// ignore
				// missing options
				cl = clp.parse(clOptions, args);
			}

			// only use a defined subset of the input structure (extract motif
			// functionality)<
			if (cl.hasOption('X')) {

				// initialize parameters from command line
				ParameterProvider.initializeFromCommandLine(cl, false);

				String inputFile = ParameterProvider.getInstance().getMotif();
				String extractString = ParameterProvider.getInstance().getExtractMotifString();

				LOG.info("trying to extract motif " + extractString + " from file " + inputFile);

				MotifExtractor me = new MotifExtractor(inputFile, extractString);

				// extract motif
				me.extract();

				// use the extracted motif as input
				String extractedMotifFileName = me.getExtractedMotifFileName();
				LOG.info("extracted motif " + extractedMotifFileName + " will be used as new input");
				ParameterProvider.getInstance().setMotif(extractedMotifFileName);

				// initialize exchanges again
				ParameterProvider.getInstance().reinitializeExchanges();

			} else if (!cl.hasOption('R')) {

				ParameterProvider.initializeFromCommandLine(cl, true);
			}

			if (!cl.hasOption('R')) {

				LOG.info("options: "
						+ InputOutputUtils.getFormattedCommandline(ParameterProvider.getInstance().getCl()));
			}

			// add shutdown hook
			Runtime.getRuntime().addShutdownHook(SESSION_SAVER);

			// run
			runWithParameters();

		} catch (ParseException e) {

			if (!e.getMessage().equals("")) {

				LOG.warning(e.getMessage() + NEWLINE);
			}

			final HelpFormatter help = new HelpFormatter();
			help.setWidth(128);
			help.setSyntaxPrefix("usage: ");
			help.printHelp("java -jar Fit3D.jar -m <arg> [-t <arg> | -l <arg>] [OPTIONS]" + NEWLINE, clOptions);
			System.out.println(NEWLINE + "* = required");
			System.out.println("** = one of these required");

		} catch (NumberFormatException e) {

			LOG.severe(e.getMessage());
		} catch (FileNotFoundException e) {

			LOG.severe(e.getMessage());
		} catch (IOException e) {

			LOG.severe(e.getMessage());
		} catch (StructureException e) {

			LOG.severe(e.getMessage());
		} catch (InterruptedException e) {

			LOG.severe(e.getMessage());
		} catch (ClassNotFoundException e) {

			LOG.severe(e.getMessage());
		} catch (ExecutionException e) {

			LOG.severe(e.getMessage());
		} catch (MotifExtractorException e) {

			LOG.severe(e.getMessage());
		} finally {

			Runtime.getRuntime().removeShutdownHook(SESSION_SAVER);
		}
	}

	/**
	 * initialize logger
	 * 
	 * @return instance of logger
	 */
	private static Logger initLogger() {

		Logger log = Logger.getLogger("de.bioforscher.fit3d");

		// set standard log level
		log.setLevel(Level.INFO);

		log.setUseParentHandlers(false);

		log.addHandler(new Handler() {
			private BufferedWriter writer;

			@Override
			public void close() throws SecurityException {
				try {
					if (this.writer != null) {

						this.writer.close();
					}
				} catch (Exception e) {
				}
			}

			@Override
			public void flush() {
			}

			@Override
			public void publish(LogRecord record) {
				synchronized (LOG) {
					if (record.getLevel().intValue() > Level.WARNING.intValue()) {
						addLogEntry(record);
						System.err.print(getFormattedLogRecord(record));
					} else {
						System.out.print(getFormattedLogRecord(record));
					}
				}
			}

			private synchronized void addLogEntry(LogRecord record) {
				if (this.writer == null) {
					try {
						this.writer = new BufferedWriter(new FileWriter("Fit3D.err", true));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				try {
					this.writer.write(getFormattedLogRecord(record));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			private String getFormattedLogRecord(LogRecord record) {
				StringBuilder builderRecord = new StringBuilder();

				builderRecord.append(LOG_DATE_FORMAT.format(new Date(record.getMillis())) + " ");

				if (record.getLevel().intValue() == Level.SEVERE.intValue()) {

					builderRecord.append("[" + record.getSourceClassName() + "#" + record.getSourceMethodName() + "] ");
				}

				builderRecord.append(record.getLevel() + ": " + record.getMessage());

				if (record.getThrown() != null) {
					builderRecord.append(NEWLINE);
					for (StackTraceElement entry : record.getThrown().getStackTrace()) {
						builderRecord.append("\t" + entry.toString() + NEWLINE);
					}
				}

				builderRecord.append(NEWLINE);

				return builderRecord.toString();
			}
		});

		return log;
	}

	/**
	 * overrides standard output streams
	 */
	private static void overrideOutputStreams() {

		// override error stream to suppress BioJava output
		System.setErr(new PrintStream(System.err) {

			// 2013-08-06 12:56:11 WARNING: anything
			private final Pattern p = Pattern
					.compile("\\d{1,4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}:?\\d{0,2}\\s[A-Z]+:\\s.*");

			@Override
			public void println(Object x) {
				println(x.toString());
			}

			@Override
			public void println(String x) {
				if (this.p.matcher(x).matches()) {
					print(x);
					println();
				}
			}
		});
	}

	/**
	 * initiates a Fit3D search with parameters
	 * 
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws ClassNotFoundException
	 * @throws ParseException
	 */
	private static void runWithParameters() throws InterruptedException, FileNotFoundException, IOException,
			ExecutionException, ClassNotFoundException, ParseException {

		// TODO print normalized scales
		// for (double d : ClassifierUtils
		// .normalizeScale(Scales.DataValues.BURIED_ROSE)) {
		// System.out.printf("%1.4f & ", d);
		// }
		// System.out.println();

		// parse target or target list
		List<String> targets = new ArrayList<>();
		if (ParameterProvider.getInstance().isListTarget()) {

			final InputListParser lp = new InputListParser(new File(ParameterProvider.getInstance().getList()));
			targets = lp.readInput();

		} else {

			targets.add(ParameterProvider.getInstance().getTarget());
		}

		// initial output
		LOG.info("initialized with " + ParameterProvider.getInstance().getMotif());
		if (ParameterProvider.getInstance().isListTarget()) {

			if (targets.size() == 0) {

				throw new ParseException("target list seems to be empty, wrong format?");
			}

			LOG.info("target list " + ParameterProvider.getInstance().getList() + " contains " + targets.size()
					+ " structures");

		} else {

			LOG.info("target is " + ParameterProvider.getInstance().getTarget());
		}

		LOG.info("maximal RMSD set to " + ParameterProvider.getInstance().getMaxRmsd() + " \u212B");

		// generate sub lists
		final List<List<String>> targetLists = Lists.partition(targets, LIST_PARTITION);

		// initialize percent format for progress
		final NumberFormat percentFormat = NumberFormat.getPercentInstance(Starter.LOCALIZATION);
		percentFormat.setMinimumFractionDigits(2);
		percentFormat.setMaximumFractionDigits(2);

		// initialize average speed format
		final NumberFormat nf = NumberFormat.getInstance(Starter.LOCALIZATION);
		final DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern("0.00");

		// initialize result saver
		final ResultHandler rh = new ResultHandler();
		// set last sublist position
		rh.setLastSubList(ParameterProvider.getInstance().getSublistPos());

		// thread pool executor
		final ExecutorService executor = Executors.newFixedThreadPool(ParameterProvider.getInstance().getCores());

		// initialize counter for sublist position
		int sublistCounter = ParameterProvider.getInstance().getSublistPos();
		// total time storage
		long totalTime = ParameterProvider.getInstance().getTotalTime();

		// process sub lists
		for (int i = ParameterProvider.getInstance().getSublistPos(); i < targetLists.size(); i++) {

			// store current sublist
			final List<String> sublist = targetLists.get(i);

			// measure start time
			final long startTime = System.nanoTime();

			// initialize storage for jobs
			final List<MotifSearch> jobs = new ArrayList<>();

			// generate jobs
			for (String target : sublist) {

				// initialize motif search
				jobs.add(new MotifSearch(new String(ParameterProvider.getInstance().getMotif()), new String(target),
						new ArrayList<Map<Character, Integer>>(
								ParameterProvider.getInstance().getExchangeDistributions()),
						new HashMap<String, Double>(ParameterProvider.getInstance().getMotifDistanceMap())));
			}

			// execute jobs
			final List<Future<List<Hit>>> results = executor.invokeAll(jobs);

			// get current hits
			final List<Hit> currentHits = new ArrayList<>();
			for (Future<List<Hit>> allHits : results) {

				final Iterator<Hit> it = allHits.get().iterator();

				while (it.hasNext()) {

					final Hit hit = it.next();
					currentHits.add(hit);
				}
			}

			// save current hits
			rh.write(currentHits, sublistCounter);

			// do not print progress if less targets than LIST_PARTITION
			if (targets.size() < LIST_PARTITION) {

				continue;
			}

			// update GS
			ParameterProvider.getInstance().setGs(FofanovModel.getInstance().getGs());
			// update NS
			ParameterProvider.getInstance().setNs(FofanovModel.getInstance().getNs());

			// calculate estimated time
			final long estimatedTime = System.nanoTime() - startTime;

			// accumulate total time
			totalTime += estimatedTime;
			// update total time
			ParameterProvider.getInstance().setTotalTime(totalTime);

			// calculate progress
			final double progress = sublistCounter < targetLists.size() - 1
					? (double) ((sublistCounter + 1) * LIST_PARTITION) / (double) targets.size() : 1.0F;

			// calculate average speed
			final double avgSpeed = LIST_PARTITION / (totalTime / (sublistCounter + 1) / 1.0E9F);
			Starter.LOG.info("progress: " + sublist.get(sublist.size() - 1) + " (" + percentFormat.format(progress)
					+ ", avg. " + df.format(avgSpeed) + " structures/s)");

			// count sublist position
			sublistCounter++;
			// update sublist counter
			ParameterProvider.getInstance().setSublistPos(sublistCounter);
		}

		// store EC mapping result if mapping enabled
		if (ParameterProvider.getInstance().isEcMapping()) {

			EcStorage.getInstance().writeStorage();
		}

		// store Pfam mapping result if mapping enabled
		if (ParameterProvider.getInstance().isPfamMapping()) {

			PfamStorage.getInstance().writeStorage();
		}

		// shutdown executor
		executor.shutdown();

		// remove shutdown hook
		Runtime.getRuntime().removeShutdownHook(SESSION_SAVER);

		// get total results
		LOG.info("generating results");
		try {

			final List<Hit> totalResults = rh.read();

			if (!totalResults.isEmpty()) {

				// calculate p-values
				if (ParameterProvider.getInstance().isPvalues()) {

					// p-value calculation according to Fofanov et al. 2008
					if (ParameterProvider.getInstance().getPvalueMethod().equals("F")) {

						LOG.info("calculating p-values (N=" + FofanovModel.REF_SIZE + ",NS="
								+ FofanovModel.getInstance().getNs() + ",GS=" + FofanovModel.getInstance().getGs()
								+ ")");
						try {

							FofanovModel.getInstance().calculatePvalues(totalResults);
							FofanovModel.getInstance().assignPvalues(totalResults);

						} catch (StatisticalModelException e) {

							// set p-value calculation to false if failed
							ParameterProvider.getInstance().setPvalues(false);

							LOG.warning(e.getMessage());
						}
					}

					// p-value calculation according to Stark et al. 2003
					if (ParameterProvider.getInstance().getPvalueMethod().equals("S")) {

						LOG.info("calculating p-values");

						StarkModel sm = new StarkModel();
						sm.calculatePvalues(totalResults);
					}
				}

				// classify hits if enabled
				if (ParameterProvider.getInstance().isClassify()) {

					LOG.info("preparing classification");

					try {

						// initialize classifier depending on classification
						// type
						HitClassifier hCi;
						String classificationTarget = ParameterProvider.getInstance().getClassifyEc();
						if (classificationTarget.startsWith("PF")) {

							hCi = new HitPfamClassifier(totalResults, classificationTarget);
						} else {

							hCi = new HitEcClassifier(totalResults, classificationTarget);
						}

						// generate data sets
						LOG.info("generating balanced data sets");
						hCi.generateSets(true);

						LOG.info("positive/negative instances: " + hCi.getPositiveSet().size() + "/"
								+ hCi.getNegativeSet().size());

						// build classifier
						LOG.info("buildung classifier");
						hCi.buildClassifier();

						// classify results
						LOG.info("classifying instances");
						hCi.assignClasses();

						// TODO performance analysis
						hCi.select();

						// ResultHandler.cleanTemp();
						// System.exit(0);

						// store ASA results
						AsaStorage.getInstance().writeStorage();

					} catch (ClassifierException e) {

						LOG.warning(e.getMessage());
					}
				}

				// cluster hits based on RMSD
				if (ParameterProvider.getInstance().isCluster()) {

					Starter.LOG.info("preparing clustering");

					try {

						HitClusterer hC = new HitClusterer(totalResults,
								ParameterProvider.getInstance().getClusterRmsdCutoff());
						hC.cluster();

					} catch (ClusterException e) {

						LOG.warning(e.getMessage());
					}
				}

				// write mapped sequences to FASTA
				if (ParameterProvider.getInstance().isGapSeqMapping()) {

					try {
						InputOutputUtils.writeGapSequencesToFasta(totalResults);
					} catch (IOException e) {

						Starter.LOG.warning(e.getMessage());
					}
				}
			}

			// result count
			final int resultCount = totalResults.size();

			// no output for empty results
			if (resultCount != 0) {

				// initialize result printer
				final ResultPrinter rp = new ResultPrinter(totalResults, true);

				// print results to console
				rp.printToConsole();

				// print results to file if enabled
				if (ParameterProvider.getInstance().isResultFile()) {

					rp.printToFile();
				}
			}

			// print result count
			switch (resultCount) {
			case 0:
				LOG.info("no hits were found");
				break;
			case 1:
				LOG.info(totalResults.size() + " hit was found");
				break;
			default:
				LOG.info(String.format(Starter.LOCALIZATION, "%,d", totalResults.size()) + " hits were found");
			}

			// FIXME ONLY FOR WEB SERVER LIBRARY: stores all results as binary
			// object
			// rh.writeAllResults(totalResults);

			// remove temporary files
			ResultHandler.cleanTemp();

		} catch (OutOfMemoryError e) {

			LOG.severe("too many results, try to increase heap space " + e.getMessage());
		} catch (Exception e) {

			LOG.severe("unhandled exception " + e.getMessage());
			e.printStackTrace();
		}
	}
}
