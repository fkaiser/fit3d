/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Calc;
import org.biojava.bio.structure.StructureException;

import de.bioforscher.fit3d.io.parser.StructureParser;
import de.bioforscher.fit3d.io.session.SessionLoader;
import de.bioforscher.fit3d.io.utils.InputOutputUtils;
import de.bioforscher.fit3d.statistics.FofanovModel;
import de.bioforscher.fit3d.utils.AtomTypes;
import de.bioforscher.fit3d.utils.StructureUtils;

public final class ParameterProvider implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4667233810607463688L;

	private static ParameterProvider instance;

	public static ParameterProvider getInstance() {

		return instance;
	}

	public static void initializeFromBean() {

		// TODO bind web server
	}

	public static void initializeFromCommandLine(CommandLine cl, boolean initializeExchanges)
			throws ParseException, FileNotFoundException, IOException, StructureException, NumberFormatException {

		ParameterProvider inst = new ParameterProvider();

		// set command line
		inst.cl = cl;

		// set timestamp
		inst.timestamp = Starter.TIMESTAMP;

		// set GS
		inst.gs = 0;

		// set NS
		inst.ns = 0;

		// set sublist position
		inst.sublistPos = 0;

		// set total time
		inst.totalTime = 0;

		// parse motif structure
		StructureParser motifParser = new StructureParser(null, new File(cl.getOptionValue('m')));
		List<AminoAcid> motifAminoAcids = motifParser.getAllAminoAcids();

		// set custom atoms
		if (cl.hasOption('a')) {

			// extract all atom types from given motif
			Set<String> uniqueAtomList = StructureUtils
					.getUniqueAtomList(motifAminoAcids.toArray(new AminoAcid[motifAminoAcids.size()]));

			// extract all given atoms and check if atoms are valid
			// characters and contained in motif
			List<String> clAtoms = new ArrayList<>();

			for (String s : cl.getOptionValue('a').toUpperCase().split(",")) {

				if (!Arrays.asList(Starter.PDB_ATOMS).contains(InputOutputUtils.toFullAtomName(s))) {

					throw new ParseException("Invalid atoms in atom list.");
				}
				if (!uniqueAtomList.contains(s)) {

					throw new StructureException("One or more specified atom is not contained in motif.");
				}

				clAtoms.add(InputOutputUtils.toFullAtomName(s));
			}

			// set new atom list
			inst.atoms = clAtoms.toArray(new String[clAtoms.size()]);
		}

		// use motif atoms if no custom atoms
		if (!cl.hasOption('a')) {

			Set<String> uniqueAtomList = StructureUtils
					.getUniqueAtomList(motifAminoAcids.toArray(new AminoAcid[motifAminoAcids.size()]));

			List<String> motifAtoms = new ArrayList<>();
			for (String s : uniqueAtomList) {

				motifAtoms.add(InputOutputUtils.toFullAtomName(s));
			}

			// set new atom list
			inst.atoms = motifAtoms.toArray(new String[motifAtoms.size()]);
		}

		if (cl.hasOption('B')) {

			inst.classify = true;
			inst.classifyEc = cl.getOptionValue('B');
		} else {

			inst.classify = false;
		}

		// cluster
		if (cl.hasOption('C')) {

			inst.cluster = true;
			inst.clusterRmsdCutoff = Double.valueOf(cl.getOptionValue('C'));
		} else {

			inst.cluster = false;
		}

		// conserve side chain atoms
		if (cl.hasOption('c')) {

			inst.conserve = true;
		} else {

			inst.conserve = false;
		}

		// generate exchanges
		if (cl.hasOption('e') && initializeExchanges) {

			String exchangeDef = cl.getOptionValue('e');
			if (!inst.isValidExchangeDefinition(exchangeDef)) {

				throw new ParseException("Invalid residue exchange definition.");
			}

			inst.exchangesByResNum = new HashMap<>();
			inst.exchangesByType = new HashMap<>();
			inst.initializeExchanges(exchangeDef, motifAminoAcids);
		} else {

			inst.exchangesByResNum = new HashMap<>();
			inst.exchangesByType = new HashMap<>();
			inst.exchangeDistributions = new ArrayList<>();
		}

		// map EC numbers if enabled or if classification is enabled
		if (cl.hasOption('E') || cl.hasOption('B')) {

			inst.ecMapping = true;
		} else {

			inst.ecMapping = false;
		}

		// generate distance map
		inst.generateMotifDistanceMap(motifAminoAcids);

		// distance tolerance
		if (cl.hasOption('d')) {

			inst.distanceTolerance = Math.pow(Double.valueOf(cl.getOptionValue('d')), 2);
		} else {

			inst.distanceTolerance = Math.pow(1.0, 2);
		}

		// result file
		if (cl.hasOption('f')) {

			inst.resultFile = true;
			inst.resultFilePath = cl.getOptionValue('f');
		} else {

			inst.resultFile = false;
		}

		// filter environment
		if (cl.hasOption('F')) {

			inst.filterEnvironment = true;
		} else {

			inst.filterEnvironment = false;
		}

		// align output structures
		if (cl.hasOption('g')) {

			inst.alignOutputStructures = true;
		} else {

			inst.alignOutputStructures = false;
		}

		// gap sequence mapping
		if (cl.hasOption('G')) {

			inst.gapSeqMapping = true;
		} else {

			inst.gapSeqMapping = false;
		}

		// show help dialog
		if (cl.hasOption('h')) {

			throw new ParseException("");
		}

		// ignore atoms
		if (cl.hasOption('i')) {

			inst.ignoreAtoms = true;
		} else {

			inst.ignoreAtoms = false;
		}

		// check if list was given
		if (cl.hasOption('l')) {

			inst.listTarget = true;
			inst.list = cl.getOptionValue('l');
		} else {

			inst.listTarget = false;
		}

		// motif structure
		if (cl.hasOption('m')) {

			inst.motif = cl.getOptionValue('m');
		}

		// number of cores
		if (cl.hasOption('n')) {

			inst.cores = Integer.valueOf(cl.getOptionValue('n'));
		} else {

			inst.cores = Runtime.getRuntime().availableProcessors();
		}

		// output structures directory
		if (cl.hasOption('o')) {

			inst.outputStructures = true;
			inst.outputStructuresDirectory = cl.getOptionValue('o');

			// create directory if not existent
			File dir = new File(inst.outputStructuresDirectory);
			if (!dir.exists()) {

				dir.mkdirs();
			}
		} else {

			inst.outputStructures = false;
		}

		// PDB directory
		if (cl.hasOption('p')) {

			inst.pdbDirectory = cl.getOptionValue('p');
		} else {

			inst.pdbDirectory = Starter.TMP_DIR;
		}

		// p-value calculation
		if (cl.hasOption('P')) {

			inst.pvalues = true;

			if (!(cl.getOptionValue('P').equals("F") || cl.getOptionValue('P').equals("S"))) {

				throw new ParseException("Invalid p-value calculation method.");
			}

			inst.pvalueMethod = cl.getOptionValue('P');
		} else {

			inst.pvalues = false;
		}

		// Pfam mapping
		if (cl.hasOption("M")) {

			inst.pfamMapping = true;
		} else {

			inst.pfamMapping = false;
		}

		// reference population size of Fofanov et al. model
		if (cl.hasOption('N')) {

			inst.refSize = Integer.valueOf(cl.getOptionValue('N'));
		} else {

			inst.refSize = FofanovModel.REF_SIZE;
		}

		// set quiet log level
		if (cl.hasOption('q')) {

			inst.logLevel = Level.SEVERE;
		}

		// maximal RMSD
		if (cl.hasOption('r')) {

			inst.maxRmsd = Double.valueOf(cl.getOptionValue('r'));
		} else {

			inst.maxRmsd = 2.0;
		}

		// PDB directory split
		if (cl.hasOption('s')) {

			inst.pdbDirectorySplit = false;
		} else {

			inst.pdbDirectorySplit = true;
		}

		// target
		if (cl.hasOption('t')) {

			inst.target = cl.getOptionValue('t');
		}

		// map title
		if (cl.hasOption('T')) {

			inst.titleMapping = true;
		} else {

			inst.titleMapping = false;
		}

		// set verbose log level
		if (cl.hasOption('v')) {

			inst.logLevel = Level.FINE;
		}

		// set extra verbose log level
		if (cl.hasOption('x')) {

			inst.logLevel = Level.FINEST;
		}

		if (cl.hasOption('X')) {

			inst.extractMotif = true;
			inst.extractMotifString = cl.getOptionValue('X');
		}

		// set standard log level
		if (!cl.hasOption('q') && !cl.hasOption('v') && !cl.hasOption('x')) {

			inst.logLevel = Level.INFO;
		}

		// assign instance
		ParameterProvider.instance = inst;
		ParameterProvider.instance.postInitialization();
	}

	public static void initializeFromSession(String sessionFilePath) throws ClassNotFoundException, IOException {

		SessionLoader sl = new SessionLoader();
		ParameterProvider.instance = sl.load(sessionFilePath);
		ParameterProvider.instance.postInitialization();
	}

	private int refSize;

	private boolean extractMotif;

	private String extractMotifString;

	// command line
	private CommandLine cl;

	// motif distance map
	private Map<String, Double> motifDistanceMap;

	// exchange settings
	private Map<Integer, Set<Character>> exchangesByResNum;

	private Map<Character, Set<Character>> exchangesByType;

	private List<Map<Character, Integer>> exchangeDistributions;

	// filter environment
	private boolean filterEnvironment;

	// PDB settings
	private String pdbDirectory;

	private boolean pdbDirectorySplit;

	// algorithm parameters
	private boolean ignoreAtoms;
	private double distanceTolerance;
	private double maxRmsd;

	// atoms
	private String[] atoms;

	// classify hits
	private boolean classify;
	// classification
	private String classifyEc;

	// cluster
	private boolean cluster;
	private double clusterRmsdCutoff;

	// conserve sidechain atoms
	private boolean conserve;
	// gap sequence mapping
	private boolean gapSeqMapping;

	// log level
	private Level logLevel;

	// output result file
	private boolean resultFile;

	private String resultFilePath;

	// output structures settings
	private boolean outputStructures;

	private boolean alignOutputStructures;

	private String outputStructuresDirectory;

	// input settings
	private String motif;

	// statistical model parameters GS and NS
	private int gs;

	private int ns;

	// target settings
	private String target;
	private String list;

	private boolean listTarget;
	// parallelization settings
	private int cores;
	// Pfam annotation mapping
	private boolean pfamMapping;

	// p-value calculation
	private boolean pvalues;

	private String pvalueMethod;
	// EC number mapping
	private boolean ecMapping;

	// timestamp
	private long timestamp;

	// sublist position
	private int sublistPos;

	// total time
	private long totalTime;

	// title mapping
	private boolean titleMapping;

	private ParameterProvider() {

	}

	public String[] getAtoms() {
		return this.atoms;
	}

	public CommandLine getCl() {
		return this.cl;
	}

	public String getClassifyEc() {
		return this.classifyEc;
	}

	public double getClusterRmsdCutoff() {
		return this.clusterRmsdCutoff;
	}

	public int getCores() {
		return this.cores;
	}

	public double getDistanceTolerance() {
		return this.distanceTolerance;
	}

	public List<Map<Character, Integer>> getExchangeDistributions() {
		return this.exchangeDistributions;
	}

	public Map<Integer, Set<Character>> getExchangesByResNum() {
		return this.exchangesByResNum;
	}

	public Map<Character, Set<Character>> getExchangesByType() {
		return this.exchangesByType;
	}

	public String getExtractMotifString() {
		return this.extractMotifString;
	}

	public int getGs() {
		return this.gs;
	}

	public String getList() {
		return this.list;
	}

	public Level getLogLevel() {
		return this.logLevel;
	}

	public double getMaxRmsd() {
		return this.maxRmsd;
	}

	public String getMotif() {
		return this.motif;
	}

	public Map<String, Double> getMotifDistanceMap() {
		return this.motifDistanceMap;
	}

	public int getNs() {
		return this.ns;
	}

	public String getOutputStructuresDirectory() {
		return this.outputStructuresDirectory;
	}

	public String getPdbDirectory() {
		return this.pdbDirectory;
	}

	public String getPvalueMethod() {

		return this.pvalueMethod;
	}

	public Integer getRefSize() {
		return this.refSize;
	}

	public String getResultFilePath() {
		return this.resultFilePath;
	}

	public int getSublistPos() {
		return this.sublistPos;
	}

	public String getTarget() {
		return this.target;
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public long getTotalTime() {
		return this.totalTime;
	}

	public boolean isAlignOutputStructures() {
		return this.alignOutputStructures;
	}

	public boolean isClassify() {

		return this.classify;
	}

	public boolean isCluster() {
		return this.cluster;
	}

	public boolean isConserve() {
		return this.conserve;
	}

	public boolean isEcMapping() {
		return this.ecMapping;
	}

	public boolean isExtractMotif() {
		return this.extractMotif;
	}

	public boolean isFilterEnvironment() {
		return this.filterEnvironment;
	}

	public boolean isGapSeqMapping() {
		return this.gapSeqMapping;
	}

	public boolean isIgnoreAtoms() {
		return this.ignoreAtoms;
	}

	public boolean isListTarget() {
		return this.listTarget;
	}

	public boolean isOutputStructures() {
		return this.outputStructures;
	}

	public boolean isPdbDirectorySplit() {
		return this.pdbDirectorySplit;
	}

	public boolean isPfamMapping() {
		return this.pfamMapping;
	}

	public boolean isPvalues() {
		return this.pvalues;
	}

	public boolean isResultFile() {
		return this.resultFile;
	}

	public boolean isTitleMapping() {
		return this.titleMapping;
	}

	public void reinitializeExchanges() throws FileNotFoundException, IOException, StructureException, ParseException {

		// parse motif structure
		StructureParser motifParser = new StructureParser(null, new File(this.motif));
		List<AminoAcid> motifAminoAcids = motifParser.getAllAminoAcids();

		// generate exchanges
		if (this.cl.hasOption('e')) {

			String exchangeDef = this.cl.getOptionValue('e');
			if (!isValidExchangeDefinition(exchangeDef)) {

				throw new ParseException("Invalid residue exchange definition.");
			}

			this.exchangesByResNum = new HashMap<>();
			this.exchangesByType = new HashMap<>();
			this.initializeExchanges(exchangeDef, motifAminoAcids);
		} else {

			this.exchangesByResNum = new HashMap<>();
			this.exchangesByType = new HashMap<>();
			this.exchangeDistributions = new ArrayList<>();
		}
	}

	public void setAlignOutputStructures(boolean alignOutputStructures) {
		this.alignOutputStructures = alignOutputStructures;
	}

	public void setAtoms(String[] atoms) {
		this.atoms = atoms;
	}

	public void setCl(CommandLine cl) {
		this.cl = cl;
	}

	public void setClassify(boolean classify) {
		this.classify = classify;
	}

	public void setClassifyEc(String classifyEc) {
		this.classifyEc = classifyEc;
	}

	public void setCluster(boolean cluster) {
		this.cluster = cluster;
	}

	public void setClusterRmsdCutoff(double clusterRmsdCutoff) {
		this.clusterRmsdCutoff = clusterRmsdCutoff;
	}

	public void setConserve(boolean conserve) {
		this.conserve = conserve;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public void setDistanceTolerance(double distanceTolerance) {
		this.distanceTolerance = distanceTolerance;
	}

	public void setEcMapping(boolean ecMapping) {
		this.ecMapping = ecMapping;
	}

	public void setExchangeDistributions(List<Map<Character, Integer>> exchangeDistributions) {
		this.exchangeDistributions = exchangeDistributions;
	}

	public void setExchangesByResNum(Map<Integer, Set<Character>> exchangesByResNum) {
		this.exchangesByResNum = exchangesByResNum;
	}

	public void setExchangesByType(Map<Character, Set<Character>> exchangesByType) {
		this.exchangesByType = exchangesByType;
	}

	public void setExtractMotif(boolean extractMotif) {
		this.extractMotif = extractMotif;
	}

	public void setExtractMotifString(String extractMotifString) {
		this.extractMotifString = extractMotifString;
	}

	public void setFilterEnvironment(boolean filterEnvironment) {
		this.filterEnvironment = filterEnvironment;
	}

	public void setGapSeqMapping(boolean gapSeqMapping) {
		this.gapSeqMapping = gapSeqMapping;
	}

	public void setGs(int gs) {
		this.gs = gs;
	}

	public void setIgnoreAtoms(boolean ignoreAtoms) {
		this.ignoreAtoms = ignoreAtoms;
	}

	public void setList(String list) {
		this.list = list;
	}

	public void setListTarget(boolean listTarget) {
		this.listTarget = listTarget;
	}

	public void setLogLevel(Level logLevel) {
		this.logLevel = logLevel;
	}

	public void setMaxRmsd(double maxRmsd) {
		this.maxRmsd = maxRmsd;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	public void setMotifDistanceMap(Map<String, Double> motifDistanceMap) {
		this.motifDistanceMap = motifDistanceMap;
	}

	public void setNs(int ns) {
		this.ns = ns;
	}

	public void setOutputStructures(boolean outputStructures) {
		this.outputStructures = outputStructures;
	}

	public void setOutputStructuresDirectory(String outputStructuresDirectory) {
		this.outputStructuresDirectory = outputStructuresDirectory;
	}

	public void setPdbDirectory(String pdbDirectory) {
		this.pdbDirectory = pdbDirectory;
	}

	public void setPdbDirectorySplit(boolean pdbDirectorySplit) {
		this.pdbDirectorySplit = pdbDirectorySplit;
	}

	public void setPfamMapping(boolean pfamMapping) {
		this.pfamMapping = pfamMapping;
	}

	public void setPvalues(boolean pvalues) {

		this.pvalues = pvalues;
	}

	public void setPvalues(String pvalueMethod) {

		this.pvalueMethod = pvalueMethod;
	}

	public void setRefSize(Integer refSize) {
		this.refSize = refSize;
	}

	public void setResultFile(boolean resultFile) {
		this.resultFile = resultFile;
	}

	public void setResultFilePath(String resultFilePath) {
		this.resultFilePath = resultFilePath;
	}

	public void setSublistPos(int sublistPos) {
		this.sublistPos = sublistPos;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public void setTitleMapping(boolean titleMapping) {
		this.titleMapping = titleMapping;
	}

	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	/**
	 * calculates all possible amino acid distributions of a given vector
	 * 
	 * @param vector
	 * @return list of amino acid distributions
	 */
	private List<Map<Character, Integer>> generateExchangeDistributions(String[] vector) {

		List<Map<Character, Integer>> exchangeDistributions = new ArrayList<>();

		// index of vector elements
		int[] index = new int[vector.length];

		do {
			// storage object
			Map<Character, Integer> d = new HashMap<>();

			// join current object
			for (int j = 0; j < vector.length; j++) {

				char c = vector[j].charAt(index[j]);

				if (!d.containsKey(c)) {

					d.put(c, 1);
				} else {

					d.put(c, d.get(c) + 1);
				}
			}

			exchangeDistributions.add(d);

			// shift indices
			index[index.length - 1]++;

			for (int j = index.length - 1; j > 0; j--) {
				if (index[j] > vector[j].length() - 1) {

					index[j - 1]++;
				}

				index[j] = index[j] % vector[j].length();
			}

		} while (index[0] < vector[0].length());

		return exchangeDistributions;
	}

	/**
	 * generates the pairwise distance map of the motif
	 * 
	 * @param motif
	 * @return distance map
	 * @throws StructureException
	 */
	private void generateMotifDistanceMap(List<AminoAcid> motif) throws StructureException {

		Map<String, Double> distanceMap = new HashMap<>();

		for (AminoAcid aa1 : motif) {

			Character aa1Type = aa1.getAminoType();

			List<Character> aa1Exchanges = new ArrayList<>();
			if (this.exchangesByType.containsKey(aa1Type)) {

				for (Character aa1ExchangeType : this.exchangesByType.get(aa1Type)) {

					aa1Exchanges.add(aa1ExchangeType);
				}
			}
			aa1Exchanges.add(aa1Type);

			for (Character type1 : aa1Exchanges) {

				for (AminoAcid aa2 : motif) {

					if (aa1.equals(aa2)) {

						continue;
					}

					double d = Calc.getDistanceFast(aa1.getAtom(AtomTypes.CA.getAtomName()),
							aa2.getAtom(AtomTypes.CA.getAtomName()));

					Character aa2Type = aa2.getAminoType();

					List<Character> aa2Exchanges = new ArrayList<>();
					if (this.exchangesByType.containsKey(aa2Type)) {

						for (Character aa2ExchangeType : this.exchangesByType.get(aa2Type)) {

							aa2Exchanges.add(aa2ExchangeType);
						}
					}
					aa2Exchanges.add(aa2Type);

					for (Character type2 : aa2Exchanges) {

						String distanceKey = type1.toString() + type2.toString();

						if (distanceMap.containsKey(distanceKey)) {

							double lastDistance = distanceMap.get(distanceKey);

							if (lastDistance > d) {

								continue;
							}

							distanceMap.put(distanceKey, d);

						} else {

							distanceMap.put(distanceKey, d);
						}
					}
				}
			}
		}

		this.motifDistanceMap = distanceMap;
	}

	/**
	 * initializes residue exchanges
	 * 
	 * @param exchangeDef
	 * @param motif
	 * @throws ParseException
	 */
	private void initializeExchanges(String exchangeDef, List<AminoAcid> motif) throws ParseException {

		// set empty list if no exchanges were defined
		if (exchangeDef == null) {

			this.exchangeDistributions = new ArrayList<>();
		}

		List<String> exchangeStrings = new ArrayList<>();
		for (String s : exchangeDef.split(",")) {

			Integer motifResidueNumber = Integer.valueOf(s.split(":")[0]);
			String allowedResidues = s.split(":")[1];

			// allow all residues
			if (allowedResidues.equals("X")) {

				allowedResidues = "GPAVLIMCFYWHKRQNEDST";
			}

			// determine residue type of exchange definition
			Character motifResidueType = StructureUtils.getResidueTypeFromResidueNumber(motifResidueNumber, motif);

			// throw exception if residue does not exist
			if (motifResidueType == null) {

				throw new ParseException("Invalid residue exchange definition. Residue number not found in motif.");
			}

			// remove original residue type if defined in allowed residues
			allowedResidues = allowedResidues.replaceAll(motifResidueType.toString(), "");

			Set<Character> allowesResidueSet = new HashSet<>();
			for (Character c : allowedResidues.toCharArray()) {

				allowesResidueSet.add(c);
			}

			// store single residue exchange
			this.exchangesByResNum.put(motifResidueNumber, allowesResidueSet);

			// store exchanges by type
			if (this.exchangesByType.containsKey(motifResidueType)) {

				Set<Character> currentAllowedResidues = this.exchangesByType.get(motifResidueType);
				currentAllowedResidues.addAll(allowesResidueSet);
				this.exchangesByType.put(motifResidueType, currentAllowedResidues);
			} else {

				this.exchangesByType.put(motifResidueType, allowesResidueSet);
			}

			// store string for exchange generation
			exchangeStrings.add(motifResidueType + allowedResidues);
		}

		// add remaining motif amino acids to exchange strings
		for (AminoAcid motifAminoAcid : motif) {

			if (!this.exchangesByResNum.containsKey(motifAminoAcid.getResidueNumber().getSeqNum())) {

				exchangeStrings.add(motifAminoAcid.getAminoType().toString());
			}
		}

		// generate list of exchange distributions
		this.exchangeDistributions = generateExchangeDistributions(
				exchangeStrings.toArray(new String[exchangeStrings.size()]));
	}

	/**
	 * checks if exchange definition is valid
	 * 
	 * @param exchangeDefinition
	 * @return true if valid exchange definition
	 */
	private boolean isValidExchangeDefinition(String exchangeDefinition) {

		String regex = "(\\d+:[GPAVLIMCFYWHKRQNEDSTX]+,)+";
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(exchangeDefinition + ",");

		if (matcher.matches()) {

			return true;
		}

		return false;
	}

	private void postInitialization() {

		// set log level
		Starter.LOG.setLevel(this.logLevel);
	}

}
