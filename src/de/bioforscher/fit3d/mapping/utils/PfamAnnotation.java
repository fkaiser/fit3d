/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.mapping.utils;

import java.io.Serializable;

/**
 * A class to hold a Pfam annotation.
 * 
 * @author fkaiser
 *
 */
public class PfamAnnotation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6741776222333800317L;
	private String accNumber;
	private int startPdb;
	private int stopPdb;
	private char chainId;

	public String getAccNumber() {
		return this.accNumber;
	}

	public char getChainId() {
		return this.chainId;
	}

	public int getStartPdb() {
		return this.startPdb;
	}

	public int getStopPdb() {
		return this.stopPdb;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setChainId(char chainId) {
		this.chainId = chainId;
	}

	public void setStartPdb(int startPdb) {
		this.startPdb = startPdb;
	}

	public void setStopPdb(int stopPdb) {
		this.stopPdb = stopPdb;
	}
}
