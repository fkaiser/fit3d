/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.mapping.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bioforscher.fit3d.Starter;

public class PfamStorage {

	// FIXME ONLY FOR WEB SERVER LIBRARY: only web server!!!
	// private static final String PFAM_STORAGE_PATH = "../../pfamStorage.fit";
	private static final String PFAM_STORAGE_PATH = "pfamStorage.fit";

	private static PfamStorage instance = new PfamStorage();

	public static PfamStorage getInstance() {

		return instance;
	}

	private Map<String, List<PfamAnnotation>> storage;

	@SuppressWarnings("unchecked")
	private PfamStorage() {

		File pfamStorageFile = new File(PFAM_STORAGE_PATH);
		if (pfamStorageFile.exists()) {

			try {

				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(pfamStorageFile));

				Object o = ois.readObject();
				if (o instanceof Map<?, ?>) {

					this.storage = (Map<String, List<PfamAnnotation>>) o;
				}
				ois.close();

			} catch (ClassNotFoundException e) {

				Starter.LOG.warning(e.getMessage());
			} catch (FileNotFoundException e) {

				Starter.LOG.warning(e.getMessage());
			} catch (IOException e) {

				Starter.LOG.warning(e.getMessage());
			}
		} else {

			this.storage = new HashMap<>();
		}
	}

	/**
	 * adds an entry to the storage
	 * 
	 * @param identifier
	 * @param pfamAcc
	 */
	public void addEntry(String pdbId, List<PfamAnnotation> annotations) {

		this.storage.put(pdbId, annotations);
	}

	public Map<String, List<PfamAnnotation>> getStorage() {
		return this.storage;
	}

	/**
	 * writes storage to disk
	 */
	public void writeStorage() {

		try {

			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(PFAM_STORAGE_PATH));
			oos.writeObject(this.storage);
			oos.close();

		} catch (FileNotFoundException e) {

			Starter.LOG.warning(e.getMessage());
		} catch (IOException e) {

			Starter.LOG.warning(e.getMessage());
		}
	}
}
