/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.mapping.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bioforscher.fit3d.Starter;

public class EcStorage {

	// FIXME ONLY FOR WEB SERVER LIBRARY: only web server!!!
	// private static final String EC_STORAGE_PATH = "../../ecStorage.fit";
	private static final String EC_STORAGE_PATH = "ecStorage.fit";

	private static EcStorage instance = new EcStorage();

	public static EcStorage getInstance() {

		return instance;
	}

	private Map<String, List<EcAnnotaion>> storage;

	@SuppressWarnings("unchecked")
	private EcStorage() {

		File ecStorageFile = new File(EC_STORAGE_PATH);
		if (ecStorageFile.exists()) {

			try {

				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ecStorageFile));

				Object o = ois.readObject();
				if (o instanceof Map<?, ?>) {

					this.storage = (Map<String, List<EcAnnotaion>>) o;
				}
				ois.close();

			} catch (ClassNotFoundException e) {

				Starter.LOG.warning(e.getMessage());
			} catch (FileNotFoundException e) {

				Starter.LOG.warning(e.getMessage());
			} catch (IOException e) {

				Starter.LOG.warning(e.getMessage());
			}
		} else {

			this.storage = new HashMap<>();
		}
	}

	/**
	 * adds an entry to the storage
	 * 
	 * @param pdbId
	 * @param ecNumber
	 */
	public void addEntry(String pdbId, List<EcAnnotaion> annotations) {

		this.storage.put(pdbId, annotations);
	}

	public Map<String, List<EcAnnotaion>> getStorage() {

		return this.storage;
	}

	/**
	 * writes storage to disk
	 */
	public void writeStorage() {

		try {

			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(EC_STORAGE_PATH));
			oos.writeObject(this.storage);
			oos.close();

		} catch (FileNotFoundException e) {

			Starter.LOG.warning(e.getMessage());
		} catch (IOException e) {

			Starter.LOG.warning(e.getMessage());
		}
	}
}
