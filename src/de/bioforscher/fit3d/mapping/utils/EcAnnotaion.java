/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.mapping.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to hold a EC annotation.
 * 
 * @author fkaiser
 *
 */
public class EcAnnotaion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2054682518514618969L;
	private List<Character> chainIds = new ArrayList<Character>();
	private String ecClass;

	public void addChainId(Character id) {

		this.chainIds.add(id);
	}

	public List<Character> getChainIds() {
		return this.chainIds;
	}

	public String getEcClass() {
		return this.ecClass;
	}

	public void setChainIds(List<Character> chainIds) {
		this.chainIds = chainIds;
	}

	public void setEcClass(String ecClass) {
		this.ecClass = ecClass;
	}
}
