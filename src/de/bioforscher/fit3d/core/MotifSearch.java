/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.Calc;
import org.biojava.bio.structure.Chain;
import org.biojava.bio.structure.PDBHeader;
import org.biojava.bio.structure.SVDSuperimposer;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.jama.Matrix;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.io.StructureWriter;
import de.bioforscher.fit3d.io.parser.StructureParser;
import de.bioforscher.fit3d.io.utils.InputOutputUtils;
import de.bioforscher.fit3d.mapping.utils.EcAnnotaion;
import de.bioforscher.fit3d.mapping.utils.EcStorage;
import de.bioforscher.fit3d.mapping.utils.PfamAnnotation;
import de.bioforscher.fit3d.mapping.utils.PfamStorage;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;
import de.bioforscher.fit3d.model.impl.HitAminoAcidImpl;
import de.bioforscher.fit3d.model.impl.HitAtomImpl;
import de.bioforscher.fit3d.model.impl.HitImpl;
import de.bioforscher.fit3d.statistics.FofanovModel;
import de.bioforscher.fit3d.utils.AtomTypes;
import de.bioforscher.fit3d.utils.StructureUtils;

/**
 * A motif search in a target structure.
 * 
 * @author fkaiser
 * 
 */
public class MotifSearch implements Callable<List<Hit>> {

	private String motifFile;
	private List<AminoAcid> motifAminoAcids = new ArrayList<>();

	private List<AminoAcid> targetAminoAcids = new ArrayList<>();
	private List<AminoAcid> preprocessedTarget = new ArrayList<>();
	private PDBHeader targetHeader;

	private double motifExpansion;
	private Map<Character, Integer> motifDistribution = new HashMap<>();
	private Map<String, Double> motifDistanceMap = new HashMap<>();
	private String target;

	private List<Map<Character, Integer>> exchangeDistributions = new ArrayList<>();

	private List<AminoAcid> lastBestPermutation = new ArrayList<>();
	private final List<Hit> results = new ArrayList<>();

	// private String ecNumber;

	// private String motifFile;
	// private List<AminoAcid> motifAminoAcids;
	//
	// private List<AminoAcid> targetAminoAcids;
	// private List<AminoAcid> preprocessedTarget;
	// private PDBHeader targetHeader;
	//
	// private double motifExpansion;
	// private Map<Character, Integer> motifDistribution;
	// private Map<String, Double> motifDistanceMap;
	// private String target;
	//
	// private List<Map<Character, Integer>> exchangeDistributions;
	//
	// private List<AminoAcid> lastBestPermutation;
	// private List<Hit> results;
	// private String ecNumber;

	public MotifSearch(String motifFile, String target, List<Map<Character, Integer>> exchangeDistributions,
			Map<String, Double> motifDistanceMap) {

		this.motifFile = motifFile;
		this.target = target;

		this.exchangeDistributions = exchangeDistributions;
		this.motifDistanceMap = motifDistanceMap;
	}

	@Override
	public List<Hit> call() {

		Starter.LOG.fine("processing " + this.target);

		try {

			// initialize
			initialize();

			// check if hit is possible in structure, if not increase NS
			countNs();

			for (Map<Character, Integer> currentDistribution : this.exchangeDistributions) {

				// set current distribution
				this.motifDistribution = currentDistribution;

				// preprocess target
				preprocessStructure();

				// // TODO algorithm visualization
				// List<String> preprocessedViz = new ArrayList<>();
				// for (AminoAcid aa : this.preprocessedTarget) {
				//
				// for (Atom a : aa.getAtoms()) {
				//
				// preprocessedViz.add(a.toPDB());
				// }
				// }
				// FileUtils.writeLines(new File("preprocessedTarget.pdb"),
				// preprocessedViz);
				//
				// // TODO algorithm visualization
				// int viscounter = 1;

				// iterate over preprocessed target structure
				for (AminoAcid targetAa : this.preprocessedTarget) {

					// get environment around target amino acid
					List<AminoAcid> env;
					try {
						env = getEnvironment(this.preprocessedTarget, targetAa,
								this.motifExpansion + ParameterProvider.getInstance().getDistanceTolerance());
					} catch (StructureException e) {

						continue;
					}

					// skip if environment is empty or unreachable
					if (env.size() < this.motifAminoAcids.size() - 1 || env.isEmpty()) {

						continue;
					}

					// filter environment if enabled
					if (ParameterProvider.getInstance().isFilterEnvironment()) {

						List<AminoAcid> filteredEnv = filterEnvironment(env, targetAa);
						env = filteredEnv;
					}

					// get environment distribution
					Map<Character, Integer> envDistribution = StructureUtils.getAminoAcidDistribution(env);

					// increase counter for target amino acid
					if (envDistribution.containsKey(targetAa.getAminoType())) {

						envDistribution.put(targetAa.getAminoType(), envDistribution.get(targetAa.getAminoType()) + 1);
					} else {
						envDistribution.put(targetAa.getAminoType(), 1);
					}

					// check if environment is possible solution
					if (!isPossibleSolution(envDistribution, false)) {

						// // TODO algorithm visualization
						// List<String> currentEnvViz = new ArrayList<>();
						// for (AminoAcid aa : env) {
						//
						// for (Atom a : aa.getAtoms()) {
						//
						// currentEnvViz.add(a.toPDB());
						// }
						// }
						// FileUtils.writeLines(
						// new File("env_"
						// + String.format("%04d", viscounter)
						// + "_invalid.pdb"), currentEnvViz);
						//
						// List<String> currentTargetAa = new ArrayList<>();
						// for (Atom a : targetAa.getAtoms()) {
						//
						// currentTargetAa.add(a.toPDB());
						// }
						// FileUtils.writeLines(
						// new File(String.format("%04d", viscounter)
						// + ".pdb"), currentTargetAa);
						// viscounter++;

						continue;
					}

					// // TODO algorithm visualization
					// List<String> currentEnvViz = new ArrayList<>();
					// for (AminoAcid aa : env) {
					//
					// for (Atom a : aa.getAtoms()) {
					//
					// currentEnvViz.add(a.toPDB());
					// }
					// }
					// FileUtils.writeLines(
					// new File("env_" + String.format("%04d", viscounter)
					// + "_valid.pdb"), currentEnvViz);
					// List<String> currentTargetAa = new ArrayList<>();
					// for (Atom a : targetAa.getAtoms()) {
					//
					// currentTargetAa.add(a.toPDB());
					// }
					// FileUtils
					// .writeLines(
					// new File(String.format("%04d", viscounter)
					// + ".pdb"), currentTargetAa);
					// viscounter++;

					// calculate all combinations of environment
					List<AminoAcid> prev = new ArrayList<>();
					List<List<AminoAcid>> combinations = combinateAminoAcids(env, 0, this.motifAminoAcids.size() - 1,
							prev);

					// TODO debug print
					// Starter.LOG.info("#C " + combinations.size());

					// iterate over all combinations
					for (List<AminoAcid> c : combinations) {

						// add target amino acid
						c.add(targetAa);

						// check if already found as hit
						if (isAlreadyHit(getHit(c, -999.0F))) {

							continue;
						}

						// get distribution of combination
						Map<Character, Integer> cDistribution = StructureUtils.getAminoAcidDistribution(c);

						// check if combination distribution is identical to
						// motif
						if (!isPossibleSolution(cDistribution, true)) {

							continue;
						}

						// calculate all permutations of combination
						List<AminoAcid> prev1 = new ArrayList<>();
						List<List<AminoAcid>> permutations = permutateAminoAcids(c, prev1);

						// // TODO debug print
						// Starter.LOG.info("#P " + permutations.size());
						// // // TODO debug print
						// Starter.LOG.info("#CP " + combinations.size() *
						// permutations.size());
						// Starter.LOG.info("");

						// find lowest RMSD
						Hit bestHit = findLowestRmsd(permutations);

						// if no hit exists
						if (bestHit == null) {

							continue;
						}

						// continue if RMSD threshold is missed
						if (bestHit.getRmsd() > ParameterProvider.getInstance().getMaxRmsd()) {

							continue;
						}

						// store result
						this.results.add(bestHit);

						// check if structure output is enabled
						if (!ParameterProvider.getInstance().isOutputStructures()) {

							continue;
						}

						// output result structure
						if (ParameterProvider.getInstance().isAlignOutputStructures()) {

							outputHit(true);
						} else {

							outputHit(false);
						}
					}
				}
			}

		} catch (StructureException e) {

			Starter.LOG.warning(this.target + " " + e.getMessage());
		} catch (IOException e) {

			Starter.LOG.warning(this.target + " " + e.getMessage());
		} finally {

			// clean references
			cleanUp();
		}

		// increase GS if at least one hit was found
		countGs();

		return this.results;
	}

	/**
	 * clears object fields
	 */
	private void cleanUp() {

		this.motifAminoAcids.clear();
		this.motifAminoAcids = null;

		this.targetAminoAcids.clear();
		this.targetAminoAcids = null;

		this.preprocessedTarget.clear();
		this.preprocessedTarget = null;

		this.lastBestPermutation.clear();
		this.lastBestPermutation = null;

		// this.motifDistribution.clear();
		// this.motifDistribution = null;

		this.motifFile = null;
		this.target = null;
		this.targetHeader = null;

		this.exchangeDistributions.clear();
		this.exchangeDistributions = null;

		this.motifDistanceMap.clear();
		this.motifDistanceMap = null;
	}

	/**
	 * recursively calculates all combinations of amino acids without revision
	 * 
	 * @param items
	 * @param start
	 * @param m
	 * @param prev
	 * @return all combinations of amino acids without revision
	 */
	private List<List<AminoAcid>> combinateAminoAcids(List<AminoAcid> items, int start, int m, List<AminoAcid> prev) {

		int n = items.size();

		if (m > n) {

			return null;
		}

		List<List<AminoAcid>> combinations = new ArrayList<>();

		if (m == 0) {

			ArrayList<AminoAcid> copy = new ArrayList<>();
			for (int i = 0; i < prev.size(); i++) {

				copy.add(prev.get(i));
			}
			combinations.add(copy);
			return combinations;
		}

		for (int i = start; i < n - m + 1; i++) {

			prev.add(items.get(i));
			List<List<AminoAcid>> thisCombinations = combinateAminoAcids(items, i + 1, m - 1, prev);
			prev.remove(prev.size() - 1);
			combinations.addAll(thisCombinations);
		}

		return combinations;
	}

	/**
	 * increases GS of statictical model
	 */
	private void countGs() {

		// count GS
		if (!this.results.isEmpty()) {

			FofanovModel.getInstance().countGs();
		}
	}

	/**
	 * checks whether a hit is possible in target or not and increased NS if no
	 * hit is possible
	 */
	private void countNs() {

		// count NS
		Map<Character, Integer> targetDistribution = StructureUtils.getAminoAcidDistribution(this.targetAminoAcids);
		boolean noHitPossible = true;
		for (Map<Character, Integer> currentDistribution : this.exchangeDistributions) {

			this.motifDistribution = currentDistribution;

			if (isPossibleSolution(targetDistribution, false)) {

				noHitPossible = false;
				break;
			}
		}

		if (noHitPossible) {

			FofanovModel.getInstance().countNs();
		}
	}

	/**
	 * filters micro environment based on local distance constraints
	 * 
	 * @param env
	 * @param targetAa
	 * @return filtered environment
	 * @throws StructureException
	 */
	private List<AminoAcid> filterEnvironment(List<AminoAcid> env, AminoAcid targetAa) throws StructureException {

		List<AminoAcid> filteredEnv = new ArrayList<>();

		for (AminoAcid aa : env) {

			String distanceKey = targetAa.getAminoType().toString() + aa.getAminoType().toString();

			if (this.motifDistanceMap.containsKey(distanceKey)) {

				double d = Calc.getDistanceFast(targetAa.getAtom(AtomTypes.CA.getAtomName()),
						aa.getAtom(AtomTypes.CA.getAtomName()));

				if (d <= this.motifDistanceMap.get(distanceKey)
						+ ParameterProvider.getInstance().getDistanceTolerance()) {

					filteredEnv.add(aa);
				}
			}
		}

		return filteredEnv;
	}

	/**
	 * finds permutation with lowest RMSD
	 * 
	 * @param permutations
	 * @throws StructureException
	 */
	private Hit findLowestRmsd(List<List<AminoAcid>> permutations) {

		double lowestRmsd = 999.0;
		Hit hit = null;

		// // TODO algorithm visualization
		// int permutationVisCounter = 1;
		// iterate over each permutation
		for (List<AminoAcid> p : permutations) {

			// check if order is identical to motif amino acids if no exchange
			if (ParameterProvider.getInstance().getExchangesByResNum().isEmpty()) {

				if (!isSameAminoAcidsOrder(p, this.motifAminoAcids)) {

					continue;
				}
			} else {

				// check if exchange is valid
				if (!isValidExchange(p, this.motifAminoAcids)) {

					continue;
				}
			}

			try {

				// get atom sets
				List<Atom[]> exchangeAtomSets = getAtomSets(p, getCopyOfAminoAcids(this.motifAminoAcids));
				if (exchangeAtomSets == null) {

					continue;
				}

				Atom[] pAtomSet = exchangeAtomSets.get(0);
				Atom[] motifAtomSet = exchangeAtomSets.get(1);

				// create SVD superimposer
				SVDSuperimposer svd = new SVDSuperimposer(pAtomSet, motifAtomSet);

				// get rotation matrix
				Matrix rotation = svd.getRotation();

				// get translation
				Atom translation = svd.getTranslation();

				// rotate and shift every atom of motif atom set
				for (Atom a : motifAtomSet) {

					Calc.rotate(a, rotation);
					Calc.shift(a, translation);
				}

				// calculate RMSD
				double rmsd = SVDSuperimposer.getRMS(motifAtomSet, pAtomSet);

				// // TODO algorithm visualization
				// List<String> currentPermutationVis = new ArrayList<>();
				// List<String> currentPermutationVisMotif = new ArrayList<>();
				// for (Atom a : motifAtomSet) {
				// currentPermutationVisMotif.add(a.toPDB());
				// }
				// for (Atom a : pAtomSet) {
				//
				// currentPermutationVis.add(a.toPDB());
				// }
				// try {
				// FileUtils.writeLines(
				// new File("perm_"
				// + String.format("%04d", viscounter)
				// + "_"
				// + String.format("%04d",
				// permutationVisCounter) + ".pdb"),
				// currentPermutationVis);
				// // FileUtils
				// // .writeLines(
				// // new File("perm_"
				// // + String.format("%04d", viscounter)
				// // + "_"
				// // + String.format("%04d",
				// // permutationVisCounter)
				// // + "_motif.pdb"),
				// // currentPermutationVisMotif);
				// } catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// permutationVisCounter++;

				// TODO debug print
				// System.out.println(rmsd + " " + p);
				// for (AminoAcid aa : this.motifAminoAcids) {
				//
				// for (Atom aaa : aa.getAtoms()) {
				//
				// System.out.print(aaa.toPDB());
				// }
				// }

				// check if RMSD is sufficient
				if (rmsd > lowestRmsd) {

					continue;
				}

				// store lowest RMSD and best permutation
				lowestRmsd = rmsd;
				this.lastBestPermutation = p;

				// get hit
				hit = getHit(p, rmsd);
				hit.setAlignedSeq(getSequenceFromAminoAcidList(p));
				hit.setRotation(rotation);
				hit.setShift(translation);

			} catch (StructureException e) {

				Starter.LOG.fine(p.toString() + " " + e.getMessage());
			}
		}
		return hit;
	}

	/**
	 * gets atom sets for alignment of exchanged residues
	 * 
	 * @param aminoAcids1
	 * @param aminoAcids2
	 *            (motif amino acids)
	 * @return list of two atom sets
	 */
	private List<Atom[]> getAtomSets(List<AminoAcid> aminoAcids1, List<AminoAcid> aminoAcids2) {

		// initialize atom sets
		List<Atom> atomSet1 = new ArrayList<>();
		List<Atom> atomSet2 = new ArrayList<>();

		// iterate over amino acids 1 and 2
		for (int i = 0; i < aminoAcids1.size(); i++) {

			AminoAcid aa1 = aminoAcids1.get(i);
			AminoAcid aa2 = aminoAcids2.get(i);

			// if residue is valid exchange and not of same type consider only
			// CA atoms
			// TODO consider backbone atoms?
			if (ParameterProvider.getInstance().getExchangesByResNum().containsKey(aa2.getResidueNumber().getSeqNum())
					&& (aa1.getAminoType() != aa2.getAminoType())) {

				Atom a1 = null;
				try {
					a1 = aa1.getAtom(AtomTypes.CA.getAtomName());
				} catch (StructureException e) {

					Starter.LOG.finest("Missing CA atom @" + InputOutputUtils.getAminoAcidString(aa1) + "@"
							+ this.target + " " + e.getMessage());

					// ignore atom if missing and enabled
					if (ParameterProvider.getInstance().isIgnoreAtoms()) {

						continue;
					}

					return null;
				}

				Atom a2 = null;
				try {
					a2 = aa2.getAtom(AtomTypes.CA.getAtomName());
				} catch (StructureException e) {

					Starter.LOG.finest("Missing CA atom @" + InputOutputUtils.getAminoAcidString(aa2) + "@"
							+ this.target + " " + e.getMessage());

					// ignore atom if missing and enabled
					if (ParameterProvider.getInstance().isIgnoreAtoms()) {

						continue;
					}

					return null;
				}

				atomSet1.add(a1);
				atomSet2.add(a2);

				continue;
			}

			// consider all atoms present in aa2 for other residues
			Set<String> aa2AtomTypes = StructureUtils.getUniqueAtomList(aa2);

			for (String type : aa2AtomTypes) {

				// skip if atoms are not defined
				if (!StructureUtils.isValidAtomType(type)) {

					continue;
				}

				Atom a1 = null;
				try {
					a1 = aa1.getAtom(type);
				} catch (StructureException e) {

					Starter.LOG.finest("Missing atom " + type + "@" + InputOutputUtils.getAminoAcidString(aa1) + "@"
							+ this.target + " " + e.getMessage());

					// ignore atom if missing and enabled
					if (ParameterProvider.getInstance().isIgnoreAtoms()) {

						continue;
					}

					return null;
				}

				Atom a2 = null;
				try {
					a2 = aa2.getAtom(type);
				} catch (StructureException e) {

					Starter.LOG.finest("Missing atom " + type + "@" + InputOutputUtils.getAminoAcidString(aa2) + "@"
							+ this.target + " " + e.getMessage());

					// ignore atom if missing and enabled
					if (ParameterProvider.getInstance().isIgnoreAtoms()) {

						continue;
					}

					return null;
				}

				atomSet1.add(a1);
				atomSet2.add(a2);
			}
		}

		// convert lists to array
		List<Atom[]> exchangeAtomSets = new ArrayList<>();
		exchangeAtomSets.add(atomSet1.toArray(new Atom[atomSet1.size()]));
		exchangeAtomSets.add(atomSet2.toArray(new Atom[atomSet2.size()]));

		return exchangeAtomSets;
	}

	/**
	 * gets an identical copy (chain, amino acids, atoms) of amino acids
	 * 
	 * @return identical copy of amino acids
	 */
	private List<AminoAcid> getCopyOfAminoAcids(List<AminoAcid> aminoAcids) {

		final List<AminoAcid> copyOfLastBestPermutation = new ArrayList<>();

		for (AminoAcid aa : aminoAcids) {

			final List<Atom> copyOfLastBestPermutationAtoms = new ArrayList<>();
			for (Atom a : aa.getAtoms()) {

				copyOfLastBestPermutationAtoms.add((Atom) a.clone());
			}

			AminoAcid aaClone = (AminoAcid) aa.clone();
			aaClone.setChain((Chain) aa.getChain().clone());
			aaClone.setAtoms(copyOfLastBestPermutationAtoms);

			copyOfLastBestPermutation.add(aaClone);
		}

		return copyOfLastBestPermutation;
	}

	/**
	 * gets the environment (excluding centroid amino acid) within a defined
	 * radius around centroid amino acid
	 * 
	 * @param aminoAcids
	 * @param centroidAminoAcid
	 * @param radius
	 * @return list of amino acid around centroid amino acid
	 * @throws StructureException
	 */
	private List<AminoAcid> getEnvironment(List<AminoAcid> aminoAcids, AminoAcid centroidAminoAcid, double radius)
			throws StructureException {

		// get centroid CA atom
		Atom centroidAminoAcidCA = centroidAminoAcid.getAtom(AtomTypes.CA.getAtomName());

		// initialize sphere
		ArrayList<AminoAcid> sphere = new ArrayList<>();

		// iterate over all given amino acids
		for (int i = 0; i < aminoAcids.size(); i++) {

			// get current amino acids CA atom
			AminoAcid currentAminoAcid = aminoAcids.get(i);
			Atom currentAminoAcidCA = currentAminoAcid.getAtom(AtomTypes.CA.getAtomName());

			// calculate distance to centroid CA atom
			if (Calc.getDistanceFast(centroidAminoAcidCA, currentAminoAcidCA) <= radius
					&& Calc.getDistanceFast(centroidAminoAcidCA, currentAminoAcidCA) != 0.0) {

				// add to sphere
				sphere.add(currentAminoAcid);
			}
		}

		return sphere;
	}

	/**
	 * gets full atom set of amino acids
	 * 
	 * @param aminoAcids
	 * @return full atom set
	 */
	private Atom[] getFullAtomSet(List<AminoAcid> aminoAcids) {

		List<Atom> fullAtoms = new ArrayList<>();
		for (AminoAcid aa : aminoAcids) {

			for (Atom a : aa.getAtoms()) {

				fullAtoms.add(a);
			}
		}

		return fullAtoms.toArray(new Atom[fullAtoms.size()]);
	}

	private Hit getHit(List<AminoAcid> aminoAcids, double rmsd) {

		Hit hit = new HitImpl();
		for (AminoAcid aa : aminoAcids) {

			HitAminoAcid hAa = new HitAminoAcidImpl();
			hAa.setChainId(aa.getChainId().charAt(0));
			hAa.setResidueNumber(aa.getResidueNumber().getSeqNum().intValue());
			hAa.setResidueType(aa.getAminoType().charValue());

			Character insCode = aa.getResidueNumber().getInsCode();

			if (insCode == null) {

				hAa.setInsCode(null);
			} else {

				hAa.setInsCode(insCode);
			}

			for (Atom a : aa.getAtoms()) {

				hAa.addAtom(new HitAtomImpl(a.getName(), a.getX(), a.getY(), a.getZ()));
			}

			hit.addAminoAcid(hAa);
		}

		hit.setRmsd(rmsd);
		hit.setPdbId(this.target);

		if (StructureUtils.isSameChain(hit)) {

			hit.setType("intra");
		} else {

			hit.setType("inter");
		}

		// map EC number if enabled
		if (ParameterProvider.getInstance().isEcMapping()) {

			// check if annotations for the target are already stored
			if (EcStorage.getInstance().getStorage().containsKey(this.target)) {

				List<EcAnnotaion> annotations = EcStorage.getInstance().getStorage().get(this.target);

				// assign EC annotation
				String ecAcc = StructureUtils.assignEcAnnotation(annotations, hit);

				hit.setEcNumber(ecAcc);
			} else {

				// retrieve EC annotations for the target
				List<EcAnnotaion> annotations = StructureUtils.getEcAnnotation(this.target);

				String ecNumber = StructureUtils.getEcNumber(hit);
				hit.setEcNumber(ecNumber);

				// store annotations
				EcStorage.getInstance().addEntry(this.target, annotations);
			}
		}

		// map Pfam accession if enabled
		if (ParameterProvider.getInstance().isPfamMapping()) {

			// check if annotations for the target are already stored
			if (PfamStorage.getInstance().getStorage().containsKey(this.target)) {

				// extract available annotations
				List<PfamAnnotation> annotations = PfamStorage.getInstance().getStorage().get(this.target);

				// assign Pfam annotation
				String pfamAcc = StructureUtils.assignPfamAnnotation(annotations, hit);

				// set Pfam annotation of hit
				hit.setPfamAcc(pfamAcc);

			} else {

				// retrieve Pfam annotations for the target
				List<PfamAnnotation> annotations = StructureUtils.getPfamAnnotations(this.target);

				String pfamAcc = StructureUtils.assignPfamAnnotation(annotations, hit);
				hit.setPfamAcc(pfamAcc);

				// store annotations
				PfamStorage.getInstance().addEntry(this.target, annotations);
			}
		}

		if (ParameterProvider.getInstance().isTitleMapping()) {

			// set title if existent
			String title = this.targetHeader.getTitle();
			if (title != null) {
				// delete CSV separator from title
				hit.setTitle(title.replaceAll(",", ""));
				// delete escape sequence from title
				hit.setTitle(title.replaceAll("\"", ""));
			}
		}

		if (ParameterProvider.getInstance().isGapSeqMapping()) {

			GapSequenceMapper mapper = new GapSequenceMapper(hit, this.targetAminoAcids);

			hit.setGapSequence(mapper.mapGapSeq());
		}

		return hit;
	}

	/**
	 * gets sequence from a list of amino acids
	 * 
	 * @param aminoAcids
	 * @return sequence as string
	 */
	private String getSequenceFromAminoAcidList(List<AminoAcid> aminoAcids) {

		StringBuilder seq = new StringBuilder();
		for (AminoAcid aa : aminoAcids) {

			seq.append(aa.getAminoType());
		}

		return seq.toString();
	}

	/**
	 * initializes search
	 * 
	 * @throws StructureException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private void initialize() throws FileNotFoundException, IOException, StructureException {

		// initialize target parser
		StructureParser targetParser;

		// check if target is file
		File targetFile = new File(this.target);
		if (targetFile.exists()) {

			// read target from file
			targetParser = new StructureParser(null, new File(this.target));

			// assume filename as target PDB-ID
			this.target = targetFile.getName().toString();

		} else {

			// read target from PDB
			targetParser = new StructureParser(this.target, null);

			// ensure upper case target name
			this.target = this.target.toUpperCase();
		}

		// TODO automatically fetch superseded
		// // get target amino acids
		// try {

		// get target amino acids
		this.targetAminoAcids = targetParser.getAllAminoAcids();

		// } catch (IOException e) {
		//
		// Starter.LOG
		// .info("entry not existing, trying to fetch current if superseded");
		//
		// String newEntry = PDBStatus.getCurrent(this.target);
		// if (newEntry != null) {
		//
		// this.target = newEntry;
		// initialize();
		// } else {
		//
		// throw new IOException(
		// "could not find PDB 3ltx in file system and also could not
		// download");
		// }
		// }
		//
		// // store structure
		// Structure s = targetParser.getStructure();

		// get target header
		this.targetHeader = targetParser.getStructure().getPDBHeader();

		// get EC number
		// if (ParameterProvider.getInstance().isEcMapping()) {
		//
		// // try to load EC number
		// if (EcStorage.getInstance().getStorage().containsKey(this.target)) {
		//
		// this.ecNumber = EcStorage.getInstance().getStorage()
		// .get(this.target);
		// } else {
		//
		// this.ecNumber = StructureUtils.getEcNumber(this.target);
		// EcStorage.getInstance().addEntry(this.target, this.ecNumber);
		// }
		// }

		// parse motif structure
		StructureParser motifParser = new StructureParser(null, new File(this.motifFile));
		this.motifAminoAcids = motifParser.getAllAminoAcids();

		// // store copy of original motif amino acids
		// this.originalMotifAminoAcids =
		// getCopyOfAminoAcids(this.motifAminoAcids);

		// get motif distribution
		this.motifDistribution = StructureUtils.getAminoAcidDistribution(this.motifAminoAcids);

		// get motif expansion
		this.motifExpansion = StructureUtils.getMaxSquaredExtent(this.motifAminoAcids);

		// set motif distribution if no exchanges defined
		if (this.exchangeDistributions.isEmpty()) {

			this.exchangeDistributions.add(this.motifDistribution);
		}

	}

	/**
	 * tests if a hit is already in results
	 * 
	 * @param aminoAcids
	 * @return true if amino acids are already a hit
	 */
	private boolean isAlreadyHit(Hit test) {

		for (Hit hit : this.results) {

			if (hit.equals(test)) {

				return true;
			}
		}
		return false;
	}

	/**
	 * tests if distribution is reachable or identical solution
	 * 
	 * @param distribution
	 * @param identical
	 * @return true if solution is reachable or identical
	 */
	private boolean isPossibleSolution(Map<Character, Integer> distribution, boolean identical) {

		// iterate over all keys in reference distribution
		for (Character c : this.motifDistribution.keySet()) {

			// if distribution contains key of reference distribution
			if (distribution.keySet().contains(c)) {

				// greater or equal distribution
				if (identical) {

					// if value is greater or equal than reference value
					if (distribution.get(c) != null && distribution.get(c).equals(this.motifDistribution.get(c))) {

						continue;
					} else {

						// immediately decline
						return false;
					}
				} else {
					if (distribution.get(c) != null
							&& (distribution.get(c).compareTo(this.motifDistribution.get(c)) >= 0)) {

						continue;
					} else {

						// immediately decline
						return false;
					}
				}
			} else {

				// immediately decline
				return false;
			}
		}

		// accept
		return true;
	}

	/**
	 * checks if two lists of amino acids are of same order
	 * 
	 * @param aminoAcids1
	 * @param aminoAcids2
	 * @return true if order of amino acids lists is identical
	 */
	private boolean isSameAminoAcidsOrder(List<AminoAcid> aminoAcids1, List<AminoAcid> aminoAcids2) {

		// determine order of amino acid lists
		String order1 = "";
		for (AminoAcid aa : aminoAcids1) {

			order1 += aa.getAminoType().toString();
		}

		String order2 = "";
		for (AminoAcid aa : aminoAcids2) {

			order2 += aa.getAminoType().toString();
		}

		// accept if order is identical
		if (order1.equals(order2)) {

			return true;
		} else {

			return false;
		}
	}

	/**
	 * checks if aminoAcids2 are valid exchanges for aminoAcids1
	 * 
	 * @param aminoAcids1
	 * @param aminoAcids2
	 * @return true if valid exchange
	 */
	private boolean isValidExchange(List<AminoAcid> aminoAcids1, List<AminoAcid> aminoAcids2) {

		// check for equal size
		if (aminoAcids1.size() != aminoAcids2.size()) {

			return false;
		}

		boolean isValid = true;

		// iterate over amino acids 1 and 2
		for (int i = 0; i < aminoAcids1.size(); i++) {

			AminoAcid a1 = aminoAcids1.get(i);
			AminoAcid a2 = aminoAcids2.get(i);

			// continue if amino acids are of the same type
			if (a1.getAminoType() == a2.getAminoType()) {

				continue;
			}

			// determine type of amino acid 1
			char aType1 = a1.getAminoType();

			// skip if no exchanges were defined
			if (!ParameterProvider.getInstance().getExchangesByResNum()
					.containsKey(a2.getResidueNumber().getSeqNum())) {

				isValid = false;
				continue;
			}

			// get allowed exchanges for amino acid 2
			Set<Character> allowedExchanges = ParameterProvider.getInstance().getExchangesByResNum()
					.get(a2.getResidueNumber().getSeqNum());

			boolean match = false;
			for (Character c : allowedExchanges) {

				if (c == aType1) {

					match = true;
					break;
				}
			}

			if (!match) {
				isValid = false;
			}
		}

		return isValid;
	}

	/**
	 * output last hit as PDB file and align if enabled
	 * 
	 * @param align
	 * @throws StructureException
	 * @throws IOException
	 */
	private void outputHit(boolean align) throws StructureException {

		// get copy of last best permutation
		List<AminoAcid> copyOfLastBestPermutation = getCopyOfAminoAcids(this.lastBestPermutation);

		// initialize atom sets
		List<Atom[]> atomSets = getAtomSets(copyOfLastBestPermutation, getCopyOfAminoAcids(this.motifAminoAcids));
		Atom[] pAtomSet = atomSets.get(0);
		Atom[] motifAtomSet = atomSets.get(1);

		// create SVD superimposer
		SVDSuperimposer svd = new SVDSuperimposer(motifAtomSet, pAtomSet);

		// conserve side chain atoms if enabled
		if (ParameterProvider.getInstance().isConserve()) {

			pAtomSet = getFullAtomSet(copyOfLastBestPermutation);
		}

		// align to motif if enabled
		if (align) {

			// get rotation matrix
			Matrix rotation = svd.getRotation();

			// get translation
			Atom translation = svd.getTranslation();

			// rotate and shift every atom of motif atom set
			for (Atom a : pAtomSet) {

				Calc.rotate(a, rotation);
				Calc.shift(a, translation);
			}
		}

		try {

			// initialize structure writer
			StructureWriter sw = new StructureWriter(
					InputOutputUtils.getOutputFilename(this.results.get(this.results.size() - 1)), pAtomSet);
			// write structure
			sw.write();

		} catch (IOException e) {

			Starter.LOG.warning(e.getMessage());
		}

	}

	/**
	 * calculated all permutations of amino acids
	 * 
	 * @param items
	 * @param prev
	 * @return all combinations of amino acids
	 */
	private List<List<AminoAcid>> permutateAminoAcids(List<AminoAcid> items, List<AminoAcid> prev) {

		List<List<AminoAcid>> permutations = new ArrayList<>();

		if (items.isEmpty()) {

			List<AminoAcid> copy = new ArrayList<>();

			for (int i = 0; i < prev.size(); i++) {
				copy.add(prev.get(i));
			}

			permutations.add(copy);
		}

		for (int i = 0; i < items.size(); i++) {

			AminoAcid first = items.get(0);
			AminoAcid ith = items.get(i);
			items.set(i, first);
			items.remove(0);

			List<AminoAcid> copy = new ArrayList<>();
			for (int j = 0; j < items.size(); j++) {
				copy.add(items.get(j));
			}

			prev.add(ith);
			List<List<AminoAcid>> thisPermutations = permutateAminoAcids(copy, prev);
			prev.remove(prev.size() - 1);
			permutations.addAll(thisPermutations);
			items.add(0, ith);
		}

		return permutations;
	}

	/**
	 * pre processing of target structure and keeps only amino acid types
	 * present in motif
	 */
	private void preprocessStructure() {

		List<AminoAcid> preprocessedTarget = new ArrayList<>();

		for (AminoAcid aa : this.targetAminoAcids) {

			if (!this.motifDistribution.containsKey(aa.getAminoType())) {

				continue;
			} else {

				try {

					aa.getAtom(AtomTypes.CA.getAtomName());
				} catch (StructureException e) {

					Starter.LOG
							.fine(this.target + "@" + InputOutputUtils.getAminoAcidString(aa) + " " + e.getMessage());
				}

				preprocessedTarget.add(aa);
			}
		}

		this.preprocessedTarget = preprocessedTarget;
	}
}
