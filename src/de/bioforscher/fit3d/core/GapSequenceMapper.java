/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.core;

import java.util.Collections;
import java.util.List;

import org.biojava.bio.structure.AminoAcid;

import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;
import de.bioforscher.fit3d.utils.StructureUtils;

public class GapSequenceMapper {

	private final List<AminoAcid> targetAminoAcids;
	private final Hit hit;

	public GapSequenceMapper(Hit hit, List<AminoAcid> targetAminoAcids) {

		this.hit = hit;
		this.targetAminoAcids = targetAminoAcids;
	}

	public Hit getHit() {
		return this.hit;
	}

	public List<AminoAcid> getTargetAminoAcids() {
		return this.targetAminoAcids;
	}

	/**
	 * maps the sequence gap between the hit amino acids
	 * 
	 * @param hit
	 * @param targetAminoAcids
	 * 
	 */
	public String mapGapSeq() {

		// TODO Why are strange combinations occurring? E.g. for DY YY occurs.

		// ignore inter-molecular hits
		if (!StructureUtils.isSameChain(this.hit)) {

			return null;
		}

		// get chain ID of intra-molecular hit
		String hitChainId = String.valueOf(StructureUtils.getUniqueChainList(
				this.hit).toArray()[0]);

		// get all amino acids of hit and sort according to sequence number
		List<HitAminoAcid> hAas = this.hit.getAminoAcids();
		Collections.sort(hAas);

		// get minimal and maximal residue number of hit
		int minHitAaResNum = hAas.get(0).getResidueNumber();
		int maxHitAaResNum = hAas.get(hAas.size() - 1).getResidueNumber();

		// extract gap
		StringBuilder sb = new StringBuilder();
		for (AminoAcid aa : this.targetAminoAcids) {

			int resNum = aa.getResidueNumber().getSeqNum();
			String chainId = aa.getChainId();

			if ((resNum >= minHitAaResNum) && (resNum <= maxHitAaResNum)
					&& chainId.equals(hitChainId)) {

				sb.append(aa.getAminoType());
			}
		}

		return sb.toString();
	}

}
