/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.statistics;

/**
 * A enumeration representing residue abundances.
 * 
 * @author fkaiser
 *
 */
public enum Abundances {

	STARK {
		@Override
		public double[] getValues() {

			return new double[] { 8.19, 4.62, 4.66, 5.79, 1.64, 3.71, 5.99,
					7.96, 2.33, 5.42, 8.39, 6.04, 2.03, 3.98, 4.59, 6.33, 6.15,
					1.54, 3.65, 7 };
		}
	};

	public abstract double[] getValues();
}
