/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.statistics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.StructureException;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.classification.utils.ClassifierUtils;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;

/**
 * A class representing the statistical model for local similarity significance
 * calculation based on
 * 
 * Stark, A.; Sunyaev, S. & Russell, R. B. A model for statistical significance
 * of local similarities in structure J. Mol. Biol., 2003, 326, 1307-1316 *
 * 
 * @author fkaiser
 * 
 */
public class StarkModel extends StatisticalModel {

	private static final double A_0 = 2.678E9;
	private static final double A_2 = 0.1277E-6;

	private static final double A_3 = 1.79E-3;

	private static final double C_2 = 0.196;
	private static final double C_3 = 0.094;
	private List<Hit> filteredResults;

	/**
	 * calculates p-value for each hit
	 * 
	 * @param totalResults
	 * @throws StructureException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@Override
	public void calculatePvalues(List<Hit> totalResults) throws FileNotFoundException, IOException, StructureException {

		this.filteredResults = new ArrayList<>();
		this.filteredResults = removeExchangeHits(totalResults);

		totalResults.removeAll(this.filteredResults);

		// iterate over all results
		for (Hit h : this.filteredResults) {

			// number of residues of motif
			int n = h.getAminoAcids().size();

			double a;

			double b;

			// correction factor 1
			double c = 1.0;

			// correction factor 2
			double d = 0.0;

			// for more than two residues
			if (n > 2) {

				a = A_0 * Math.pow(A_3, n);

				b = 2.93 * n - 5.88;

				// multiply abundances and calculate correction factors
				for (HitAminoAcid hAa : h.getAminoAcids()) {

					a = a * (Abundances.STARK.getValues()[ClassifierUtils.getAminoAcidIndex(hAa.getResidueType())]);

					String[] atomNames = ParameterProvider.getInstance().getAtoms();

					int nAtoms = 0;
					for (String atomName : atomNames) {

						try {

							if (hAa.getAtom(atomName.replaceAll(" ", "")) != null) {

								nAtoms++;
							}
						} catch (StructureException e) {

							Starter.LOG.finest(e.getMessage());
						}
					}

					if (nAtoms == 2) {

						c = c * C_2;
						d = d + 2;

					}
					if (nAtoms > 2) {

						c = c * C_3;
						d = d + 3;
					}

				}

			} else {

				a = A_0 * A_2;
				b = 0.97;

				for (HitAminoAcid hAa : h.getAminoAcids()) {

					a = a * (Abundances.STARK.getValues()[ClassifierUtils.getAminoAcidIndex(hAa.getResidueType())]);

					String[] atomNames = ParameterProvider.getInstance().getAtoms();

					int nAtoms = 0;
					for (String atomName : atomNames) {

						try {

							if (hAa.getAtom(atomName.replaceAll(" ", "")) != null) {

								nAtoms++;
							}
						} catch (StructureException e) {

							Starter.LOG.finest(e.getMessage());
						}
					}

					if (nAtoms == 2) {

						c = c * C_2;
						d = d + 2;

					}
					if (nAtoms > 2) {

						c = c * C_3;
						d = d + 3;
					}
				}
			}

			// calculate p-value with or without dependence atom correction
			if ((a * c * Math.pow(h.getRmsd(), (b + d))) < (a * Math.pow(h.getRmsd(), b))) {

				h.setPvalue(1 - Math.exp(-(a * c * Math.pow(h.getRmsd(), (b + d)))));

			} else {

				h.setPvalue(1 - Math.exp(-((a * Math.pow(h.getRmsd(), b)))));
			}
		}

		totalResults.addAll(this.filteredResults);
	}
}
