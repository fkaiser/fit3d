/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.statistics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.StructureException;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.io.parser.StructureParser;
import de.bioforscher.fit3d.model.Hit;

public abstract class StatisticalModel {

	public abstract void calculatePvalues(List<Hit> totalResults)
			throws StatisticalModelException, FileNotFoundException, IOException, StructureException;

	/**
	 * removes all hits containing one or more PSEs from the result list
	 * 
	 * @param totalResults
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws StructureException
	 */
	protected List<Hit> removeExchangeHits(List<Hit> totalResults)
			throws FileNotFoundException, IOException, StructureException {

		List<Hit> filteredResults = new ArrayList<>();
		StructureParser motifParser = new StructureParser(null, new File(ParameterProvider.getInstance().getMotif()));

		StringBuilder seq = new StringBuilder();

		for (AminoAcid aa : motifParser.getAllAminoAcids()) {

			seq.append(aa.getAminoType());
		}

		for (Hit h : totalResults) {

			if (h.getAlignedSeq().equals(seq.toString())) {

				filteredResults.add(h);
			}
		}

		return filteredResults;
	}
}
