/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.statistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.biojava.bio.structure.StructureException;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.model.Hit;
import weka.gui.Main;

/**
 * A class representing the variables for a statistical model based on
 * 
 * Fofanov, V.; Chen, B.; Bryant, D.; Moll, M.; Lichtarge, O.; Kavraki, L. &
 * Kimmel, M.: A statistical model to correct systematic bias introduced by
 * algorithmic thresholds in protein structural comparison algorithms
 * Bioinformatics and Biomedicine Workshops, 2008. BIBMW 2008. IEEE
 * International Conference on, 2008, 1-8
 * 
 * @author fkaiser
 * 
 */
public final class FofanovModel extends StatisticalModel {

	public static final int REF_SIZE = 31133;

	// number of PDB protein structures as of 2015-07-15
	public static final int SAMPLE_SIZE = 100000;
	public static final int START_RMSD = 0;

	private static FofanovModel instance = new FofanovModel();

	public static FofanovModel getInstance() {

		return instance;
	}

	// private final NormalDistribution nd = new NormalDistribution();
	private int ns = ParameterProvider.getInstance().getNs();
	private int gs = ParameterProvider.getInstance().getGs();
	private final String rmsdTempOutputPath = Starter.TMP_DIR + "/" + ParameterProvider.getInstance().getTimestamp()
			+ "_rmsd.fit";
	private final String pvalueOutputPath = Starter.TMP_DIR + "/" + ParameterProvider.getInstance().getTimestamp()
			+ "_pvalues.fit";
	private final List<Double> pvalues = new ArrayList<>();

	private List<Hit> filteredResults;

	private FofanovModel() {

	}

	public void assignPvalues(List<Hit> totalResults) throws StatisticalModelException {

		totalResults.removeAll(this.filteredResults);

		if (this.filteredResults.size() != this.pvalues.size()) {

			throw new StatisticalModelException("p-values could not be assigned");
		}

		for (int i = 0; i < this.filteredResults.size(); i++) {

			this.filteredResults.get(i).setPvalue(this.pvalues.get(i));
		}

		totalResults.addAll(this.filteredResults);
	}

	@Override
	public void calculatePvalues(List<Hit> totalResults)
			throws StatisticalModelException, FileNotFoundException, IOException, StructureException {

		this.filteredResults = new ArrayList<>();
		this.filteredResults = removeExchangeHits(totalResults);

		final NumberFormat nf = NumberFormat.getInstance(Starter.LOCALIZATION);
		final DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern("0.0000");

		try {

			// write RMSD values to temporary file
			BufferedWriter out = new BufferedWriter(new FileWriter(this.rmsdTempOutputPath));

			out.write("RMSD");
			out.newLine();
			for (Hit h : this.filteredResults) {

				out.write(df.format(h.getRmsd()));
				out.newLine();
			}
			out.close();

			// copy R script to temporary directory
			URL scriptUri = Main.class.getResource("/de/bioforscher/fit3d/statistics/statistical_model.R");
			File tempScriptFile = new File(
					Starter.TMP_DIR + "/" + ParameterProvider.getInstance().getTimestamp() + "_statistical_model.fit");
			FileUtils.copyURLToFile(scriptUri, tempScriptFile);

			// build process
			ProcessBuilder pb = new ProcessBuilder("Rscript", tempScriptFile.getAbsolutePath(),
					String.valueOf(ParameterProvider.getInstance().getRefSize()), String.valueOf(this.ns),
					String.valueOf(this.gs), String.valueOf(START_RMSD),
					String.valueOf(ParameterProvider.getInstance().getMaxRmsd()), String.valueOf(SAMPLE_SIZE),
					this.rmsdTempOutputPath, this.pvalueOutputPath);

			// start R calculation
			Process p = pb.start();

			// TODO debug print
			// Scanner sc = new Scanner(p.getErrorStream());
			// sc.useDelimiter("\\Z");
			// Starter.LOG.info(sc.next());
			// sc.close();

			// wait for R calculation to finish
			int exitStatus = p.waitFor();
			if (exitStatus != 0) {
				throw new StatisticalModelException("p-value calculation ended with exit status: " + exitStatus);
			}

			// read R output
			BufferedReader br = new BufferedReader(new FileReader(this.pvalueOutputPath));
			String s;
			while ((s = br.readLine()) != null) {

				this.pvalues.add(Double.valueOf(s));
			}
			br.close();

			// delete script file
			tempScriptFile.delete();

		} catch (IOException e) {

			Starter.LOG.warning(e.getMessage());
		} catch (InterruptedException e) {
			Starter.LOG.warning(e.getMessage());
		}
	}

	public synchronized void countGs() {
		this.gs++;
	}

	public synchronized void countNs() {
		this.ns++;
	}

	// public void estimateBandwidth(double[] data) {
	//
	// // if (data.length < 2) {
	// //
	// // throw new StatisticalModelException(
	// // "Unable to calculate bandwith, need at least three data points.");
	// // }
	//
	// double h0 = getHnorm(data);
	// double v0 = getSj(data, h0);
	// System.out.println(getPhi6(2.0));
	// System.out.println(getPhi4(2.0));
	//
	// System.out.println(h0);
	// }

	public int getGs() {
		return this.gs;
	}

	public int getNs() {
		return this.ns;
	}

	public List<Double> getPvalues() {
		return this.pvalues;
	}

	// private double getHnorm(double[] data) {
	//
	// double standardDev = Math.sqrt(getVariance(data));
	// return standardDev * Math.pow((4.0 / (3.0 * data.length)), (1.0 / 5.0));
	// }
	//
	// private double getMean(double[] data) {
	//
	// double sum = 0.0;
	// for (double d : data) {
	//
	// sum += d;
	// }
	//
	// // TODO debug print
	// System.out.println("mean " + (sum / data.length));
	// return sum / data.length;
	// }
	//
	// private double getPhi4(double d) {
	//
	// return (Math.pow(d, 4.0) - 6 * Math.pow(d, 2.0) + 3.0)
	// * this.nd.density(d);
	// }
	//
	// private double getPhi6(double d) {
	//
	// return (Math.pow(d, 6.0) - 15 * Math.pow(d, 4.0) + 45.0
	// * Math.pow(d, 2.0) - 15.0)
	// * this.nd.density(d);
	// }
	//
	// private double getSj(double[] data, double h0) {
	//
	// double lambda = StatUtils.percentile(data, 0.75)
	// - StatUtils.percentile(data, 0.25);
	// double a = 0.92 * lambda * Math.pow(data.length, (-1.0 / 7.0));
	// double b = 0.912 * lambda * Math.pow(data.length, (-1.0 / 9.0));
	// return 0;
	// }
	//
	// private double getVariance(double[] data) {
	//
	// double mean = getMean(data);
	// double temp = 0.0;
	// for (double d : data) {
	//
	// temp += Math.pow(d - mean, 2);
	// }
	//
	// // data.length - 1 ?
	// System.out.println("variance " + (temp / (data.length - 1)));
	// return temp / (data.length - 1);
	// }

	// private double hnorm(double[] data) {
	//
	// double standardDeviation = Math.sqrt(wvar(data));
	// return 0;
	// }

}
