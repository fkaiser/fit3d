/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.classification.scales;

public enum Scales {

	HYDROPHOBICITY_KYTE_DOOLITTLE {
		@Override
		public double[] getValues() {

			return new double[] { 1.8, -4.5, -3.5, -3.5, 2.5, -3.5, -3.5, -0.4,
					-3.2, 4.5, -3.8, -3.9, 1.9, 2.8, -1.6, -0.8, -0.7, -0.9,
					-1.3, 4.2 };
		}
	},
	POLARITY_GRANTHAM {
		@Override
		public double[] getValues() {

			return new double[] { 8.1, 10.5, 11.6, 13, 5.5, 10.5, 12.3, 9,
					10.4, 5.2, 4.9, 11.3, 5.7, 5.2, 8, 9.2, 8.6, 5.4, 6.2, 5.9 };
		}
	},
	MUTABILITY_DAYHOFF {
		@Override
		public double[] getValues() {

			return new double[] { 100.000, 65.000, 134.000, 106.000, 20.000,
					93.000, 102.000, 49.000, 66.000, 96.000, 40.000, 56.000,
					94.000, 41.000, 56.000, 120.000, 97.000, 18.000, 41.000,
					74.000 };
		}
	},
	BULKINESS_ZIMMERMANN {
		@Override
		public double[] getValues() {

			return new double[] { 11.500, 14.280, 12.820, 11.680, 13.460,
					14.450, 13.570, 3.400, 13.690, 21.400, 21.400, 15.710,
					16.250, 19.800, 17.430, 9.470, 15.770, 21.670, 18.030,
					21.570 };
		}
	},
	FLEXIBILITY_BHASKARAN {
		@Override
		public double[] getValues() {

			return new double[] { 0.360, 0.530, 0.460, 0.510, 0.350, 0.490,
					0.500, 0.540, 0.320, 0.460, 0.370, 0.470, 0.300, 0.310,
					0.510, 0.510, 0.440, 0.310, 0.420, 0.390 };
		}
	},
	BURIED_ROSE {
		@Override
		public double[] getValues() {

			return new double[] { 86.600, 162.200, 103.300, 97.800, 132.300,
					119.200, 113.900, 62.900, 155.800, 158.000, 164.100,
					115.500, 172.900, 194.100, 92.900, 85.600, 106.500,
					224.600, 177.700, 141.000 };
		}
	};

	public abstract double[] getValues();
}
