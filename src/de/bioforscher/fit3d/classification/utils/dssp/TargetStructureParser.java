/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.classification.utils.dssp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Chain;
import org.biojava.bio.structure.Group;
import org.biojava.bio.structure.Structure;

public class TargetStructureParser extends StructureParser {

	private String pdbId;
	private Structure structure;
	private List<AminoAcid> allAminoAcids;

	public TargetStructureParser(String pdbPath, String pdbId)
			throws IOException {
		super(pdbPath);
		this.pdbId = pdbId;
		parse();
	}

	public List<AminoAcid> getAllAminoAcids() {
		return this.allAminoAcids;
	}

	public String getPdbId() {
		return this.pdbId;
	}

	public Structure getStructure() {
		return this.structure;
	}

	@Override
	public void parse() throws IOException {
		List<AminoAcid> allAminoAcids = new ArrayList<>();

		this.structure = this.pdbReader.getStructureById(this.pdbId
				.toLowerCase());

		if (this.structure != null) {

			// get a list of all chains of the first model
			List<Chain> chainsOfModel = this.structure.getModel(0);

			// iterate over all chains of the model
			for (Chain c : chainsOfModel) {

				// assemble list of all amino acids
				for (Group g : c.getAtomGroups()) {

					if (g instanceof AminoAcid) {

						allAminoAcids.add((AminoAcid) g);
					}
				}
			}
		}

		this.allAminoAcids = allAminoAcids;
	}

	public void setPdbId(String pdbId) {
		this.pdbId = pdbId;
	}
}
