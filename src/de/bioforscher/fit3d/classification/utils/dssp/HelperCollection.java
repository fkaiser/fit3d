/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.classification.utils.dssp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.StatUtils;
import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Chain;
import org.biojava.bio.structure.Group;
import org.biojava.bio.structure.ResidueNumber;
import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.StructureException;

public class HelperCollection {

	public static final Map<String, String> SECONDARY_STRUCTURES = new HashMap<String, String>();
	static {
		SECONDARY_STRUCTURES.put("G", "H"); // 3-10-helix
		SECONDARY_STRUCTURES.put("H", "H"); // alpha-helix
		SECONDARY_STRUCTURES.put("I", "H"); // pi-helix
		SECONDARY_STRUCTURES.put("E", "E"); // beta-sheet
		SECONDARY_STRUCTURES.put("B", "E"); // beta-bulge
	}

	/**
	 * ensures a Group has all needed information for further processing<br />
	 * throws a MissingPropertyException if a NullPointerException occurs
	 * 
	 * @param residue
	 *            the suspicious Group
	 * @param props
	 *            all property-map keys that should be present
	 * @return true if all resources are present
	 */
	public static boolean checkIntegrity(Group residue, String... props) {
		for (String prop : props)
			try {
				if (residue.getProperty(prop) == null)
					return false;
			} catch (NullPointerException e) {
				throw new RuntimeException(
						prop
								+ " of residue "
								+ residue.getPDBName()
								+ " is missing as "
								+ Thread.currentThread().getStackTrace()[1]
										.getClassName() + " called");
			}
		return true;
	}

	/**
	 * smoothing over a gaussian-weighted window of data
	 * 
	 * @param values
	 *            the input
	 * @return the smoothed value for the central data
	 */
	public static double gaussianAdvanced(double[] values) {
		if (values.length % 2 == 0) {
			throw new IllegalArgumentException(
					"array for smoothing needs to contain an odd number of values - found "
							+ values.length);
		}
		double re = 0;
		double[] weights = gaWeights(values);
		for (int i = 0; i < values.length; i++)
			re += weights[i] * values[i];
		return re;
	}

	/**
	 * calculates gaussian weights for the giving size of smoothing window
	 * 
	 * @param values
	 *            input to smooth
	 * @return weights
	 */
	public static double[] gaWeights(double[] values) {
		double sigma = Math.sqrt(values.length / 2);
		double[] weights = new double[values.length];
		for (int i = 0; i < values.length; i++) {
			weights[i] = 1
					/ (sigma * Math.sqrt(2 * Math.PI))
					* Math.pow(Math.E,
							-0.5 * Math.pow((i - values.length / 2) / sigma, 2));
		}

		double sum = 1 - StatUtils.sum(weights);
		for (int i = 0; i < values.length; i++)
			weights[i] += sum / values.length;

		return weights;
	}

	public static void smooth(Structure structure, String[] prop, int windowSize) {
		for (String s : prop)
			for (Chain c : structure.getChains()) {
				HashMap<ResidueNumber, Double> smoVal = new HashMap<ResidueNumber, Double>();
				for (Group g : c.getAtomGroups())
					if (g instanceof AminoAcid) {
						int rn = g.getResidueNumber().getSeqNum();
						ArrayList<Double> values = new ArrayList<Double>();
						for (int i = 0; i < windowSize; i++) {
							try {
								Group tmpGroup = c.getGroupByPDB(ResidueNumber
										.fromString((rn + i - (int) Math
												.floor(windowSize / 2)) + ""));
								if (checkIntegrity(tmpGroup, s)
										&& tmpGroup.getProperty(s) instanceof Double)
									values.add((Double) tmpGroup.getProperty(s));
							} catch (StructureException e) {
								// ulgy, but probably the easiest way to find
								// out whether neighboring groups exist
							}
						}
						// try smoothing with the bell-shaped curve, if values
						// are missing calculate the mean
						smoVal.put(
								g.getResidueNumber(),
								values.size() == windowSize ? gaussianAdvanced(toDoubleArray(values))
										: StatUtils.mean(toDoubleArray(values)));
					}
				for (Group g : c.getAtomGroups())
					if (g instanceof AminoAcid
							&& !smoVal.get(g.getResidueNumber()).isNaN())
						g.setProperty(s, smoVal.get(g.getResidueNumber()));
			}
	}

	/**
	 * casts a List of double values to an array
	 * 
	 * @param values
	 *            the input
	 * @return the double array
	 */
	public static double[] toDoubleArray(List<Double> values) {
		double[] re = new double[values.size()];
		for (int i = 0; i < values.size(); i++)
			re[i] = values.get(i);
		return re;
	}

}
