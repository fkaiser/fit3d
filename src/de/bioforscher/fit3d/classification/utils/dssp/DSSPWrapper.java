/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.classification.utils.dssp;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.biojava.bio.structure.AminoAcid;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;

/**
 * a wrapping class for the dssp-executable<br />
 * writes asa and loop-fraction to the property-map of each residue
 * 
 * @author S
 *
 */
public class DSSPWrapper {

	private static final String DSSP_LOCATION = "/tmp/dssp-2.0.4-linux-amd64";

	private static final int WINDOW_SIZE = 9;
	// public static void main(String[] args) throws IOException {
	//
	// TargetStructureParser tsp = new TargetStructureParser("/opt/pdb/",
	// "1MUH");
	// tsp.parse();
	// FileUtils.writeStringToFile(new File("/tmp/dssp.pdb"), tsp.getS()
	// .toPDB());
	//
	// DSSPWrapper dssp = new DSSPWrapper(tsp.parse());
	// dssp.runQuery("/tmp/dssp.pdb", "/tmp/dssp.out");
	// dssp.parseFile("/tmp/dssp.out", null);
	//
	// HelperCollection.smooth(tsp.getS(), new String[] { "loopFraction" }, 9);
	//
	// System.out.println();
	// }

	private final List<AminoAcid> aminoAcids;

	public DSSPWrapper(String pdbId) throws IOException {

		TargetStructureParser tsp = new TargetStructureParser(ParameterProvider
				.getInstance().getPdbDirectory(), pdbId);
		this.aminoAcids = tsp.getAllAminoAcids();

		FileUtils.writeStringToFile(new File(Starter.TMP_DIR + "/dssp.pdb"),
				tsp.getStructure().toPDB());

		runQuery(Starter.TMP_DIR + "/dssp.pdb", Starter.TMP_DIR + "/dssp.out");
		parseFile(Starter.TMP_DIR + "/dssp.out", null);

		HelperCollection.smooth(tsp.getStructure(),
				new String[] { "loopFraction" }, WINDOW_SIZE);
	}

	public List<AminoAcid> getAminoAcids() {
		return this.aminoAcids;
	}

	public void parseFile(String filepath, String chain) {
		try {
			for (String line : FileUtils.readLines(new File(filepath))) {
				if (line.length() != 136)
					continue;
				String chainId = line.substring(11, 12);
				String resNum = line.substring(5, 10).trim();
				String secStruc = line.substring(16, 17);

				for (AminoAcid aa : this.aminoAcids) {

					if (aa.getChainId().equals(chainId)
							&& aa.getResidueNumber().getSeqNum() == Integer
									.valueOf(resNum)) {

						aa.setProperty("secStruc", secStruc);
					}
				}
			}

			for (AminoAcid aa : this.aminoAcids) {
				aa.setProperty("loopFraction",
						HelperCollection.SECONDARY_STRUCTURES.containsKey(aa
								.getProperty("secStruc")) ? 0.0 : 1.0);
			}

		} catch (IOException e) {

		}
	}

	public void runQuery(String inputPath, String outputPath) {
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(
					new String[] { DSSP_LOCATION, "-i", inputPath, "-o",
							outputPath });
			process.waitFor();
		} catch (IOException e) {
		} catch (InterruptedException e) {
		} finally {
			try {
				process.destroy();
			} catch (NullPointerException e) {
			}
		}
	}
}