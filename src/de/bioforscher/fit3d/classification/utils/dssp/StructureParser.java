/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.classification.utils.dssp;

import java.io.IOException;

import org.biojava.bio.structure.io.FileParsingParameters;
import org.biojava.bio.structure.io.PDBFileReader;

public abstract class StructureParser {

	PDBFileReader pdbReader;

	public StructureParser(String pdbPath) {

		this.pdbReader = new PDBFileReader();
		this.pdbReader.setPath(pdbPath);
		this.pdbReader.setPdbDirectorySplit(true);
		this.pdbReader.setAutoFetch(false);

		FileParsingParameters params = new FileParsingParameters();
		params.setAlignSeqRes(false);
		params.setParseCAOnly(false);

		this.pdbReader.setFileParsingParameters(params);
	}

	public abstract void parse() throws IOException;
}
