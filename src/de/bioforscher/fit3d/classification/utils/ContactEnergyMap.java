/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.classification.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.bio.structure.StructureTools;
import org.biojava.bio.structure.UnknownPdbAminoAcidException;

public class ContactEnergyMap {

	private Map<Character, Map<Character, Double>> contactEnergyMap;

	public ContactEnergyMap() throws NumberFormatException,
			UnknownPdbAminoAcidException, IOException {

		loadContactEnergyMap();
	}

	public Map<Character, Map<Character, Double>> getContactEnergyMap() {
		return this.contactEnergyMap;
	}

	public void setContactEnergyMap(
			Map<Character, Map<Character, Double>> contactEnergyMap) {
		this.contactEnergyMap = contactEnergyMap;
	}

	private void loadContactEnergyMap() throws UnknownPdbAminoAcidException,
			NumberFormatException, IOException {
		Map<Character, Map<Character, Double>> re = new HashMap<Character, Map<Character, Double>>(
				25);
		int c = 0;
		List<String> ids = new ArrayList<String>();
		String[] parts;
		// TODO: is this working in all cases?
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream is = classLoader
				.getResourceAsStream("de/bioforscher/fit3d/classification/utils/contactGlob.txt");

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line;
		while ((line = reader.readLine()) != null) {
			parts = line.split("\t");
			if (c == 0) {
				for (int i = 1; i < parts.length; i++)
					ids.add(parts[i]);
				for (String idI : ids)
					re.put(StructureTools.convert_3code_1code(idI),
							new HashMap<Character, Double>());
				c++;
				continue;
			}
			String idJ = parts[0];
			for (int i = 1; i < parts.length; i++)
				re.get(StructureTools.convert_3code_1code(idJ)).put(
						StructureTools.convert_3code_1code(ids.get(i - 1)),
						Double.valueOf(parts[i]));
		}

		this.contactEnergyMap = re;
	}
}
