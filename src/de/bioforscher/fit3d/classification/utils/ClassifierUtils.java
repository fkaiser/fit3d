/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.classification.utils;

import java.util.Map;

import com.google.common.primitives.Doubles;

public class ClassifierUtils {

	/**
	 * converts secondary structure information to double values
	 * 
	 * STRAND/SHEET=-1.0,COIL=0.0,HELIX=1.0
	 * 
	 * @param secStruc
	 * @return secStrucInt
	 */
	public static double convertSecondaryStructure(Map<String, String> secStruc) {

		// coil
		if (secStruc.isEmpty()) {

			return 0.0;
		}

		String secStrucId = secStruc.get("PDB_AUTHOR_ASSIGNMENT");

		// strand/sheet
		if (secStrucId.equals("STRAND")) {

			return -1.0;
		}

		// helix
		if (secStrucId.equals("HELIX")) {

			return 1.0;
		} else {

			return 0.0;
		}
	}

	/**
	 * gets index of amino acids according to ExPASy ProtScale
	 * 
	 * http://web.expasy.org/protscale/
	 * 
	 * @param type
	 * @return index
	 */
	public static int getAminoAcidIndex(char type) {

		int index = -1;

		String aminoAcids = "ARNDCQEGHILKMFPSTWYV";

		for (int i = 0; i < aminoAcids.length(); i++) {

			if (aminoAcids.charAt(i) == type) {

				index = i;
				break;
			}
		}

		return index;
	}

	/**
	 * normalizes a vector
	 * 
	 * @param coords
	 * @return normalizedDescriptor
	 */
	public static double[] normalizeDescriptor(double[] coords) {

		double length = Math.sqrt(Math.pow(coords[0], 2)
				+ Math.pow(coords[1], 2) + Math.pow(coords[2], 2));

		double[] normalizedDescriptor = new double[3];
		normalizedDescriptor[0] = coords[0] / length;
		normalizedDescriptor[1] = coords[1] / length;
		normalizedDescriptor[2] = coords[2] / length;

		return normalizedDescriptor;
	}

	/**
	 * normalizes a scale to the range [0,1]
	 * 
	 * @param scale
	 * @return normalized
	 */
	public static double[] normalizeScale(double[] scale) {

		double[] normalized = new double[20];
		double min = Doubles.min(scale);
		double max = Doubles.max(scale);

		for (int i = 0; i < scale.length; i++) {

			normalized[i] = (scale[i] - min) / (max - min);
		}

		return normalized;

	}
}
