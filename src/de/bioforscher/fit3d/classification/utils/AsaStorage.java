/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.classification.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import de.bioforscher.fit3d.Starter;

/**
 * A reader and writer class for ASA results.
 * 
 * @author fkaiser
 *
 */
public class AsaStorage {

	private static final String ASA_STORAGE_PATH = "asaStorage.fit";

	public static AsaStorage getInstance() {

		return instance;
	}

	private Map<String, AsaResult> storage;
	private static AsaStorage instance = new AsaStorage();

	@SuppressWarnings("unchecked")
	private AsaStorage() {

		File asaStorageFile = new File(ASA_STORAGE_PATH);
		if (asaStorageFile.exists()) {

			try {

				ObjectInputStream ois = new ObjectInputStream(
						new FileInputStream(asaStorageFile));

				Object o = ois.readObject();
				if (o instanceof Map<?, ?>) {

					this.storage = (Map<String, AsaResult>) o;
				}
				ois.close();

			} catch (ClassNotFoundException e) {

				Starter.LOG.warning(e.getMessage());
			} catch (FileNotFoundException e) {

				Starter.LOG.warning(e.getMessage());
			} catch (IOException e) {

				Starter.LOG.warning(e.getMessage());
			}
		} else {

			this.storage = new HashMap<>();
		}
	}

	/**
	 * adds an entry to the ASA storage
	 * 
	 * @param pdbId
	 * @param result
	 */
	public void addEntry(String pdbId, AsaResult result) {

		this.storage.put(pdbId, result);
	}

	public Map<String, AsaResult> getStorage() {
		return this.storage;
	}

	/**
	 * writes ASA storage to disk
	 */
	public void writeStorage() {

		try {

			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(ASA_STORAGE_PATH));
			oos.writeObject(this.storage);
			oos.close();

		} catch (FileNotFoundException e) {

			Starter.LOG.warning(e.getMessage());
		} catch (IOException e) {

			Starter.LOG.warning(e.getMessage());
		}
	}
}
