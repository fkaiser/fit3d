/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.classification.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Group;
import org.biojava.bio.structure.asa.GroupAsa;

import de.bioforscher.fit3d.io.utils.InputOutputUtils;

/**
 * A class to hold a ASA result.
 * 
 * @author fkaiser
 *
 */
public class AsaResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7991779917863075905L;
	private final Map<String, Double> storage;

	public AsaResult() {

		this.storage = new HashMap<>();
	}

	public void addGroupResult(GroupAsa[] result) {

		for (GroupAsa r : result) {

			Group g = r.getGroup();
			if (g instanceof AminoAcid) {

				this.storage.put(
						InputOutputUtils.getAminoAcidString((AminoAcid) g),
						r.getAsaU());
			}
		}
	}

	public Map<String, Double> getStorage() {
		return this.storage;
	}
}
