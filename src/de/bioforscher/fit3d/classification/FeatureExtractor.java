/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.classification;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.AtomImpl;
import org.biojava.bio.structure.Calc;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.StructureTools;
import org.biojava.bio.structure.UnknownPdbAminoAcidException;
import org.biojava.bio.structure.asa.AsaCalculator;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import de.bioforscher.ep.model.EPChain;
import de.bioforscher.ep.model.EnergyProfile;
import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.classification.scales.Scales;
import de.bioforscher.fit3d.classification.utils.AsaResult;
import de.bioforscher.fit3d.classification.utils.AsaStorage;
import de.bioforscher.fit3d.classification.utils.ClassifierUtils;
import de.bioforscher.fit3d.classification.utils.ContactEnergyMap;
import de.bioforscher.fit3d.classification.utils.dssp.DSSPWrapper;
import de.bioforscher.fit3d.io.parser.StructureParser;
import de.bioforscher.fit3d.io.utils.InputOutputUtils;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.utils.AtomTypes;
import de.bioforscher.fit3d.utils.StructureUtils;
import de.hsmw.bioinformatics.ep.eCalc.ECalcGlobular;

// TODO parallelization
public class FeatureExtractor {

	// TODO find right property cutoff
	private static final double PROPERTY_CUTOFF = 0.5;
	private final Hit hit;
	private Instance ins;
	private List<AminoAcid> hitAminoAcids;
	private List<Atom> hitCaAtoms;
	private Atom hitCentroid;
	private double envSize;
	private ArrayList<AminoAcid> envAminoAcids;
	private final FastVector attributes;

	private final String classLabel;

	public FeatureExtractor(Hit hit, FastVector attributes, String classLabel)
			throws FileNotFoundException, IOException, StructureException {

		this.hit = hit;
		this.attributes = attributes;
		this.classLabel = classLabel;

		initialize();
	}

	/**
	 * returns the feature vector of hit
	 * 
	 * @return ins
	 * @throws StructureException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws UnknownPdbAminoAcidException
	 * @throws NumberFormatException
	 * @throws ClassNotFoundException
	 */
	public Instance getFeatureVector() throws StructureException,
			FileNotFoundException, IOException, NumberFormatException,
			UnknownPdbAminoAcidException {

		// initialize feature vector of attribute corresponding length
		this.ins = new Instance(this.hit.getAminoAcids().size()
				+ this.hit.getAminoAcids().size()
				+ this.hit.getAminoAcids().size()
				+ this.hit.getAminoAcids().size()
				+ this.hit.getAminoAcids().size() + 3 * Scales.values().length
				+ 1);

		// secondary structure
		calculateSecondaryStructure();

		// accessible surface area
		calculateAsa();

		// energy
		// TODO serialize energy calculation results
		calculateEnergy();

		// contact energy
		// TODO serialize contact energy calculation results
		calculateContactEnergy();

		// loop fraction
		// TODO serialize loop fraction calculation results
		calculateLoopFraction();

		// calculate physicochemical property descriptors
		calculateDescriptors(Scales.values());

		// // hydrophobicity
		// calculateDescriptor(Scales.HYDROPHOBICITY);
		//
		// // polarity
		// calculateDescriptor(Scales.POLARITY);
		//
		// // bulkiness
		// calculateDescriptor(Scales.BULKINESS);
		//
		// // flexibility
		// calculateDescriptor(Scales.FLEXIBILITY);
		//
		// // mutability
		// calculateDescriptor(Scales.MUTABILITY);
		//
		// // buried
		// calculateDescriptor(Scales.BURIED);

		// set class label if defined
		if (this.classLabel != null) {

			this.ins.setValue((Attribute) this.attributes.lastElement(),
					this.classLabel);

			// // TODO shuffling class label
			// Random r = new Random();
			// if (r.nextBoolean()) {
			//
			// this.ins.setValue((Attribute) this.attributes.lastElement(),
			// "positive");
			//
			// } else {
			//
			// this.ins.setValue((Attribute) this.attributes.lastElement(),
			// "negative");
			// }
		}

		return this.ins;
	}

	/**
	 * calculates accessible surface area of hit
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws StructureException
	 * @throws ClassNotFoundException
	 */
	private void calculateAsa() throws FileNotFoundException, IOException,
			StructureException {

		// try to load ASA result
		String hitPdbId = this.hit.getPdbId();
		AsaResult result = new AsaResult();

		if (AsaStorage.getInstance().getStorage().containsKey(hitPdbId)) {

			Starter.LOG.finer("restored ASA results for " + hitPdbId);

			result = AsaStorage.getInstance().getStorage().get(hitPdbId);

		} else {

			Starter.LOG.finer("de novo calculation of ASA for " + hitPdbId);

			// parse structure
			StructureParser sp = new StructureParser(hitPdbId, null);
			sp.getAllAminoAcids();

			// initialize ASA calculator
			AsaCalculator ac = new AsaCalculator(sp.getStructure(),
					AsaCalculator.DEFAULT_PROBE_SIZE,
					AsaCalculator.DEFAULT_N_SPHERE_POINTS, ParameterProvider
							.getInstance().getCores(), false);
			result.addGroupResult(ac.getGroupAsas());

			// add result to ASA storage
			AsaStorage.getInstance().addEntry(hitPdbId, result);
		}

		// get ASA result for all amino acids of hit
		for (int i = 0; i < this.hitAminoAcids.size(); i++) {

			AminoAcid aa = this.hitAminoAcids.get(i);

			if (result.getStorage().containsKey(
					InputOutputUtils.getAminoAcidString(aa))) {

				double value = result.getStorage().get(
						InputOutputUtils.getAminoAcidString(aa));
				this.ins.setValue((Attribute) this.attributes
						.elementAt(this.hitAminoAcids.size() + i), value);
			}
			// for (GroupAsa groupResult : result) {
			//
			// Group g = groupResult.getGroup();
			//
			// if (!(g instanceof AminoAcid)) {
			//
			// continue;
			// }
			//
			// AminoAcid aaAsa = (AminoAcid) g;
			//
			// if (aa.getChainId().equals(String.valueOf(aaAsa.getChainId()))
			// && aa.getResidueNumber()
			// .getSeqNum()
			// .equals(Integer.valueOf(aaAsa
			// .getResidueNumber().getSeqNum()))
			// && aa.getAminoType().equals(
			// Character.valueOf(aaAsa.getAminoType()))) {
			//
			// if (aa.getResidueNumber().getInsCode() == null
			// && aaAsa.getResidueNumber().getInsCode() == null) {
			//
			// this.ins.setValue((Attribute) this.attributes
			// .elementAt(this.hitAminoAcids.size() + i),
			// groupResult.getAsaU());
			// }
			// if (aa.getResidueNumber().getInsCode() != null
			// && aaAsa.getResidueNumber().getInsCode() != null) {
			//
			// if (aa.getResidueNumber().getInsCode()
			// .equals(aaAsa.getResidueNumber().getInsCode())) {
			//
			// this.ins.setValue((Attribute) this.attributes
			// .elementAt(this.hitAminoAcids.size() + i),
			// groupResult.getAsaU());
			// }
			// }
			// }
			//
			// }
		}
	}

	/**
	 * calculates geometric centroid of hit
	 */
	private void calculateCentroid() {

		double[] coords = new double[3];

		coords[0] = 0.0;
		coords[1] = 0.0;
		coords[2] = 0.0;

		for (Atom a : this.hitCaAtoms) {

			coords[0] += a.getX();
			coords[1] += a.getY();
			coords[2] += a.getZ();
		}

		coords[0] = coords[0] / this.hitCaAtoms.size();
		coords[1] = coords[1] / this.hitCaAtoms.size();
		coords[2] = coords[2] / this.hitCaAtoms.size();

		Atom centroid = new AtomImpl();
		centroid.setCoords(coords);

		this.hitCentroid = centroid;
	}

	private void calculateContactEnergy() throws StructureException,
			NumberFormatException, UnknownPdbAminoAcidException, IOException {

		ContactEnergyMap cem = new ContactEnergyMap();

		StructureParser sp = new StructureParser(this.hit.getPdbId(), null);
		List<AminoAcid> targetAminoAcids = sp.getAllAminoAcids();

		for (int i = 0; i < this.hitAminoAcids.size(); i++) {

			AminoAcid aaMotif = this.hitAminoAcids.get(i);

			// System.out.println(currentGroup.getChainId() + " " +
			// currentGroup.getResidueNumber().getSeqNum() + " " +
			// currentGroup.getPDBName());
			Character cga;
			try {
				cga = StructureTools.convert_3code_1code(aaMotif.getPDBName());
			} catch (UnknownPdbAminoAcidException e) {
				// TODO log
				continue;
			}
			double contact = 0;
			for (AminoAcid aaTarget : targetAminoAcids) {

				if (aaTarget.equals(aaMotif)) {

					continue;
				}

				Character sga;
				try {
					sga = StructureTools.convert_3code_1code(aaTarget
							.getPDBName());
				} catch (UnknownPdbAminoAcidException e) {
					// TODO log
					continue;
				}
				Atom a1;
				if (aaTarget.getAminoType() == 'G') {

					a1 = Calc.createVirtualCBAtom(aaTarget);
				} else {

					a1 = aaTarget.getCB();
				}
				Atom a2;
				if (aaMotif.getAminoType() == 'G') {

					a2 = Calc.createVirtualCBAtom(aaTarget);
				} else {

					a2 = aaMotif.getCB();
				}
				if (Calc.getDistanceFast(a1, a2) <= 64.0) {
					contact += cem.getContactEnergyMap().get(cga).get(sga);
				}
				// ATOM_3_INTERACTION_SCHEME.interacting(currentGroup,
				// surroundingGroup);
			}

			this.ins.setValue((Attribute) this.attributes
					.elementAt(this.hitAminoAcids.size()
							+ this.hitAminoAcids.size()
							+ this.hitAminoAcids.size() + i), contact);

		}
	}

	/**
	 * calculates physicochemical property descriptors for a set of scales
	 * 
	 * @param scales
	 * @throws StructureException
	 */
	private void calculateDescriptors(Scales... scales)
			throws StructureException {

		int scaleCounter = 0;
		for (Scales s : scales) {

			// TODO normalize again
			double[] normalizedScale = ClassifierUtils.normalizeScale(s
					.getValues());
			// double[] normalizedScale = s.getValues();

			double[] coords = new double[3];
			coords[0] = 0.0;
			coords[1] = 0.0;
			coords[2] = 0.0;

			int validEnvCounter = 0;
			for (AminoAcid aa : this.envAminoAcids) {

				int aaIndex = ClassifierUtils.getAminoAcidIndex(aa
						.getAminoType());

				// skip if unknown type
				if (aaIndex == -1) {

					Starter.LOG
							.fine("no physicochemical property value could be extracted for "
									+ InputOutputUtils.getAminoAcidString(aa));
					continue;
				}

				double property = normalizedScale[aaIndex];

				// binary decision
				if (property > PROPERTY_CUTOFF) {

					validEnvCounter++;
					coords[0] += aa.getAtom(AtomTypes.CA.getAtomName()).getX()
							* property;
					coords[1] += aa.getAtom(AtomTypes.CA.getAtomName()).getY()
							* property;
					coords[2] += aa.getAtom(AtomTypes.CA.getAtomName()).getZ()
							* property;

				}
			}

			// normalize vector
			coords[0] = coords[0] / validEnvCounter;
			coords[1] = coords[1] / validEnvCounter;
			coords[2] = coords[2] / validEnvCounter;

			// TODO normalize again
			double[] coordsNormalized = ClassifierUtils
					.normalizeDescriptor(coords);
			// double[] coordsNormalized = coords;

			Atom descriptor = new AtomImpl();
			descriptor.setCoords(coordsNormalized);

			this.ins.setValue(
					(Attribute) this.attributes.elementAt(this.hitAminoAcids
							.size() * 5 + scaleCounter), descriptor.getX());
			this.ins.setValue(
					(Attribute) this.attributes.elementAt(this.hitAminoAcids
							.size() * 5 + scaleCounter + 1), descriptor.getY());
			this.ins.setValue(
					(Attribute) this.attributes.elementAt(this.hitAminoAcids
							.size() * 5 + scaleCounter + 2), descriptor.getZ());

			// increase scale counter
			scaleCounter = scaleCounter + 3;
		}
	}

	private void calculateEnergy() throws FileNotFoundException, IOException,
			StructureException {

		StructureParser sp = new StructureParser(this.hit.getPdbId(), null);
		sp.getAllAminoAcids();

		ECalcGlobular eG = new ECalcGlobular();
		EnergyProfile profile = eG.calculateEnergyProfile(sp.getStructure(),
				this.hit.getPdbId());

		for (int i = 0; i < this.hit.getAminoAcids().size(); i++) {

			EPChain chain = profile.getChainById(this.hit.getAminoAcids()
					.get(i).getChainId());

			// System.out.println(chain);
			double energy = chain.getEntryByIndex(
					this.hit.getAminoAcids().get(i).getResidueNumber())
					.getEnergy();
			this.ins.setValue(
					(Attribute) this.attributes.elementAt(this.hitAminoAcids
							.size() + this.hitAminoAcids.size() + i), energy);
		}
	}

	private void calculateLoopFraction() throws IOException {

		DSSPWrapper dssp = new DSSPWrapper(this.hit.getPdbId());

		for (int i = 0; i < this.hitAminoAcids.size(); i++) {

			AminoAcid hAa = this.hitAminoAcids.get(i);

			for (AminoAcid dsspAa : dssp.getAminoAcids()) {

				if (InputOutputUtils.getAminoAcidString(hAa).equals(
						InputOutputUtils.getAminoAcidString(dsspAa))) {

					hAa.setProperty("loopFraction",
							dsspAa.getProperty("loopFraction"));
				}
			}

			this.ins.setValue((Attribute) this.attributes
					.elementAt(this.hitAminoAcids.size()
							+ this.hitAminoAcids.size()
							+ this.hitAminoAcids.size()
							+ this.hitAminoAcids.size() + i), (Double) hAa
					.getProperty("loopFraction"));
		}
	}

	/**
	 * calculates secondary structure of hit
	 * 
	 * @param attributes
	 * @throws StructureException
	 */
	private void calculateSecondaryStructure() throws StructureException {
		for (int i = 0; i < this.hitAminoAcids.size(); i++) {

			AminoAcid aa = this.hitAminoAcids.get(i);

			// store feature value
			this.ins.setValue((Attribute) this.attributes.elementAt(i),
					ClassifierUtils.convertSecondaryStructure(aa.getSecStruc()));
		}
	}

	/**
	 * gets environment around geometric center of hit
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws StructureException
	 */
	private void getEnvironment() throws FileNotFoundException, IOException,
			StructureException {

		StructureParser sp = new StructureParser(this.hit.getPdbId(), null);
		List<AminoAcid> aminoAcids = sp.getAllAminoAcids();

		this.envAminoAcids = new ArrayList<>();

		for (AminoAcid aa : aminoAcids) {

			try {
				double d = Calc.getDistanceFast(
						aa.getAtom(AtomTypes.CA.getAtomName()),
						this.hitCentroid);
				if (d <= this.envSize && d != 0.0) {

					this.envAminoAcids.add(aa);
				}
			} catch (StructureException e) {

				Starter.LOG.fine("could not add " + aa + " to environment"
						+ e.getMessage());
			}
		}
	}

	private void initialize() throws FileNotFoundException, IOException,
			StructureException {

		// get BioJava AminoAcids of hit
		this.hitAminoAcids = StructureUtils
				.getBioJavaAminoAcidsFromHit(this.hit);
		this.hitCaAtoms = new ArrayList<>();

		// get doubled motif spatial extent as environment size
		this.envSize = StructureUtils.getMaxSquaredExtent(this.hitAminoAcids) * 2;

		// add CA atom to list
		for (AminoAcid aa : this.hitAminoAcids) {

			this.hitCaAtoms.add(aa.getAtom(AtomTypes.CA.getAtomName()));
		}

		// calculate centroid
		calculateCentroid();

		// get environment around centroid
		getEnvironment();

		// shift atoms to center
		shiftAtomsToCenter();
	}

	/**
	 * shifts all atoms of environment to geometric center of hit
	 */
	private void shiftAtomsToCenter() {

		for (AminoAcid aa : this.envAminoAcids) {

			for (Atom a : aa.getAtoms()) {

				a.setX(a.getX() - this.hitCentroid.getX());
				a.setY(a.getY() - this.hitCentroid.getY());
				a.setZ(a.getZ() - this.hitCentroid.getZ());
			}
		}
	}
}
