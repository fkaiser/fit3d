/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.classification;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.UnknownPdbAminoAcidException;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.PrincipalComponents;
import weka.attributeSelection.Ranker;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.meta.GridSearch;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.filters.AllFilter;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.classification.scales.Scales;
import de.bioforscher.fit3d.io.utils.NullPrintStream;
import de.bioforscher.fit3d.model.Hit;

/**
 * A classifier for functional annotation of hits.
 * 
 * @author fkaiser
 *
 */
@Deprecated
public class HitEcClassifierLegacy {

	private static final double GRIDSEARCH_X_MIN = 1.0;
	private static final double GRIDSEARCH_X_MAX = 2.0;
	private static final double GRIDSEARCH_X_STEP = 1;
	private static final double GRIDSEARCH_Y_MIN = 0;
	private static final double GRIDSEARCH_Y_MAX = 3.0;
	private static final double GRIDSEARCH_Y_STEP = 1;
	private List<Hit> positiveSet;
	private List<Hit> negativeSet;

	private final int motifSize;
	private LibSVM svm;
	private final String ecNumber;

	private final List<Hit> hits;
	private Instances trainingSet;
	private FastVector attributes;

	public HitEcClassifierLegacy(List<Hit> hits, String ecNumber) {

		this.hits = hits;
		this.ecNumber = ecNumber;
		this.motifSize = this.hits.get(0).getAminoAcids().size();
	}

	/**
	 * assigns classes to results
	 * 
	 */
	public void assignClasses() {

		// calculate features for all hits and classify
		for (Hit h : this.hits) {

			try {

				Starter.LOG.fine("classifying instance " + h);

				// avoid classifying training instances
				if (this.positiveSet.contains(h)
						|| this.negativeSet.contains(h)) {

					h.setClassification("o");
					continue;
				}

				// extract features
				FeatureExtractor fe = new FeatureExtractor(h, this.attributes,
						null);
				Instances toBeClassified = new Instances("hits",
						this.attributes, 0);
				toBeClassified.add(fe.getFeatureVector());

				// set class index
				toBeClassified
						.setClassIndex(toBeClassified.numAttributes() - 1);

				// predict instance
				double[] distributions = this.svm
						.distributionForInstance(toBeClassified.firstInstance());

				if (distributions[0] > distributions[1]) {

					h.setClassification("+");
					h.setClassificationDistribution(distributions[0]);
				} else {

					h.setClassification("-");
					h.setClassificationDistribution(distributions[1]);
				}

			} catch (FileNotFoundException e) {
				Starter.LOG.fine("could not classify instance "
						+ e.getMessage());
			} catch (IOException e) {
				Starter.LOG.fine("could not classify instance "
						+ e.getMessage());
			} catch (StructureException e) {
				Starter.LOG.fine("could not classify instance "
						+ e.getMessage());
			} catch (Exception e) {
				Starter.LOG.fine("could not classify instance "
						+ e.getMessage());
			}
		}
	}

	/**
	 * builds the libSVM classifier
	 * 
	 * @throws ClassNotFoundException
	 * @throws ClassifierException
	 */
	public void buildClassifier() throws ClassNotFoundException,
			ClassifierException {

		// build attributes
		this.attributes = buildAttributes();

		// check if enough instances are availaible
		if ((this.positiveSet.size() < this.attributes.size())
				|| (this.negativeSet.size() < this.attributes.size())) {

			throw new ClassifierException(
					"number of instances is smaller than number of features");
		}

		// find training set
		this.trainingSet = new Instances("hits", this.attributes, 0);

		// add positive instances
		Starter.LOG.info("adding positive instances");
		for (Hit hit : this.positiveSet) {

			try {

				Starter.LOG.fine("extracting features for " + hit.getPdbId());
				FeatureExtractor fe = new FeatureExtractor(hit,
						this.attributes, "positive");
				Instance ins = fe.getFeatureVector();
				this.trainingSet.add(ins);

			} catch (FileNotFoundException e) {
				Starter.LOG.fine("could not extract features for " + hit + " "
						+ e.getMessage());
			} catch (IOException e) {
				Starter.LOG.fine("could not extract features for " + hit + " "
						+ e.getMessage());
			} catch (StructureException e) {
				Starter.LOG.fine("could not extract features for " + hit + " "
						+ e.getMessage());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownPdbAminoAcidException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// add negative instances
		Starter.LOG.info("adding negative instances");
		for (Hit hit : this.negativeSet) {

			try {

				Starter.LOG.fine("extracting features for " + hit.getPdbId());
				FeatureExtractor fe = new FeatureExtractor(hit,
						this.attributes, "negative");
				Instance ins = fe.getFeatureVector();
				this.trainingSet.add(ins);

			} catch (FileNotFoundException e) {
				Starter.LOG.warning("could not extract features for " + hit
						+ " " + e.getMessage());
			} catch (IOException e) {
				Starter.LOG.warning("could not extract features for " + hit
						+ " " + e.getMessage());
			} catch (StructureException e) {
				Starter.LOG.warning("could not extract features for " + hit
						+ " " + e.getMessage());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownPdbAminoAcidException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// set last attribute to class label
		this.trainingSet.setClassIndex(this.trainingSet.numAttributes() - 1);

		// TODO GRID SEARCH
		// Starter.LOG.info("performing grid search");
		// gridSearch();

		// TODO SELECTION
		// Starter.LOG.info("performing attribute selection");
		// select();

		// TRAINING
		Starter.LOG.info("training classifier");
		train();

		// EVALUATION
		Starter.LOG.info("evaluating classifier");
		evaluate();
	}

	/**
	 * initializes classifier training sets
	 * 
	 * @param balance
	 */
	public void generateSets(boolean balance) {

		this.positiveSet = new ArrayList<>();
		this.negativeSet = new ArrayList<>();

		Pattern pattern = Pattern.compile("^" + this.ecNumber + ".*");

		// generate positive set
		for (Hit hit : this.hits) {

			String ecNumber = hit.getEcNumber();
			Matcher matcher = pattern.matcher(ecNumber);

			if (matcher.matches()) {

				this.positiveSet.add(hit);
			} else {

				if (!ecNumber.equals("?")) {

					this.negativeSet.add(hit);
				}
			}
		}

		// balance sets if enabled
		if (balance) {
			balanceSets();
		}
	}

	public List<Hit> getNegativeSet() {
		return this.negativeSet;
	}

	public List<Hit> getPositiveSet() {
		return this.positiveSet;
	}

	/**
	 * performs a grid search to find optimal parameters for libSVm RBF kernel
	 * (C,gamma)
	 */
	public void gridSearch() {

		Starter.LOG.info("performing grid search");
		GridSearch gs = new GridSearch();
		LibSVM gridSearchClassifier = new LibSVM();
		gs.setClassifier(gridSearchClassifier);

		gs.setXProperty("classifier.cost");
		gs.setXMin(GRIDSEARCH_X_MIN);
		gs.setXMax(GRIDSEARCH_X_MAX);
		gs.setXStep(GRIDSEARCH_X_STEP);

		gs.setYProperty("classifier.gamma");
		gs.setYMin(GRIDSEARCH_Y_MIN);
		gs.setYMax(GRIDSEARCH_Y_MAX);
		gs.setYStep(GRIDSEARCH_Y_STEP);

		gs.setFilter(new AllFilter());
		gs.setEvaluation(new SelectedTag(GridSearch.EVALUATION_CC,
				GridSearch.TAGS_EVALUATION));
		gs.setDebug(true);
		System.out.println(gs);
		try {
			gs.buildClassifier(this.trainingSet);
			System.out.println(gs);
			System.out.println(gs.toSummaryString());
			System.out.println(gs.getMeasure("ACC"));

			Evaluation eva = new Evaluation(this.trainingSet);
			LibSVM libSvm = new LibSVM();
			libSvm.setCost(2);
			libSvm.setGamma(3.0);
			eva.crossValidateModel(libSvm, this.trainingSet, 10, new Random(1));
			System.out.println(eva.toSummaryString());
			System.out.println();
		} catch (Exception e) {

			Starter.LOG.warning(e.getMessage());
		}
	}

	/**
	 * performs attribute selection for the training set
	 */
	public void select() {

		try {

			// System.out.println("\n1. Meta-classfier");
			// AttributeSelectedClassifier classifier = new
			// AttributeSelectedClassifier();
			// CfsSubsetEval eval = new CfsSubsetEval();
			// GreedyStepwise search = new GreedyStepwise();
			// search.setSearchBackwards(true);
			// J48 base = new J48();
			// classifier.setClassifier(base);
			// classifier.setEvaluator(eval);
			// classifier.setSearch(search);
			// Evaluation evaluation = new Evaluation(this.trainingSet);
			// evaluation.crossValidateModel(classifier, this.trainingSet, 10,
			// new Random(1));
			// System.out.println(evaluation.toSummaryString());
			// System.out.println(classifier.toString());
			// System.out.println();

			// GAIN-RATIO-ATTRIBUTE
			// GainRatioAttributeEval evaluation = new GainRatioAttributeEval();

			// CFS-SUBSET
			// CfsSubsetEval evaluation = new CfsSubsetEval();

			// PCA
			PrincipalComponents evaluation = new PrincipalComponents();

			// @phdthesis{Hall1998,
			// address = {Hamilton, New Zealand},
			// author = {M. A. Hall},
			// school = {University of Waikato},
			// title = {Correlation-based Feature Subset Selection for Machine
			// Learning},
			// year = {1998}
			// }

			// RANKER
			Ranker search = new Ranker();

			// GREEDY-STEPWISE
			// GreedyStepwise search = new GreedyStepwise();
			// search.setSearchBackwards(true);

			// attribute selection
			AttributeSelection selector = new AttributeSelection();
			selector.setEvaluator(evaluation);
			selector.setSearch(search);
			selector.SelectAttributes(this.trainingSet);

			System.out.println(selector.toResultsString());

			FileUtils.write(new File("pca.txt"), selector.toResultsString());

			// TODO print features for manual PCA
			for (int i = 0; i < this.trainingSet.numInstances(); i++) {

				FileUtils.write(new File("instances.csv"),
						Arrays.toString(this.trainingSet.instance(i)
								.toDoubleArray()), true);

			}
		} catch (Exception e) {

			Starter.LOG.warning("unhandled exception " + e.getMessage());
		}
	}

	/**
	 * balances positive and negative set to equal size
	 */
	private void balanceSets() {

		// remove randomly until classes are balanced
		while (this.negativeSet.size() != this.positiveSet.size()) {

			int randomIndex = (int) (Math.random() * this.negativeSet.size());

			if (this.negativeSet.size() > this.positiveSet.size()) {

				this.negativeSet.remove(randomIndex);
			}
			if (this.negativeSet.size() < this.positiveSet.size()) {

				this.positiveSet.remove(randomIndex);
			}
		}
	}

	/**
	 * build attribute description
	 * 
	 * @return attributeInfo
	 */
	private FastVector buildAttributes() {

		FastVector attributeInfo = new FastVector();

		// secondary structure
		for (int i = 0; i < this.motifSize; i++) {

			attributeInfo.addElement(new Attribute("aa" + i + "ss"));
		}

		// accessible surface area
		for (int i = 0; i < this.motifSize; i++) {

			attributeInfo.addElement(new Attribute("asa" + i));
		}

		// physicochemical property descriptors
		for (Scales s : Scales.values()) {

			attributeInfo.addElement(new Attribute(s.name() + "_X"));
			attributeInfo.addElement(new Attribute(s.name() + "_Y"));
			attributeInfo.addElement(new Attribute(s.name() + "_Z"));
		}

		// // hydrophobicity
		// attributeInfo.addElement(new Attribute("hphobX"));
		// attributeInfo.addElement(new Attribute("hphobY"));
		// attributeInfo.addElement(new Attribute("hphobZ"));
		//
		// // polarity
		// attributeInfo.addElement(new Attribute("polX"));
		// attributeInfo.addElement(new Attribute("polY"));
		// attributeInfo.addElement(new Attribute("polZ"));
		//
		// // bulkiness
		// attributeInfo.addElement(new Attribute("bulX"));
		// attributeInfo.addElement(new Attribute("bulY"));
		// attributeInfo.addElement(new Attribute("bulZ"));
		//
		// // flexibility
		// attributeInfo.addElement(new Attribute("flexX"));
		// attributeInfo.addElement(new Attribute("flexY"));
		// attributeInfo.addElement(new Attribute("flexZ"));
		//
		// // mutability
		// attributeInfo.addElement(new Attribute("mutX"));
		// attributeInfo.addElement(new Attribute("mutY"));
		// attributeInfo.addElement(new Attribute("mutZ"));
		//
		// // buried
		// attributeInfo.addElement(new Attribute("burX"));
		// attributeInfo.addElement(new Attribute("burY"));
		// attributeInfo.addElement(new Attribute("burZ"));

		// class labels
		FastVector classValues = new FastVector(2);
		classValues.addElement("positive");
		classValues.addElement("negative");
		attributeInfo.addElement(new Attribute("class", classValues));

		return attributeInfo;
	}

	/**
	 * evaluates the classifier by 10-fold cross-validaton
	 * 
	 * @param trainingSet
	 */
	private void evaluate() {

		try {

			Evaluation eva = new Evaluation(this.trainingSet);
			LibSVM evaSvm = new LibSVM();
			evaSvm.setProbabilityEstimates(true);

			// redirect output to dummy stream
			PrintStream org = System.out;
			System.setOut(new NullPrintStream());

			// cross-validation
			StringBuffer predictions = new StringBuffer();
			eva.crossValidateModel(evaSvm, this.trainingSet, 10, new Random(1),
					predictions, null, true);

			// TODO store predictions of evaluation
			// FileUtils.write(new File("predictions.csv"),
			// predictions.toString(), true);

			// restore original output
			System.setOut(org);

			// print formatted performance
			NumberFormat percentFormat = NumberFormat
					.getPercentInstance(Starter.LOCALIZATION);
			percentFormat.setMinimumFractionDigits(2);
			percentFormat.setMaximumFractionDigits(2);
			Starter.LOG.info("correctly/incorrectly classified instances "
					+ percentFormat.format(eva.pctCorrect() / 100) + "/"
					+ percentFormat.format(eva.pctIncorrect() / 100));

			// TODO print performance measurements
			// double[][] matrix = eva.confusionMatrix();
			// String seperator = ",";
			// StringBuilder sbPerformance = new StringBuilder();
			// sbPerformance.append(FilenameUtils.removeExtension(FilenameUtils
			// .getBaseName(ParameterProvider.getInstance().getMotif())));
			// sbPerformance.append(seperator);
			// sbPerformance.append(String.valueOf(matrix[0][0]));
			// sbPerformance.append(seperator);
			// sbPerformance.append(String.valueOf(matrix[0][1]));
			// sbPerformance.append(seperator);
			// sbPerformance.append(String.valueOf(matrix[1][0]));
			// sbPerformance.append(seperator);
			// sbPerformance.append(String.valueOf(matrix[1][1]));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.areaUnderROC(0));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.fMeasure(0));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.fMeasure(1));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.KBInformation());
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.KBMeanInformation());
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.KBRelativeInformation());
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.kappa());
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.truePositiveRate(0));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.truePositiveRate(1));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.trueNegativeRate(0));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.trueNegativeRate(1));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.falsePositiveRate(0));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.falsePositiveRate(1));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.falseNegativeRate(0));
			// sbPerformance.append(seperator);
			// sbPerformance.append(eva.falseNegativeRate(1));
			// FileUtils.writeStringToFile(new File("performance.csv"),
			// sbPerformance.toString() + "\n", true);

			// TODO get ROC curve data
			// FastVector predictions = eva.predictions();
			// for (int i = 0; i < predictions.size(); i++) {
			//
			// FileUtils.writeStringToFile(new File("predictions.csv"),
			// predictions.elementAt(i) + "\n", true);
			// }

			// TODO show ROC curve
			// ThresholdCurve tc = new ThresholdCurve();
			// int classIndex = 0;
			// FastVector predictions = eva.predictions();
			//
			// Instances result = tc.getCurve(predictions, classIndex);
			//
			// // plot curve
			// ThresholdVisualizePanel vmc = new ThresholdVisualizePanel();
			// vmc.setROCString("(Area under ROC = "
			// + Utils.doubleToString(ThresholdCurve.getROCArea(result), 4)
			// + ")");
			// vmc.setName(result.relationName());
			// PlotData2D tempd = new PlotData2D(result);
			// tempd.setPlotName(result.relationName());
			// tempd.addInstanceNumberAttribute();
			// // specify which points are connected
			// boolean[] cp = new boolean[result.numInstances()];
			// for (int n = 1; n < cp.length; n++)
			// cp[n] = true;
			// tempd.setConnectPoints(cp);
			// // add plot
			// vmc.addPlot(tempd);
			//
			// // display curve
			// String plotName = vmc.getName();
			// final javax.swing.JFrame jf = new javax.swing.JFrame(
			// "Weka Classifier Visualize: " + plotName);
			// jf.setSize(500, 400);
			// jf.getContentPane().setLayout(new BorderLayout());
			// jf.getContentPane().add(vmc, BorderLayout.CENTER);
			// jf.addWindowListener(new java.awt.event.WindowAdapter() {
			// @Override
			// public void windowClosing(java.awt.event.WindowEvent e) {
			// jf.dispose();
			// }
			// });
			// jf.setVisible(true);

		} catch (Exception e) {

			Starter.LOG.warning("unhandled exception " + e.getMessage());
		}
	}

	/**
	 * trains the classifier by using the training set
	 * 
	 * @param trainingSet
	 */
	private void train() {

		this.svm = new LibSVM();
		this.svm.setProbabilityEstimates(true);
		try {

			// redirect output to dummy stream
			PrintStream org = System.out;
			System.setOut(new NullPrintStream());

			this.svm.buildClassifier(this.trainingSet);

			// restore original output
			System.setOut(org);

		} catch (Exception e) {
			Starter.LOG.warning(e.getMessage());
		}
	}
}
