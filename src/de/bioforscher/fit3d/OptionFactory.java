/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;

/**
 * A command line option provider class.
 * 
 * @author fkaiser
 * 
 */
public final class OptionFactory {

	public final static OptionFactory INSTANCE = new OptionFactory();

	private final Options clOptions = new Options();
	private final OptionGroup inputOptions = new OptionGroup();
	private final Options restoreOptions = new Options();
	private final OptionGroup targetOptions = new OptionGroup();
	private final OptionGroup verbosityOptions = new OptionGroup();

	private OptionFactory() {

		generateClOptions();
		generateInputOptions();
		generateRestoreOptions();
		generateTargetOptions();
		generateVerbosityOptions();
	}

	public Options getClOptions() {

		return this.clOptions;
	}

	public OptionGroup getInputOptions() {
		return this.inputOptions;
	}

	public Options getRestoreOptions() {
		return this.restoreOptions;
	}

	public OptionGroup getTargetOptions() {

		return this.targetOptions;
	}

	public OptionGroup getVerbosityOptions() {
		return this.verbosityOptions;
	}

	/**
	 * generates command line options
	 */
	private void generateClOptions() {

		// align output
		Option alignOutputStructures = new Option("g", "align output structures (default: false)");
		alignOutputStructures.setLongOpt("align-output");
		this.clOptions.addOption(alignOutputStructures);

		// atoms
		Option atoms = new Option("a",
				"PDB identifier of atoms used for alignment (default: all non-hydrogen motif atoms)");
		atoms.setLongOpt("atoms");
		atoms.setArgName("CA,CB,CG,CD,...");
		atoms.setArgs(1);
		this.clOptions.addOption(atoms);

		// classification
		Option classify = new Option("B",
				"classify hits based on EC class, e.g 3.4.21 for peptide hydrolases (experimental)");
		classify.setLongOpt("classify-hits");
		classify.setArgs(1);
		this.clOptions.addOption(classify);

		// clustering
		Option cluster = new Option("C", "hierarchical clustering of hits below LRMSD cutoff");
		cluster.setLongOpt("cluster-hits");
		cluster.setArgs(1);
		this.clOptions.addOption(cluster);

		// conserve sidechain atoms
		Option conserve = new Option("c", "conserve atoms for structure output");
		conserve.setLongOpt("conserve");
		this.clOptions.addOption(conserve);

		// distance tolerance
		Option distanceTolerance = new Option("d",
				"allowed tolerance of query motif spatial extent (default: 1.0 \u212B) " + Starter.NEWLINE
						+ " WARNING: performance decrease if raised, set lower value for larger motifs");
		distanceTolerance.setLongOpt("distance-tolerance");
		distanceTolerance.setArgs(1);
		this.clOptions.addOption(distanceTolerance);

		// exchange residues
		Option exchangeResidues = new Option("e",
				"allowed residue exchanges for input motif (default: none)" + Starter.NEWLINE
						+ " syntax: [motif residue number]:[allowed residues],... " + Starter.NEWLINE
						+ " e.g. 12:ASHPW,43:PR");
		exchangeResidues.setLongOpt("exchange-residues");
		exchangeResidues.setArgs(1);
		this.clOptions.addOption(exchangeResidues);

		// EC mapping
		Option ecMapping = new Option("E", "map EC numbers by getting RESTful data from RCSB" + Starter.NEWLINE
				+ " WARNING: requires internet connection and decreases performance");
		ecMapping.setLongOpt("ec-mapping");
		this.clOptions.addOption(ecMapping);

		// filter environment
		Option filterEnvironment = new Option("F", "pre-filter mirco environments based on local distance constraints"
				+ Starter.NEWLINE + " WARNING: performance gain, can result in loss of some hits");
		filterEnvironment.setLongOpt("filter-environment");
		this.clOptions.addOption(filterEnvironment);

		// gap sequence mapping
		Option gapSeqMapping = new Option("G", "map gap sequence between hit amino acids" + Starter.NEWLINE
				+ " WARNING: hit amino acids are reordered according to sequential order");
		gapSeqMapping.setLongOpt("gap-mapping");
		this.clOptions.addOption(gapSeqMapping);

		// result file
		Option resultFile = new Option("f", "result file");
		resultFile.setLongOpt("result-file");
		resultFile.setArgs(1);
		this.clOptions.addOption(resultFile);

		// help
		Option help = new Option("h", "show help dialog");
		help.setLongOpt("help");
		this.clOptions.addOption(help);

		// ignore missing atoms
		Option ignoreAtoms = new Option("i", "ignore missing atoms, force alignment (default: false)");
		ignoreAtoms.setLongOpt("ignore-atoms");
		this.clOptions.addOption(ignoreAtoms);

		// number of threads
		Option numThreads = new Option("n", "number of threads used for calculation (default: all available)");
		numThreads.setLongOpt("num-threads");
		numThreads.setArgs(1);
		this.clOptions.addOption(numThreads);

		// output structures directory
		Option outputFile = new Option("o", "output structures directory");
		outputFile.setLongOpt("output-structures");
		outputFile.setArgs(1);
		this.clOptions.addOption(outputFile);

		// PDB directory
		Option pdb = new Option("p", "path to local PDB directory");
		pdb.setLongOpt("pdb");
		pdb.setArgs(1);
		this.clOptions.addOption(pdb);

		// p-value calculation
		Option pvalueCalculation = new Option("P",
				"calculate p-values for matches according to Fafoanov et al. 2008 (F) or Stark et al. 2003 (S) (default: false) "
						+ Starter.NEWLINE + " WARNING: F needs R in path with package sfsmisc installed");
		pvalueCalculation.setLongOpt("pvalues");
		pvalueCalculation.setArgs(1);
		pvalueCalculation.setArgName("F|S");
		this.clOptions.addOption(pvalueCalculation);

		// Pfam annotation mapping
		Option pfamMapping = new Option("M", "map Pfam annotation by getting RESTful data from RCSB " + Starter.NEWLINE
				+ " WARNING: requires internet connection and decreases performance");
		pfamMapping.setLongOpt("pfam-mapping");
		this.clOptions.addOption(pfamMapping);

		// reference population size for Fofanov et al.
		Option refSize = new Option("N",
				"size of reference population for p-value calculation to estimate point-weight correction according to Fofanov et al. 2008 (default: 31133) "
						+ Starter.NEWLINE
						+ " WARNING: changing this parameter requires knowledge of the underlying method");
		refSize.setLongOpt("ref-size");
		refSize.setArgs(1);
		this.clOptions.addOption(refSize);

		// maximal RMSD
		Option rmsd = new Option("r", "maximal allowed LRMSD for hits  (default: 2.0 \u212B)");
		rmsd.setLongOpt("rmsd");
		rmsd.setArgs(1);
		this.clOptions.addOption(rmsd);

		// PDB directory split
		Option pdbSplit = new Option("s", "disable PDB directory split (default: false)");
		pdbSplit.setLongOpt("no-pdb-split");
		this.clOptions.addOption(pdbSplit);

		// title mapping
		Option titleMapping = new Option("T", "map structure titles assigned by PDB");
		titleMapping.setLongOpt("title-mapping");
		this.clOptions.addOption(titleMapping);

		// motif extraction
		Option motifExtraction = new Option("X",
				"extract motif from structure input (-m) following the syntax [chain]-[residue type][residue number]_... (e.g. A-E651_A-D649_A-T177)\n INFO: a subsequent search is performed and the extracted motif is written in PDB format");
		motifExtraction.setLongOpt("extract");
		motifExtraction.setArgs(1);
		this.clOptions.addOption(motifExtraction);
	}

	/**
	 * generate command line options for target definition
	 */
	private void generateInputOptions() {

		// motif
		Option motif = new Option("m", "motif PDB structure file*");
		motif.setLongOpt("motif");
		motif.setArgs(1);
		motif.setRequired(true);
		this.inputOptions.addOption(motif);

		// set input options required
		this.inputOptions.setRequired(true);
	}

	/**
	 * generate session restore options
	 */
	private void generateRestoreOptions() {

		// restore session
		Option restoreSession = new Option("R", "restore session from file");
		restoreSession.setLongOpt("restore-session");
		restoreSession.setArgs(1);
		this.clOptions.addOption(restoreSession);
		this.restoreOptions.addOption(restoreSession);
	}

	/**
	 * generate command line options for target definition
	 */
	private void generateTargetOptions() {

		// single target
		Option target = new Option("t", "target PDB-ID or file**");
		target.setLongOpt("target");
		target.setArgs(1);
		target.setRequired(true);
		this.targetOptions.addOption(target);

		// target list
		Option list = new Option("l", "target list of PDB-IDs or files**");
		list.setLongOpt("target-list");
		list.setArgs(1);
		list.setRequired(true);
		this.targetOptions.addOption(list);

		// set target options required
		this.targetOptions.setRequired(true);
	}

	/**
	 * generate command line options for verbosity
	 */
	private void generateVerbosityOptions() {

		Option quiet = new Option("q", "show only results");
		quiet.setLongOpt("quiet");
		this.verbosityOptions.addOption(quiet);

		Option verbose = new Option("v", "verbose output");
		verbose.setLongOpt("verbose");
		this.verbosityOptions.addOption(verbose);

		Option extraVerbose = new Option("x", "extra verbose output");
		extraVerbose.setLongOpt("vverbose");
		this.verbosityOptions.addOption(extraVerbose);
	}

}
