/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.model;

import java.util.List;

import org.biojava.bio.structure.StructureException;

public interface HitAminoAcid extends Comparable<HitAminoAcid> {

	void addAtom(HitAtom atom);

	HitAtom getAtom(String atomType) throws StructureException;

	List<HitAtom> getAtoms();

	char getChainId();

	Character getInsCode();

	int getResidueNumber();

	char getResidueType();

	void setAtoms(List<HitAtom> atoms);

	void setChainId(char chainId);

	void setInsCode(Character insCode);

	void setResidueNumber(int residueNumber);

	void setResidueType(char residueType);
}
