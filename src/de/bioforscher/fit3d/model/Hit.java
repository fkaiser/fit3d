/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.model;

import java.util.List;

import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.jama.Matrix;

public interface Hit extends Comparable<Hit> {

	void addAminoAcid(HitAminoAcid aa);

	String getAlignedSeq();

	List<HitAminoAcid> getAminoAcids();

	String getClassification();

	Double getClassificationDistribution();

	String getEcNumber();

	String getGapSequence();

	String getPdbId();

	String getPfamAcc();

	Double getPvalue();

	double getRmsd();

	Matrix getRotation();

	Atom getShift();

	String getTitle();

	String getType();

	void setAlignedSeq(String alignedSeq);

	void setAminoAcids(List<HitAminoAcid> aminoAcids);

	void setClassification(String classification);

	void setClassificationDistribution(Double classificationDistribution);

	void setEcNumber(String ecNumber);

	void setGapSequence(String string);

	void setPdbId(String pdbId);

	void setPfamAcc(String pfamAcc);

	void setPvalue(Double pvalue);

	void setRmsd(double rmsd);

	void setRotation(Matrix rotation);

	void setShift(Atom shift);

	void setTitle(String title);

	void setType(String type);

}
