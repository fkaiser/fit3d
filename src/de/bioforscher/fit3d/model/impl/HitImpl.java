/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.model.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.jama.Matrix;

import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;

/**
 * A hit found by Fit3D.
 * 
 * @author fkaiser
 * 
 */
public class HitImpl implements Hit, Comparable<Hit>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -621174697833159509L;

	private String alignedSeq;
	private String type;

	private List<HitAminoAcid> aminoAcids = new ArrayList<>();

	private String pdbId;

	private double rmsd;
	private Double pvalue;
	private Double classificationDistribution;
	private String title;
	private String ecNumber;
	private String pfamAcc;
	private String classification;
	private String gapSequence;

	private Matrix rotation;
	private Atom shift;

	/**
	 * add a single amino acid to hit
	 * 
	 * @param aa
	 */
	@Override
	public void addAminoAcid(HitAminoAcid aa) {

		this.aminoAcids.add(aa);
	}

	@Override
	public int compareTo(Hit o) {

		return o.getRmsd() < this.rmsd ? 1 : o.getRmsd() > this.rmsd ? -1 : 0;
	}

	// TODO compare all fields
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hit other = (Hit) obj;
		if (this.aminoAcids == null) {
			if (other.getAminoAcids() != null)
				return false;
		}

		if (!other.getPdbId().equals(this.pdbId)) {

			return false;
		}

		if (other.getRmsd() != -999.0F) {
			if (this.rmsd != other.getRmsd())
				return false;
		}

		if (other.getAlignedSeq() != null) {
			if (!this.alignedSeq.equals(other.getAlignedSeq())) {
				return false;
			}
		}

		int match = 0;
		for (HitAminoAcid aa : this.aminoAcids) {

			for (HitAminoAcid aaOther : other.getAminoAcids()) {

				if (aaOther.equals(aa)) {
					match++;
				}
			}
		}

		if (match != this.aminoAcids.size()) {

			return false;
		}

		return true;
	}

	@Override
	public String getAlignedSeq() {
		return this.alignedSeq;
	}

	@Override
	public List<HitAminoAcid> getAminoAcids() {
		return this.aminoAcids;
	}

	@Override
	public String getClassification() {

		return this.classification;
	}

	@Override
	public Double getClassificationDistribution() {

		return this.classificationDistribution;
	}

	@Override
	public String getEcNumber() {

		return this.ecNumber;
	}

	@Override
	public String getGapSequence() {

		return this.gapSequence;
	}

	@Override
	public String getPdbId() {
		return this.pdbId;
	}

	@Override
	public String getPfamAcc() {
		return this.pfamAcc;
	}

	@Override
	public Double getPvalue() {
		return this.pvalue;
	}

	@Override
	public double getRmsd() {
		return this.rmsd;
	}

	@Override
	public Matrix getRotation() {
		return this.rotation;
	}

	@Override
	public Atom getShift() {
		return this.shift;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public String getType() {
		return this.type;
	}

	// TODO include all fields
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.aminoAcids == null) ? 0 : this.aminoAcids.hashCode())
				+ ((this.alignedSeq == null) ? 0 : this.alignedSeq.hashCode());
		if (this.rmsd != -999) {
			result = (int) (prime * result + this.rmsd);
		}
		return result;
	}

	@Override
	public void setAlignedSeq(String alignedSeq) {
		this.alignedSeq = alignedSeq;
	}

	@Override
	public void setAminoAcids(List<HitAminoAcid> aminoAcids) {
		this.aminoAcids = aminoAcids;
	}

	@Override
	public void setClassification(String classification) {

		this.classification = classification;
	}

	@Override
	public void setClassificationDistribution(Double classificationDistribution) {

		this.classificationDistribution = classificationDistribution;
	}

	@Override
	public void setEcNumber(String ecNumber) {

		this.ecNumber = ecNumber;
	}

	@Override
	public void setGapSequence(String gapSequence) {

		this.gapSequence = gapSequence;
	}

	@Override
	public void setPdbId(String pdbId) {
		this.pdbId = pdbId;
	}

	@Override
	public void setPfamAcc(String pfamAcc) {
		this.pfamAcc = pfamAcc;
	}

	@Override
	public void setPvalue(Double pvalue) {
		this.pvalue = pvalue;
	}

	@Override
	public void setRmsd(double rmsd) {
		this.rmsd = rmsd;
	}

	@Override
	public void setRotation(Matrix rotation) {
		this.rotation = rotation;
	}

	@Override
	public void setShift(Atom shift) {
		this.shift = shift;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(this.pdbId);
		sb.append("_");
		for (HitAminoAcid hAa : this.aminoAcids) {

			sb.append(hAa.toString());
			sb.append("_");
		}
		sb.setLength(sb.length() - 1);

		return sb.toString();
	}
}
