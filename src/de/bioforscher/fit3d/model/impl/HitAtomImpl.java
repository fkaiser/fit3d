/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.model.impl;

import java.io.Serializable;

import de.bioforscher.fit3d.model.HitAtom;

/**
 * An atom of a amino acid of a hit found by Fit3D.
 * 
 * @author fkaiser
 * 
 */
public class HitAtomImpl implements HitAtom, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4607872008403737556L;

	private String name;
	private double x;
	private double y;
	private double z;

	public HitAtomImpl(String name, double x, double y, double z) {

		this.name = name;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public double getX() {
		return this.x;
	}

	@Override
	public double getY() {
		return this.y;
	}

	@Override
	public double getZ() {
		return this.z;
	}

	@Override
	public void setType(String type) {
		this.name = type;
	}

	@Override
	public void setX(double x) {
		this.x = x;
	}

	@Override
	public void setY(double y) {
		this.y = y;
	}

	@Override
	public void setZ(double z) {
		this.z = z;
	}
}
