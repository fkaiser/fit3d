/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.model.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.StructureException;

import de.bioforscher.fit3d.model.HitAminoAcid;
import de.bioforscher.fit3d.model.HitAtom;

/**
 * A single amino acid of a hit found by Fit3D.
 * 
 * @author fkaiser
 * 
 */
public class HitAminoAcidImpl implements HitAminoAcid, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -568343137699103399L;

	private List<HitAtom> atoms = new ArrayList<>();

	private char chainId;
	private int residueNumber;
	private char residueType;

	private Character insCode;

	@Override
	public void addAtom(HitAtom atom) {

		this.atoms.add(atom);
	}

	@Override
	public int compareTo(HitAminoAcid o) {

		if (this.residueNumber != o.getResidueNumber()) {

			return Integer.compare(this.residueNumber, o.getResidueNumber());
		} else {

			return Character.compare(this.chainId, o.getChainId());
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HitAminoAcidImpl other = (HitAminoAcidImpl) obj;
		if (this.chainId != other.getChainId())
			return false;
		if (this.residueNumber != other.getResidueNumber())
			return false;
		if (this.residueType != other.getResidueType())
			return false;
		if (this.insCode != other.getInsCode())
			return false;
		return true;
	}

	@Override
	public HitAtom getAtom(String atomType) throws StructureException {

		for (HitAtom hA : this.atoms) {

			if (hA.getName().equals(atomType)) {

				return hA;
			}
		}

		throw new StructureException("HitAminoAcid does not contain atom "
				+ atomType);
	}

	@Override
	public List<HitAtom> getAtoms() {
		return this.atoms;
	}

	@Override
	public char getChainId() {
		return this.chainId;
	}

	@Override
	public Character getInsCode() {

		return this.insCode;
	}

	@Override
	public int getResidueNumber() {
		return this.residueNumber;
	}

	@Override
	public char getResidueType() {
		return this.residueType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.chainId;
		result = prime * result + this.residueNumber;
		result = prime * result + this.residueType;

		return result;
	}

	@Override
	public void setAtoms(List<HitAtom> atoms) {
		this.atoms = atoms;
	}

	@Override
	public void setChainId(char chainId) {
		this.chainId = chainId;
	}

	@Override
	public void setInsCode(Character insCode) {

		this.insCode = insCode;
	}

	@Override
	public void setResidueNumber(int residueNumber) {
		this.residueNumber = residueNumber;
	}

	@Override
	public void setResidueType(char residueType) {
		this.residueType = residueType;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		if (this.chainId != ' ') {

			sb.append(this.chainId);
		}
		sb.append("-");
		sb.append(this.residueType);
		sb.append(this.residueNumber);
		if (this.insCode != null) {

			sb.append(this.insCode);
		}

		return sb.toString();
	}
}
