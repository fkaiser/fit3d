/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.clustering;

import weka.core.Instance;
import weka.core.NormalizableDistance;
import weka.core.RevisionUtils;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import de.bioforscher.fit3d.Starter;

public class HitDistanceFunction extends NormalizableDistance {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6545822880108607549L;

	@Override
	public double distance(Instance arg0, Instance arg1) {

		for (int i = 1; i < arg0.numValues(); i++) {

			if (arg0.attribute(i).name().equals(arg1.stringValue(0))) {

				return arg0.value(i);
			}
		}

		return 0.0;
	}

	@Override
	public String getRevision() {

		return RevisionUtils.extract("$Revision: 1.0 $");
	}

	/**
	 * Returns an instance of a TechnicalInformation object, containing detailed
	 * information about the technical background of this class, e.g., paper
	 * reference or book this class is based on.
	 * 
	 * @return the technical information about this class
	 */
	public TechnicalInformation getTechnicalInformation() {
		TechnicalInformation result;

		result = new TechnicalInformation(Type.MISC);
		result.setValue(Field.AUTHOR, "Florian Kaiser");
		result.setValue(Field.TITLE, "RMSD distance");
		result.setValue(Field.URL, "http://bioforscher.de");

		return result;
	}

	@Override
	public String globalInfo() {
		return "Implementing RMSD distance function." + Starter.NEWLINE
				+ Starter.NEWLINE + getTechnicalInformation().toString();
	}

	@Override
	protected double updateDistance(double currDist, double diff) {

		double result;

		result = currDist;
		result += diff * diff;

		return result;
	}
}