/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.clustering.io;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.forester.archaeopteryx.AptxUtil;
import org.forester.archaeopteryx.AptxUtil.GraphicsExportType;
import org.forester.archaeopteryx.Configuration;
import org.forester.archaeopteryx.Options;
import org.forester.archaeopteryx.TreeColorSet;
import org.forester.io.parsers.PhylogenyParser;
import org.forester.io.parsers.util.ParserUtils;
import org.forester.phylogeny.Phylogeny;
import org.forester.phylogeny.PhylogenyMethods;
import org.forester.phylogeny.PhylogenyNode;
import org.forester.phylogeny.data.BranchColor;
import org.forester.phylogeny.data.BranchWidth;
import org.forester.phylogeny.data.NodeData.NODE_DATA;
import org.forester.phylogeny.data.NodeVisualization;
import org.forester.phylogeny.data.NodeVisualization.NodeFill;
import org.forester.phylogeny.data.NodeVisualization.NodeShape;
import org.forester.phylogeny.iterators.PhylogenyNodeIterator;

public class TreeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			// Reading-in of a tree from a file.
			final File treefile = new File("/home/fkaiser/test.nwk");
			final PhylogenyParser parser = ParserUtils
					.createParserDependingOnFileType(treefile, true);
			final Phylogeny phy = PhylogenyMethods.readPhylogenies(parser,
					treefile)[0];
			// Creating a node name -> color map.
			final Map<String, Color> colors = new HashMap<String, Color>();
			colors.put("Primates", new Color(255, 255, 0));
			colors.put("PANTR", new Color(255, 0, 255));
			colors.put("HUMAN", new Color(255, 0, 0));
			colors.put("RAT", new Color(155, 0, 0));
			colors.put("MOUSE", new Color(55, 155, 0));
			colors.put("CAVPO", new Color(155, 155, 0));
			colors.put("LOTGI", new Color(155, 155, 255));
			// Setting colors.
			for (final PhylogenyNodeIterator it = phy.iteratorPostorder(); it
					.hasNext();) {
				final PhylogenyNode n = it.next();
				if (colors.containsKey(n.getName())) {
					n.getBranchData().setBranchColor(
							new BranchColor(colors.get(n.getName())));

					NodeVisualization nv = new NodeVisualization();
					nv.setFillColor(new Color(0, 0, 255));
					nv.setSize(80);

					// To make colored subtrees thicker:
					n.getBranchData().setBranchWidth(new BranchWidth(40));

					List<NodeVisualization> nvList = new ArrayList<>();
					nvList.add(nv);

					n.getNodeData().setNodeVisualizations(nvList);
					n.preorderPrint();
					System.out.println(n);
				}
			}
			// Setting up a configuration object.
			final Configuration config = new Configuration();
			config.putDisplayColors(TreeColorSet.BACKGROUND, new Color(255,
					255, 255));
			config.putDisplayColors(TreeColorSet.BRANCH, new Color(0, 0, 0));
			config.putDisplayColors(TreeColorSet.TAXONOMY, new Color(0, 0, 0));
			config.putDisplayColors(TreeColorSet.NODE_BOX, new Color(255, 0, 0));
			config.setPhylogenyGraphicsType(Options.PHYLOGENY_GRAPHICS_TYPE.RECTANGULAR);
			config.setDefaultNodeShapeSize((short) 40);

			config.setTaxonomyColorize(true);
			config.setColorizeBranches(true);
			config.setUseBranchesWidths(true);
			config.setDisplayTaxonomyCode(true);
			config.setBaseFontSize(100);
			config.setDisplayInternalData(true);
			config.setColorLabelsSameAsParentBranch(true);
			config.setDisplayNodeNames(true);
			config.setShowBranchLengthValues(true);
			config.setShowDefaultNodeShapesExternal(true);
			config.setShowDefaultNodeShapesInternal(true);
			config.setShowScale(true);
			config.setDefaultNodeShape(NodeShape.CIRCLE);
			config.setDefaultNodeFill(NodeFill.NONE);
			config.setTaxonomyColorizeNodeShapes(true);
			config.setShowDomainLabels(true);
			config.setDisplayNodeNames(true);
			config.setExtDescNodeDataToReturn(NODE_DATA.TAXONOMY_CODE);
			config.setPrintLineWidth(200);
			config.setTaxonomyColorize(true);

			// Writing to a graphics file.
			AptxUtil.writePhylogenyToGraphicsFile(phy, new File(
					"/home/fkaiser/out.png"), 1080, 1080,
					GraphicsExportType.PNG, config);

		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public TreeTest() {
		// TODO Auto-generated constructor stub
	}

}
