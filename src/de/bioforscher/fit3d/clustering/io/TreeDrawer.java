/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.clustering.io;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.forester.archaeopteryx.AptxUtil;
import org.forester.archaeopteryx.AptxUtil.GraphicsExportType;
import org.forester.archaeopteryx.Configuration;
import org.forester.archaeopteryx.Options;
import org.forester.archaeopteryx.TreeColorSet;
import org.forester.io.parsers.PhylogenyParser;
import org.forester.io.parsers.util.ParserUtils;
import org.forester.phylogeny.Phylogeny;
import org.forester.phylogeny.PhylogenyMethods;
import org.forester.phylogeny.PhylogenyNode;
import org.forester.phylogeny.data.BranchColor;
import org.forester.phylogeny.data.BranchWidth;
import org.forester.phylogeny.data.NodeVisualization.NodeFill;
import org.forester.phylogeny.data.NodeVisualization.NodeShape;
import org.forester.phylogeny.iterators.PhylogenyNodeIterator;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.classification.ClassifierException;
import de.bioforscher.fit3d.clustering.utils.ClusteringUtils;
import de.bioforscher.fit3d.model.Hit;

/**
 * A class to draw the phylogenetic tree of clustering results.
 * 
 * @author fkaiser
 *
 */
public class TreeDrawer {

	private static final double DEFAULT_BRANCH_WIDTH = 5.0;
	private static final short DEFAULT_NODE_SHAPE_SIZE = 10;
	private static final int DEFAULT_FONT_SIZE = 15;
	private final File nwkTreeFile;
	private final List<Hit> clusteredHits;

	public TreeDrawer(File nwkTreeFile, List<Hit> clusteredHits) {

		this.nwkTreeFile = nwkTreeFile;
		this.clusteredHits = clusteredHits;
	}

	public void drawTree() throws FileNotFoundException, IOException,
			ClassifierException {

		PhylogenyParser parser = ParserUtils.createParserDependingOnFileType(
				this.nwkTreeFile, true);
		Phylogeny phy = PhylogenyMethods.readPhylogenies(parser,
				this.nwkTreeFile)[0];

		// colorize phylogeny according to classification
		if (ParameterProvider.getInstance().isClassify()) {

			// iterate over nodes of phylogeny
			for (PhylogenyNodeIterator it = phy.iteratorPostorder(); it
					.hasNext();) {

				PhylogenyNode n = it.next();

				// adapt branch width for every branch
				n.getBranchData().setBranchWidth(
						new BranchWidth(DEFAULT_BRANCH_WIDTH));

				// only consider external nodes
				if (n.isExternal()) {

					StringBuilder sbHitIdentifier = new StringBuilder(n
							.getName().replace("||", "").replace("|", "_"));
					sbHitIdentifier.setLength(sbHitIdentifier.length() - 1);

					for (Hit h : this.clusteredHits) {

						// null check
						if (h.getClassification() == null) {

							continue;
						}

						// skip other hits
						if (!sbHitIdentifier.toString().equals(h.toString())) {

							continue;
						}

						// set branch color
						Color nodeColor = ClusteringUtils.getColorFromHit(h);
						if (nodeColor != null) {

							n.getBranchData().setBranchColor(
									new BranchColor(nodeColor));
						} else {

							throw new ClassifierException(
									"could not determine color for classified hit "
											+ h);
						}

						// set branch width
						if (h.getClassificationDistribution() != null) {

							n.getBranchData().setBranchWidth(
									new BranchWidth(h
											.getClassificationDistribution()
											* DEFAULT_BRANCH_WIDTH));
						}
					}
				}
			}
		}

		// setting up configuration
		Configuration config = new Configuration();
		config.setPhylogenyGraphicsType(Options.PHYLOGENY_GRAPHICS_TYPE.RECTANGULAR);
		config.putDisplayColors(TreeColorSet.BACKGROUND, Color.WHITE);
		config.setDefaultNodeShapeSize(DEFAULT_NODE_SHAPE_SIZE);
		config.setBaseFontSize(DEFAULT_FONT_SIZE);
		config.setDefaultNodeShape(NodeShape.CIRCLE);
		config.setDefaultNodeFill(NodeFill.SOLID);
		config.setColorizeBranches(true);
		config.setShowDefaultNodeShapesExternal(true);
		config.setShowDefaultNodeShapesInternal(true);
		config.setShowBranchLengthValues(true);
		config.setUseBranchesWidths(true);
		config.setDisplayTaxonomyCode(true);
		config.setDisplayNodeNames(true);
		config.setDisplayAsPhylogram(true);

		// write phylogeny to image
		AptxUtil.writePhylogenyToGraphicsFile(phy, new File(
				"/home/fkaiser/out.png"), 1000, 1000, GraphicsExportType.PNG,
				config);
	}
}
