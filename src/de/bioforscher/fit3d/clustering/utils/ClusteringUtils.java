/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.clustering.utils;

import java.awt.Color;

import de.bioforscher.fit3d.model.Hit;

public class ClusteringUtils {

	public static Color getColorFromHit(Hit h) {

		// if no clustering data is available return black
		if (h.getClassification() == null) {

			return Color.BLACK;
		}

		// return class specific color
		if (h.getClassification().equals("o")) {

			return Color.BLACK;
		}
		if (h.getClassification().equals("+")) {

			return new Color(
					1 - h.getClassificationDistribution().floatValue(), h
							.getClassificationDistribution().floatValue(), 0.0F);
		}
		if (h.getClassification().equals("-")) {

			return new Color(h.getClassificationDistribution().floatValue(),
					1 - h.getClassificationDistribution().floatValue(), 0.0F);
		} else {

			return null;
		}
	}
}
