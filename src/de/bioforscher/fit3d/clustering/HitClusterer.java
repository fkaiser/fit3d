/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.clustering;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.Calc;
import org.biojava.bio.structure.SVDSuperimposer;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.jama.Matrix;

import weka.clusterers.HierarchicalClusterer;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.clustering.utils.ClusteringUtils;
import de.bioforscher.fit3d.io.utils.InputOutputUtils;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;
import de.bioforscher.fit3d.model.HitAtom;
import de.bioforscher.fit3d.utils.AtomTypes;
import de.bioforscher.fit3d.utils.StructureUtils;

/**
 * A class implementing a hierarchical clustering of hits based on RMSD.
 * 
 * @author fkaiser
 * 
 */
public class HitClusterer {

	private static final int LINK_TYPE = 7;

	private final List<Hit> hits;
	private List<Hit> validHits;
	private final double rmsdCutoff;

	private final int maxClusterStringSize;
	private Instances data;

	private final String newickName;

	private final String matrixName;

	private final String imageName;

	private final String colorDataName;

	/**
	 * A class for hierarchical clustering of hits found by Fit3D
	 * 
	 * @param hits
	 * @throws ClusterException
	 */
	public HitClusterer(List<Hit> hits, double rmsdCutoff)
			throws ClusterException {

		this.hits = hits;
		this.rmsdCutoff = rmsdCutoff;
		this.maxClusterStringSize = InputOutputUtils
				.getHitClusterStringSize(hits.get(0));
		this.newickName = Starter.DATE_FORMAT.format(new Date(ParameterProvider
				.getInstance().getTimestamp())) + "_clustering.nwk";
		this.matrixName = Starter.DATE_FORMAT.format(new Date(ParameterProvider
				.getInstance().getTimestamp())) + "_clustering.mat";
		this.imageName = Starter.DATE_FORMAT.format(new Date(ParameterProvider
				.getInstance().getTimestamp())) + "_clustering.mat";
		this.colorDataName = Starter.DATE_FORMAT.format(new Date(
				ParameterProvider.getInstance().getTimestamp()))
				+ "_clustering.data";
		initializeInstances();
	}

	/**
	 * performs hierarchical clustering
	 */
	public void cluster() {

		Starter.LOG.info("clustering hits");

		SelectedTag tag = new SelectedTag(LINK_TYPE,
				HierarchicalClusterer.TAGS_LINK_TYPE);

		HierarchicalClusterer hC = new HierarchicalClusterer();

		hC.setDistanceFunction(new HitDistanceFunction());
		hC.setLinkType(tag);

		try {

			// build clusterer
			hC.buildClusterer(this.data);
			// write results
			writeResults(hC);

		} catch (Exception e) {

			e.printStackTrace();
			Starter.LOG.warning(e.getMessage());
		}
	}

	public Instances getData() {
		return this.data;
	}

	public List<Hit> getHits() {
		return this.hits;
	}

	public String getImageName() {
		return this.imageName;
	}

	public int getMaxClusterStringSize() {
		return this.maxClusterStringSize;
	}

	public double getRmsdCutoff() {
		return this.rmsdCutoff;
	}

	public List<Hit> getValidHits() {
		return this.validHits;
	}

	/**
	 * build attributes for data instances (name and RMSD value to all other
	 * instances)
	 * 
	 * @return FastVector of attributes
	 */
	private FastVector buildAttributes() {

		FastVector attributeInfo = new FastVector();
		attributeInfo.addElement(new Attribute("name", (FastVector) null));

		for (int i = 0; i < this.validHits.size(); i++) {

			Hit hit = this.validHits.get(i);

			attributeInfo.addElement(new Attribute(InputOutputUtils
					.getHitClusterString(hit)));
		}

		return attributeInfo;
	}

	/**
	 * get hits valid hits below RMSD cutoff
	 * 
	 * @return valid hits below threshold
	 */
	private List<Hit> computeValidHits() {

		List<Hit> validHits = new ArrayList<>();

		for (Hit hit : this.hits) {

			if (hit.getRmsd() < this.rmsdCutoff) {

				validHits.add(hit);
			}
		}

		// sort valid hits
		Collections.sort(validHits);

		return validHits;
	}

	/**
	 * gets a string array of color information for clustered instaces as input
	 * for iTOL
	 * 
	 * @return colorData
	 */
	private List<String> getColorData() {

		List<String> colorData = new ArrayList<>();

		for (Hit h : this.validHits) {

			Color c = ClusteringUtils.getColorFromHit(h);
			colorData.add(InputOutputUtils.getHitClusterString(h)
					+ ","
					+ String.format("#%02x%02x%02x", c.getRed(), c.getGreen(),
							c.getBlue()));
		}

		return colorData;
	}

	/**
	 * gets a minimal common atom set for hit amino acids
	 * 
	 * @param hitAminoAcids1
	 * @param hitAminoAcids2
	 * @return list of two atom sets
	 */
	private List<Atom[]> getHitAtomSets(List<HitAminoAcid> hitAminoAcids1,
			List<HitAminoAcid> hitAminoAcids2) {

		// initialize atom sets
		List<Atom> hitAtomSet1 = new ArrayList<>();
		List<Atom> hitAtomSet2 = new ArrayList<>();

		// iterate over amino acids 1 and 2
		for (int i = 0; i < hitAminoAcids1.size(); i++) {

			HitAminoAcid hAa1 = hitAminoAcids1.get(i);
			HitAminoAcid hAa2 = hitAminoAcids2.get(i);

			// if residue is valid exchange and not of same type consider only
			// CA atoms
			if (hAa1.getResidueType() != hAa2.getResidueType()) {

				HitAtom hA1 = null;
				try {
					hA1 = hAa1.getAtom(AtomTypes.CA.getAtomName());
				} catch (StructureException e) {

					Starter.LOG.finest("Missing CA atom @" + hAa1 + " "
							+ e.getMessage());

					continue;
				}

				HitAtom hA2 = null;
				try {
					hA2 = hAa2.getAtom(AtomTypes.CA.getAtomName());
				} catch (StructureException e) {

					Starter.LOG.finest("Missing CA atom @" + hAa2 + " "
							+ e.getMessage());

					continue;
				}

				Atom a1 = StructureUtils.toBioJavaAtom(hA1);
				Atom a2 = StructureUtils.toBioJavaAtom(hA2);

				hitAtomSet1.add(a1);
				hitAtomSet2.add(a2);

				continue;
			}

			// consider all atoms present in aa2 for other residues
			Set<String> hAa2HitAtomTypes = StructureUtils
					.getUniqueHitAtomList(hAa2);

			for (String type : hAa2HitAtomTypes) {

				// skip if atoms are not defined
				if (!StructureUtils.isValidAtomType(type)) {

					continue;
				}

				HitAtom hA1 = null;
				try {
					hA1 = hAa1.getAtom(type);
				} catch (StructureException e) {

					Starter.LOG.finest("Missing atom " + type + "@" + hAa1
							+ " " + e.getMessage());

					continue;
				}

				HitAtom hA2 = null;
				try {
					hA2 = hAa2.getAtom(type);
				} catch (StructureException e) {

					Starter.LOG.finest("Missing atom " + type + "@" + hAa2
							+ " " + e.getMessage());

					continue;
				}

				Atom a1 = StructureUtils.toBioJavaAtom(hA1);
				Atom a2 = StructureUtils.toBioJavaAtom(hA2);

				hitAtomSet1.add(a1);
				hitAtomSet2.add(a2);
			}
		}

		// convert lists to array
		List<Atom[]> atomSets = new ArrayList<>();
		atomSets.add(hitAtomSet1.toArray(new Atom[hitAtomSet1.size()]));
		atomSets.add(hitAtomSet2.toArray(new Atom[hitAtomSet2.size()]));

		return atomSets;
	}

	/**
	 * gets instance by identifier
	 * 
	 * @param hitClusterString
	 * @return instance
	 * @throws ClusterException
	 */
	private Instance getInstanceByIdentifier(String hitClusterString)
			throws ClusterException {

		for (int i = 0; i < this.data.numInstances(); i++) {

			Instance currentInstance = this.data.instance(i);
			if (this.data.instance(i).stringValue(0).equals(hitClusterString)) {

				return currentInstance;
			}
		}

		throw new ClusterException("no instance with identifier"
				+ hitClusterString);
	}

	/**
	 * gets matrix values for clustering
	 * 
	 * @param graph
	 * @throws ClusterException
	 */
	private List<String> getMatrix(String graph) throws ClusterException {

		List<String> matrix = new ArrayList<>();

		Pattern pattern = Pattern.compile(".*(\\|\\|.*\\|\\|[\\+|\\-|o]*).*");

		List<String> tipLabels = new ArrayList<>();

		// get tip labels and construct header
		String header = String.format("%-" + this.maxClusterStringSize + "s",
				"");
		for (String s : graph.split(",")) {

			Matcher matcher = pattern.matcher(s);

			if (matcher.matches()) {

				String tip = matcher.group(1);
				tipLabels.add(tip);
				header += String.format("%-" + this.maxClusterStringSize + "s",
						tip);
			}
		}

		// add header to matrix
		matrix.add(header);

		// initialize decimal formatter
		final NumberFormat nf = NumberFormat.getInstance(Starter.LOCALIZATION);
		final DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern("0.00000000");

		// initialize distance function
		HitDistanceFunction d = new HitDistanceFunction();

		// iterate over all tips
		for (String s : tipLabels) {

			// get corresponding instance
			Instance i1 = getInstanceByIdentifier(s);

			// construct matrix row
			String matrixRow = String.format("%-" + this.maxClusterStringSize
					+ "s", s);
			for (String ss : tipLabels) {

				Instance i2 = getInstanceByIdentifier(ss);

				double dist = d.distance(i1, i2);

				matrixRow += String.format("%-" + this.maxClusterStringSize
						+ "s", df.format(dist));

			}

			// add row to matrix
			matrix.add(matrixRow);
		}

		return matrix;
	}

	/**
	 * produces data instances for clustering by pairwise alignment
	 * 
	 * @throws ClusterException
	 */
	private void initializeInstances() throws ClusterException {

		// get valid hits below RMSD cutoff
		this.validHits = computeValidHits();

		if (this.validHits.size() < 2) {

			throw new ClusterException(
					"not enough instances to perform clustering");
		}

		Starter.LOG.info("performing "
				+ (int) Math.pow(this.validHits.size(), 2) + " alignments");

		Instances data = new Instances("hits", buildAttributes(), 0);

		for (int i = 0; i < this.validHits.size(); i++) {

			Hit h1 = this.validHits.get(i);

			double[] values = new double[data.numAttributes()];

			// assign name
			values[0] = data.attribute(0).addStringValue(
					InputOutputUtils.getHitClusterString(h1));

			// fill with pairwise RMSD values
			try {

				for (int j = 0; j < this.validHits.size(); j++) {

					Hit h2 = this.validHits.get(j);

					values[j + 1] = superimposeHits(h1, h2);
				}

				// add hit to data
				Instance ins = new Instance(1.0, values);
				data.add(ins);

			} catch (StructureException e) {

				Starter.LOG.warning(e.getMessage());
			}
		}

		Starter.LOG.info(data.numInstances()
				+ " hits considered for clustering");

		this.data = data;
	}

	/**
	 * superimposes two hits and returns RMSD
	 * 
	 * @param h1
	 * @param h2
	 * @return RMSD of superimposed hits
	 * @throws StructureException
	 * @throws ClusterException
	 */
	private double superimposeHits(Hit h1, Hit h2) throws StructureException,
			ClusterException {

		List<Atom[]> hitAtomSets = getHitAtomSets(h1.getAminoAcids(),
				h2.getAminoAcids());

		Atom[] hitAtomSet1 = hitAtomSets.get(0);
		Atom[] hitAtomSet2 = hitAtomSets.get(1);

		// create SVD superimposer
		SVDSuperimposer svd = new SVDSuperimposer(hitAtomSet2, hitAtomSet1);

		// get rotation matrix
		Matrix rotation = svd.getRotation();

		// get translation
		Atom translation = svd.getTranslation();

		// rotate and shift every atom of motif atom set
		for (Atom a : hitAtomSet1) {

			Calc.rotate(a, rotation);
			Calc.shift(a, translation);
		}

		// calculate RMSD
		return SVDSuperimposer.getRMS(hitAtomSet1, hitAtomSet2);
	}

	/**
	 * write clustering results to files
	 * 
	 * @param hC
	 * @throws Exception
	 */
	private void writeResults(HierarchicalClusterer hC) throws Exception {

		// process newick file
		BufferedWriter outNewick = new BufferedWriter(new FileWriter(
				this.newickName));
		String graph = hC.graph();
		graph = graph.substring(7, graph.length()) + ";";
		outNewick.write(graph);
		outNewick.close();

		// process matrix
		BufferedWriter outMatrix = new BufferedWriter(new FileWriter(
				this.matrixName));
		List<String> matrix = getMatrix(graph);
		for (String s : matrix) {

			outMatrix.write(s);
			outMatrix.newLine();
		}
		outMatrix.close();

		// TODO implement direct iTOL post access
		// process iTOL data file
		BufferedWriter outColorData = new BufferedWriter(new FileWriter(
				this.colorDataName));
		List<String> colorData = getColorData();
		for (String s : colorData) {

			outColorData.write(s);
			outColorData.newLine();
		}
		outColorData.close();

		// TODO process image
		// TreeDrawer td = new TreeDrawer(new File(this.newickName),
		// this.validHits);
		// td.drawTree();

	}
}
