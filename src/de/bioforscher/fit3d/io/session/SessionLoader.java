/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.io.session;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;

/**
 * A class that loads a Fit3D session.
 * 
 * @author fkaiser
 * 
 */
public class SessionLoader {

	/**
	 * loads a Fit3D session from file
	 * 
	 * @param sessionFilePath
	 * @return ParameterProvider of session
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public ParameterProvider load(String sessionFilePath)
			throws ClassNotFoundException, IOException {

		Pattern pattern = Pattern.compile("(\\d+)_session.fit");

		byte[] buffer = new byte[1024];

		// open the zip file stream
		InputStream is = new FileInputStream(sessionFilePath);
		ZipInputStream zis = new ZipInputStream(is);

		ZipEntry entry;
		String sessionFile = null;
		while ((entry = zis.getNextEntry()) != null) {

			String entryName = entry.getName();
			Matcher matcher = pattern.matcher(entryName);
			if (matcher.matches()) {

				sessionFile = entryName;
			}

			String outpath = Starter.TMP_DIR + "/" + entryName;
			FileOutputStream fos = new FileOutputStream(outpath);
			int len = 0;
			while ((len = zis.read(buffer)) > 0) {

				fos.write(buffer, 0, len);
			}

			fos.close();
		}

		is.close();

		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				Starter.TMP_DIR + "/" + sessionFile));

		Object o = ois.readObject();
		ois.close();

		if (o instanceof ParameterProvider) {

			return (ParameterProvider) o;
		}

		return null;
	}
}
