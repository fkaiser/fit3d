/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.io.session;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.io.ResultHandler;

/**
 * A class that saves a Fit3D session.
 * 
 * @author fkaiser
 * 
 */
public class SessionSaver extends Thread {

	@Override
	public void run() {

		List<File> tmpFiles = new ArrayList<>();
		for (int i = 0; i < ParameterProvider.getInstance().getSublistPos(); i++) {

			tmpFiles.add(new File(Starter.TMP_DIR + "/"
					+ ParameterProvider.getInstance().getTimestamp() + "_"
					+ String.format("%06d", i) + ".fit"));
		}

		byte[] buffer = new byte[1024];

		String sessionName = Starter.DATE_FORMAT.format(new Date(
				ParameterProvider.getInstance().getTimestamp()))
				+ "_session.zip";

		try (FileOutputStream fos = new FileOutputStream(sessionName);
				ZipOutputStream zos = new ZipOutputStream(fos);) {

			for (File f : tmpFiles) {
				ZipEntry ze = new ZipEntry(f.getName());
				zos.putNextEntry(ze);

				FileInputStream in = new FileInputStream(f);

				int len;

				while ((len = in.read(buffer)) > 0) {

					zos.write(buffer, 0, len);
				}

				in.close();
				zos.closeEntry();
			}

			ZipEntry ze = new ZipEntry(ParameterProvider.getInstance()
					.getTimestamp() + "_session.fit");

			zos.putNextEntry(ze);
			ObjectOutputStream oos = new ObjectOutputStream(zos);
			oos.writeObject(ParameterProvider.getInstance());

			zos.closeEntry();
			zos.close();

		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
		} catch (IOException e) {

			System.out.println(e.getMessage());
		}

		System.out.println("Session " + sessionName + " saved.");
		System.out.println("Cleaning temporary files...");

		// clean temporary files
		ResultHandler.cleanTemp();

		System.out.println("Goodbye.");
	}
}
