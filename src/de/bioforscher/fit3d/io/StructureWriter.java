/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.biojava.bio.structure.Atom;

import de.bioforscher.fit3d.ParameterProvider;

/**
 * A PDB format writer for a list of atoms.
 * 
 * @author fkaiser
 * 
 */
public class StructureWriter {

	private String filename;
	private Atom[] atoms;

	public StructureWriter(String filename, Atom[] atoms) {

		this.filename = filename;
		this.atoms = atoms;
	}

	/**
	 * write atoms to file in PDB format
	 * 
	 * @throws IOException
	 */
	public void write() throws IOException {

		BufferedWriter out = new BufferedWriter(new FileWriter(
				ParameterProvider.getInstance().getOutputStructuresDirectory()
						+ "/" + this.filename + ".pdb"));

		// write all atoms to PDB file
		for (Atom atom : this.atoms) {

			out.write(atom.toPDB());
		}

		// close buffered writer
		out.close();
	}
}
