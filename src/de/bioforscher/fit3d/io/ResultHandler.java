/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.model.Hit;

/**
 * A class to serialize and deserialize a list of hits.
 * 
 * @author fkaiser
 * 
 */
public class ResultHandler {

	/**
	 * cleans temporary fit files if called
	 */
	public static void cleanTemp() {

		File temp = new File(Starter.TMP_DIR);
		File[] fitFiles = temp.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String filename) {

				return filename.startsWith(String.valueOf(ParameterProvider
						.getInstance().getTimestamp()));
			}
		});

		for (File fitFile : fitFiles) {

			fitFile.delete();
		}
	}

	int lastSubList = 0;

	public int getLastSubList() {

		return this.lastSubList;
	}

	/**
	 * deserializes all hits serialized with the current class instance
	 * 
	 * @return list of hits
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public List<Hit> read() throws FileNotFoundException, IOException,
			ClassNotFoundException {

		List<Hit> totalResults = new ArrayList<>();

		// iterate over all results bunches
		for (int i = 0; i <= this.lastSubList; i++) {

			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					Starter.TMP_DIR + "/"
							+ ParameterProvider.getInstance().getTimestamp()
							+ "_" + String.format("%06d", i) + ".fit"));

			Object o = ois.readObject();
			ois.close();

			if (o instanceof List<?>) {

				totalResults.addAll((List<Hit>) o);
			}
		}

		return totalResults;
	}

	public void setLastSubList(int lastSubList) {
		this.lastSubList = lastSubList;
	}

	/**
	 * serializes a list of hits
	 * 
	 * @param hits
	 * @param subListCounter
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void write(List<Hit> hits, int subListCounter)
			throws FileNotFoundException, IOException {

		// store current bunch of results
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				Starter.TMP_DIR + "/"
						+ ParameterProvider.getInstance().getTimestamp() + "_"
						+ String.format("%06d", subListCounter) + ".fit"));

		oos.writeObject(hits);
		oos.close();

		// set last sub list counter
		this.lastSubList = subListCounter;
	}

	/**
	 * writes all results to a single binary object
	 * 
	 * @param totalResults
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void writeAllResults(List<Hit> totalResults)
			throws FileNotFoundException, IOException {

		// store current bunch of results
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				new File("results.fit")));

		oos.writeObject(totalResults);
		oos.close();
	}
}
