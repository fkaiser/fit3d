/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.io.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.biojava.bio.structure.AminoAcid;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;
import de.bioforscher.fit3d.utils.StructureUtils;

/**
 * A class for providing utilities for IO handling.
 * 
 * @author fkaiser
 *
 */
public class InputOutputUtils {

	/**
	 * formats amino acid to unique nomenclature
	 * 
	 * @param aa
	 * @return formatted string representative of amino acid
	 */
	public static String getAminoAcidString(AminoAcid aa) {

		StringBuilder sb = new StringBuilder();
		sb.append(aa.getChainId());
		sb.append("-");
		sb.append(aa.getAminoType());
		sb.append(aa.getResidueNumber().getSeqNum());
		if (aa.getResidueNumber().getInsCode() != null) {
			sb.append(aa.getResidueNumber().getInsCode());
		}

		return sb.toString();
	}

	/**
	 * gets chain specific identifier of a hit
	 * 
	 * @param h
	 * @return chainSpecificIdentifier
	 */
	public static String getChainSpecifcIdentifier(Hit h) {

		Set<Character> chainSet = new HashSet<>();

		for (HitAminoAcid hAa : h.getAminoAcids()) {

			chainSet.add(hAa.getChainId());
		}

		List<Character> chainList = new ArrayList<>(chainSet);
		Collections.sort(chainList);

		StringBuilder sb = new StringBuilder();

		sb.append(h.getPdbId().toUpperCase());
		sb.append("_");

		for (Character c : chainList) {

			sb.append(String.valueOf(c).toUpperCase());
		}

		return sb.toString();
	}

	/**
	 * calculates all possible amino acid distributions of a given vector
	 * 
	 * @param vector
	 * @return list of amino acid distributions
	 */
	public static List<Map<Character, Integer>> getExchangeDistributions(
			String[] vector) {

		List<Map<Character, Integer>> exchangeDistributions = new ArrayList<>();

		// index of vector elements
		int[] index = new int[vector.length];

		do {
			// storage object
			Map<Character, Integer> d = new HashMap<>();

			// join current object
			for (int j = 0; j < vector.length; j++) {

				char c = vector[j].charAt(index[j]);

				if (!d.containsKey(c)) {

					d.put(c, 1);
				} else {

					d.put(c, d.get(c) + 1);
				}
			}

			exchangeDistributions.add(d);

			// shift indices
			index[index.length - 1]++;

			for (int j = index.length - 1; j > 0; j--) {
				if (index[j] > vector[j].length() - 1) {

					index[j - 1]++;
				}

				index[j] = index[j] % vector[j].length();
			}

		} while (index[0] < vector[0].length());

		return exchangeDistributions;
	}

	/**
	 * gets string representation of command line options
	 * 
	 * @param cl
	 * @return string of command line options
	 */
	public static String getFormattedCommandline(CommandLine cl) {

		StringBuilder clOptions = new StringBuilder();

		// iterate over non-default command line arguments
		for (Option o : cl.getOptions()) {

			if (!(o.getOpt().equals("m") || o.getOpt().equals("l")
					|| o.getOpt().equals("r") || o.getOpt().equals("t"))) {

				if (o.hasArg()) {

					clOptions.append("-");
					clOptions.append(o.getOpt());
					clOptions.append(" ");
					clOptions.append(o.getValue());
					clOptions.append(" ");
				} else {

					clOptions.append("-");
					clOptions.append(o.getOpt());
					clOptions.append(" ");
				}
			}
		}

		return clOptions.toString();
	}

	/**
	 * formats hit amino acid to unique nomenclature
	 * 
	 * @param hAa
	 * @return formatted string representative of hit amino acid
	 */
	public static String getHitAminoAcidString(HitAminoAcid hAa) {

		StringBuilder sb = new StringBuilder();
		sb.append(hAa.getChainId());
		sb.append("-");
		sb.append(hAa.getResidueType());
		sb.append(hAa.getResidueNumber());

		return sb.toString();
	}

	/**
	 * gets unique representation for clustering of hit
	 * 
	 * @param hit
	 * @return unique clustering representation
	 */
	public static String getHitClusterString(Hit hit) {

		StringBuilder sb = new StringBuilder();
		sb.append("||");
		sb.append(hit.getPdbId());
		sb.append("|");
		for (HitAminoAcid hAa : hit.getAminoAcids()) {

			sb.append(hAa);
			sb.append("|");
		}

		sb.setLength(sb.length() - 1);
		sb.append("||");

		// indicate associated class
		if (hit.getClassification() != null) {

			sb.append(hit.getClassification());
		}

		return sb.toString();
	}

	/**
	 * gets the maximal expected cluster string size for a hit
	 * 
	 * @param hit
	 * @return maximal expected cluster string size
	 */
	public static int getHitClusterStringSize(Hit hit) {

		// ||ABCD(|C-1234|C-1234|)n|
		return 7 + hit.getAminoAcids().size() * 8 + 2;
	}

	/**
	 * gets output filename for hit
	 * 
	 * @param hit
	 * @return output filename as string
	 */
	public static String getOutputFilename(Hit hit) {

		NumberFormat nf = NumberFormat.getInstance(Starter.LOCALIZATION);
		DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern("0.0000");

		StringBuilder sb = new StringBuilder();
		sb.append(df.format(hit.getRmsd()));
		sb.append("_");
		sb.append(hit.getPdbId());
		sb.append("_");
		sb.append(hit.getAlignedSeq());
		sb.append("_");

		if (StructureUtils.isSameChain(hit)) {

			sb.append("intra");
		} else {

			sb.append("inter");
		}

		sb.append("_");

		for (HitAminoAcid hAa : hit.getAminoAcids()) {

			sb.append(hAa.toString());
			sb.append("_");
		}

		sb.setLength(sb.length() - 1);

		return sb.toString();
	}

	/**
	 * converts atom name to full PDB qualified name
	 * 
	 * @param atomName
	 * @return full atom name
	 */
	public static String toFullAtomName(String atomName) {

		return String.format(" %-3s", atomName);
	}

	/**
	 * writes mapped sequence gaps to FASTA file
	 * 
	 * @param totalResults
	 * @throws IOException
	 */
	public static void writeGapSequencesToFasta(List<Hit> totalResults)
			throws IOException {

		String fileName = Starter.DATE_FORMAT.format(new Date(ParameterProvider
				.getInstance().getTimestamp())) + "_seq.fasta";
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(
				fileName)));

		// write all atoms to PDB file
		for (Hit h : totalResults) {

			String gapSequence = h.getGapSequence();

			if (gapSequence != null) {

				out.write(">" + h);
				out.write(Starter.NEWLINE);
				out.write(h.getGapSequence());
				out.write(Starter.NEWLINE);
			}
		}

		// close buffered writer
		out.close();
	}
}
