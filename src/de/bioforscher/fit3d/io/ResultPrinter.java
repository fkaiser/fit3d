/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;

/**
 * A class to format results.
 * 
 * @author fkaiser
 *
 */
public class ResultPrinter {

	private List<Hit> hits = new ArrayList<>();
	private boolean sortHits = false;

	public ResultPrinter(List<Hit> hits, boolean sortHits) {

		this.hits = hits;
		this.sortHits = sortHits;

		// sort hits if enabled
		if (this.sortHits) {

			Collections.sort(this.hits);
		}
	}

	/**
	 * prints all results to console
	 */
	public void printToConsole() {

		System.out.println();
		System.out.println("\t" + getHeaderLine("\t"));

		for (Hit hit : this.hits) {

			System.out.println("\t" + getResultLine(hit, "\t"));
		}

		System.out.println();
	}

	/**
	 * prints all results to result file
	 * 
	 * @throws IOException
	 */
	public void printToFile() throws IOException {

		File resultFile = new File(ParameterProvider.getInstance().getResultFilePath());

		File path = resultFile.getParentFile();
		if (path != null) {
			if (!path.exists()) {

				path.mkdirs();
			}
		}

		BufferedWriter out = new BufferedWriter(new FileWriter(resultFile));

		out.write(getHeaderLine(","));
		out.newLine();

		for (Hit hit : this.hits) {

			out.write(getResultLine(hit, ","));
			out.newLine();
		}

		out.close();
	}

	/**
	 * gets header of result line
	 * 
	 * @param separator
	 * @return header
	 */
	private String getHeaderLine(String separator) {

		StringBuilder header = new StringBuilder();
		header.append("LRMSD");
		header.append(separator);
		if (ParameterProvider.getInstance().isPvalues()) {

			header.append("p-value");
			header.append(separator);
			// fill with tabs for console output
			if (separator.equals("\t")) {

				header.append("\t\t");
			}
		}
		header.append("PDB-ID");
		header.append(separator);
		if (ParameterProvider.getInstance().isEcMapping()) {

			// for console output print double separator
			if (separator.equals("\t")) {
				header.append(String.format("%-12s", "EC"));
				header.append(separator);
			} else {

				header.append("EC");
				header.append(separator);
			}
		}

		if (ParameterProvider.getInstance().isPfamMapping()) {

			// for console output print double separator
			if (separator.equals("\t")) {
				header.append(String.format("%-8s", "Pfam"));
				header.append(separator);
			} else {

				header.append("Pfam");
				header.append(separator);
			}
		}

		if (ParameterProvider.getInstance().isClassify()) {

			header.append("class");
			header.append(separator);
			header.append("prob");
			header.append(separator);
		}

		header.append("occ");
		header.append(separator);
		header.append("seq");
		header.append(separator);

		// append filling whitespaces for console output
		if (separator.equals("\t") && (ParameterProvider.getInstance().isTitleMapping()
				|| ParameterProvider.getInstance().isGapSeqMapping())) {

			header.append(String.format("%-" + (this.hits.get(0).getAminoAcids().size() * 9) + "s", "motif"));
		} else {

			header.append("motif");
		}

		if (ParameterProvider.getInstance().isTitleMapping()) {

			header.append(separator);
			header.append("title");
		}

		if (ParameterProvider.getInstance().isGapSeqMapping()) {

			header.append(separator);
			header.append("gapseq");
		}

		return header.toString();
	}

	/**
	 * gets result line with specified separator
	 * 
	 * @param hit
	 * @return result line
	 */
	private String getResultLine(Hit hit, String separator) {

		final NumberFormat nfFourDecimals = NumberFormat.getInstance(Starter.LOCALIZATION);
		final DecimalFormat dfFourDecimals = (DecimalFormat) nfFourDecimals;
		dfFourDecimals.applyPattern("0.0000");

		final NumberFormat nfPvalue = NumberFormat.getInstance(Starter.LOCALIZATION);
		final DecimalFormat dfPvalue = (DecimalFormat) nfPvalue;
		dfPvalue.applyPattern("0.000000000000E0");

		StringBuilder resultLine = new StringBuilder();
		resultLine.append(dfFourDecimals.format(hit.getRmsd()));
		resultLine.append(separator);
		if (ParameterProvider.getInstance().isPvalues()) {

			if (hit.getPvalue() != null) {
				resultLine.append(dfPvalue.format(hit.getPvalue()));
				resultLine.append(separator);
			} else {
				// for console output print double separator

				if (separator.equals("\t")) {
					resultLine.append("?\t\t");
					resultLine.append(separator);
				} else {

					resultLine.append("?");
					resultLine.append(separator);
				}
			}
		}
		resultLine.append(hit.getPdbId().toUpperCase());
		resultLine.append(separator);
		if (ParameterProvider.getInstance().isEcMapping()) {

			// append filling whitespaces for console output
			if (separator.equals("\t")) {
				resultLine.append(String.format("%-12s", hit.getEcNumber()));
				resultLine.append(separator);
			} else {

				resultLine.append(hit.getEcNumber());
				resultLine.append(separator);
			}
		}

		if (ParameterProvider.getInstance().isPfamMapping()) {

			// append filling whitespaces for console output
			if (separator.equals("\t")) {
				resultLine.append(String.format("%-8s", hit.getPfamAcc()));
				resultLine.append(separator);
			} else {

				resultLine.append(hit.getPfamAcc());
				resultLine.append(separator);
			}
		}

		if (ParameterProvider.getInstance().isClassify()) {

			resultLine.append(hit.getClassification());
			resultLine.append(separator);
			if (hit.getClassificationDistribution() != null) {
				resultLine.append(dfFourDecimals.format(hit.getClassificationDistribution()));
			} else {

				resultLine.append("?");
			}
			resultLine.append(separator);
		}

		resultLine.append(hit.getType());
		resultLine.append(separator);
		resultLine.append(hit.getAlignedSeq());
		resultLine.append(separator);

		// TODO sort amino acids of hit?
		// sort amino acids for output
		// if (this.sortHits) {
		//
		// Collections.sort(hit.getAminoAcids());
		// }

		StringBuilder sbMotif = new StringBuilder();
		for (HitAminoAcid hAa : hit.getAminoAcids()) {

			sbMotif.append(hAa.toString());
			sbMotif.append(" ");
		}
		sbMotif.setLength(sbMotif.length() - 1);

		// append filling whitespaces for console output
		if (separator.equals("\t") && (ParameterProvider.getInstance().isTitleMapping()
				|| ParameterProvider.getInstance().isGapSeqMapping())) {

			resultLine.append(
					String.format("%-" + (this.hits.get(0).getAminoAcids().size() * 9) + "s", sbMotif.toString()));
		} else {

			resultLine.append(sbMotif.toString());
		}

		if (ParameterProvider.getInstance().isTitleMapping()) {
			resultLine.append(separator);
			resultLine.append("\"");
			if (hit.getTitle() != null) {
				resultLine.append(hit.getTitle());
			} else {

				resultLine.append("?");
			}
			resultLine.append("\"");
		}

		if (ParameterProvider.getInstance().isGapSeqMapping()) {

			resultLine.append(separator);
			if (hit.getGapSequence() != null) {
				resultLine.append(hit.getGapSequence());
			} else {

				resultLine.append("?");
			}
		}

		return resultLine.toString();
	}
}
