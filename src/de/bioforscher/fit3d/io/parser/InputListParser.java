/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.io.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A reader for a given input list of PDB-IDs.
 * 
 * @author fkaiser
 * 
 */
public class InputListParser {

	private static final String regex = "(^\\w{4}$)";
	private File path;

	public InputListParser(File path) {

		this.path = path;
	}

	public List<String> readInput() throws IOException {

		// initialize input list
		List<String> input = new ArrayList<>();

		// initialize buffered reader
		BufferedReader r = new BufferedReader(new FileReader(path));
		String s;

		// while line is given by reader
		while ((s = r.readLine()) != null) {

			// extract PDB-ID
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(s);

			if (matcher.find()) {

				// add PDB-ID to input list
				input.add(matcher.group(1));
			}
			if (new File(s).exists()) {

				// add file to input list
				input.add(s);
			}
		}

		r.close();

		return input;
	}
}
