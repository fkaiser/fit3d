/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.io.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Chain;
import org.biojava.bio.structure.Group;
import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.io.FileParsingParameters;
import org.biojava.bio.structure.io.PDBFileReader;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;

/**
 * A parser for PDB structures.
 * 
 * @author fkaiser
 * 
 */
public class StructureParser {

	private final String pdbId;
	private final File pdbFile;
	private Structure structure;

	public StructureParser(String pdbId, File pdbFile) {

		this.pdbId = pdbId;
		this.pdbFile = pdbFile;
	}

	/**
	 * returns all amino acids of all chains in structure
	 * 
	 * @return all amino acids of all chains
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws StructureException
	 */
	public List<AminoAcid> getAllAminoAcids() throws IOException,
			FileNotFoundException, StructureException {

		// initialize array to hold all amino acids in the structure
		List<AminoAcid> allAminoAcids = new ArrayList<>();

		// set PDB reader parsing parameters
		FileParsingParameters params = new FileParsingParameters();
		params.setAlignSeqRes(false);

		// disable parse secondary structure
		if (ParameterProvider.getInstance() != null) {

			params.setParseSecStruc(ParameterProvider.getInstance()
					.isClassify());
		} else {

			params.setParseSecStruc(false);
		}

		// TODO check if correct parse of all atoms
		// if (ParameterProvider.getInstance() != null) {
		//
		// // set accepted atom names to user defined atoms
		// params.setAcceptedAtomNames(ParameterProvider.getInstance()
		// .getAtoms());
		// String[] an = params.getAcceptedAtomNames();
		// System.out.println(an);
		// } else {
		//
		// params.setAcceptedAtomNames(Starter.PDB_ATOMS);
		// String[] an = params.getAcceptedAtomNames();
		// System.out.println(an);
		// }

		// parse all non-hydrogen atoms
		params.setAcceptedAtomNames(Starter.PDB_ATOMS);

		// create PDB reader
		PDBFileReader pdbReader = new PDBFileReader();

		if (ParameterProvider.getInstance() != null) {
			pdbReader
					.setPath(ParameterProvider.getInstance().getPdbDirectory());
			pdbReader.setPdbDirectorySplit(ParameterProvider.getInstance()
					.isPdbDirectorySplit());
		} else {

			pdbReader.setPath(Starter.TMP_DIR);
			pdbReader.setPdbDirectorySplit(true);
		}

		pdbReader.setFileParsingParameters(params);

		// TODO get autofetch working again
		pdbReader.setAutoFetch(true);

		// TODO check what this is
		// pdbReader.setFetchCurrent(true);

		// download if structure was not provided
		if (this.pdbFile == null) {

			// convert PDB-ID to lower case
			String id = this.pdbId.substring(0, 4).toLowerCase();
			this.structure = pdbReader.getStructureById(id);
		}

		// open local structure file
		if (this.pdbFile != null) {

			this.structure = pdbReader.getStructure(this.pdbFile);
		}

		// get a list of all chains of the first model
		List<Chain> chainsOfModel = this.structure.getModel(0);

		// iterate over all chains of the model
		for (Chain c : chainsOfModel) {

			// assemble list of all amino acids
			for (Group g : c.getAtomGroups()) {

				if (g instanceof AminoAcid) {

					allAminoAcids.add((AminoAcid) g);
				}
			}
		}

		return allAminoAcids;
	}

	public Structure getStructure() {

		return this.structure;
	}
}
