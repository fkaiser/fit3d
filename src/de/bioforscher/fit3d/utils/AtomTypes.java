/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.utils;

/**
 * All valid PDB format atom types.
 * 
 * @author fkaiser
 * 
 */
public enum AtomTypes {

	N("N"), ND("ND"), ND1("ND1"), ND2("ND2"), NE("NE"), NE1("NE1"), NE2("NE2"), NH(
			"NH"), NH1("NH1"), NH2("NH2"), NZ("NZ"), C("C"), CA("CA"), CB("CB"), CG(
			"CG"), CG1("CG1"), CG2("CG2"), CG3("CG3"), CD("CD"), CD1("CD1"), CD2(
			"CD2"), CD3("CD3"), CE("CE"), CE1("CE1"), CE2("CE2"), CE3("CE3"), CZ(
			"CZ"), CZ1("CZ1"), CZ2("CZ2"), CZ3("CZ3"), CH("CH"), CH1("CH1"), CH2(
			"CH2"), CH3("CH3"), O("O"), OD("OD"), OD1("OD1"), OD2("OD2"), OE(
			"OE"), OE1("OE1"), OE2("OE2"), S("S"), SA("SA"), SB("SB"), SD("SD"), SG(
			"SG");

	private String atomName;

	private AtomTypes(String atomName) {

		this.atomName = atomName;
	}

	public String getAtomName() {

		return this.atomName;
	}
}
