/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.StructureException;

import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.io.parser.StructureParser;

public class MotifExtractor {

	private static final String regex = "([A-Za-z0-9]\\-[A-Za-z0-9]\\d+_)+";
	private static final int MOTIF_SIZE_LIMIT = 2;
	private final String inputFile;
	private final String extractString;
	private String extractedMotifFileName;

	public MotifExtractor(String inputFile, String extractString) throws MotifExtractorException {

		this.inputFile = inputFile;
		this.extractString = extractString;

		Pattern pattern = Pattern.compile(regex);

		// append final underline to match regular expression
		Matcher matcher = pattern.matcher(extractString + "_");

		if (!matcher.matches()) {

			throw new MotifExtractorException("please specify a valid extraction string");
		}
	}

	public void extract() throws FileNotFoundException, IOException, StructureException, MotifExtractorException {

		StructureParser sp = new StructureParser(null, new File(this.inputFile));

		List<AminoAcid> aminoAcids = sp.getAllAminoAcids();

		List<AminoAcid> motif = new ArrayList<>();

		for (String s : this.extractString.split("_")) {

			String chainId = s.split("-")[0];
			char resType = s.split("-")[1].charAt(0);
			int resNum = Integer.valueOf(s.split("-")[1].substring(1));

			List<AminoAcid> matchingResidues = aminoAcids.stream()
					.filter(aa -> aa.getChainId().equalsIgnoreCase(chainId))
					.filter(aa -> (aa.getResidueNumber().getSeqNum().equals(resNum)))
					.filter(aa -> aa.getAminoType().equals(resType)).collect(Collectors.toList());

			if (matchingResidues.size() > 1) {

				Starter.LOG.warning("found ambigious residues " + s + " in input structure for extraction, ignored...");
			}
			if (matchingResidues.size() == 0) {

				Starter.LOG.warning(
						"could not find matching residue " + s + " in input structure for extraction, ignored...");
			} else {

				motif.addAll(matchingResidues);
			}
		}

		if (motif.size() < MOTIF_SIZE_LIMIT) {

			throw new MotifExtractorException("the extracted motif contains not enough amino acids");
		}

		List<Atom> motifAtomsList = new ArrayList<>();

		for (AminoAcid aminoAcid : motif) {

			motifAtomsList.addAll(aminoAcid.getAtoms());
		}

		// compute motif sequence
		StringBuilder motifSeq = new StringBuilder();
		Set<String> chainList = new HashSet<>();
		for (AminoAcid aa : motif) {

			motifSeq.append(aa.getAminoType());
			chainList.add(aa.getChainId());
		}

		this.extractedMotifFileName = "motif_" + motifSeq + ".pdb";

		Starter.LOG.info("writing extracted motif " + this.extractedMotifFileName);

		BufferedWriter out = new BufferedWriter(new FileWriter(this.extractedMotifFileName));

		// write all atoms to PDB file
		out.write("REMARK SOURCE " + this.inputFile + "\n");
		for (Atom atom : motifAtomsList) {

			out.write(atom.toPDB());
		}

		// close buffered writer
		out.close();

	}

	public String getExtractedMotifFileName() {
		return this.extractedMotifFileName;
	}

	public void setExtractedMotifFileName(String extractedMotifFileName) {
		this.extractedMotifFileName = extractedMotifFileName;
	}

}
