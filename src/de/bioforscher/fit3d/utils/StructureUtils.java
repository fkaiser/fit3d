/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Atom;
import org.biojava.bio.structure.AtomImpl;
import org.biojava.bio.structure.Calc;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.rcsb.RCSBDescription;
import org.biojava.bio.structure.rcsb.RCSBDescriptionFactory;
import org.biojava.bio.structure.rcsb.RCSBPolymer;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.bioforscher.fit3d.ParameterProvider;
import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.io.parser.StructureParser;
import de.bioforscher.fit3d.io.utils.InputOutputUtils;
import de.bioforscher.fit3d.mapping.utils.EcAnnotaion;
import de.bioforscher.fit3d.mapping.utils.PfamAnnotation;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;
import de.bioforscher.fit3d.model.HitAtom;

public class StructureUtils {

	/**
	 * assigns am EC annotation from a list of annotations to a hit
	 * 
	 * @return
	 */
	public static String assignEcAnnotation(List<EcAnnotaion> annotations, Hit hit) {

		Set<String> hAnnotations = new HashSet<>();

		int annotationCounter = 0;
		for (EcAnnotaion ea : annotations) {

			for (HitAminoAcid hAa : hit.getAminoAcids()) {

				if (ea.getChainIds().contains(hAa.getChainId())) {

					hAnnotations.add(ea.getEcClass());
					annotationCounter++;
				}
			}
		}

		switch (hAnnotations.size()) {
		case 0:

			Starter.LOG.fine("no EC annotation available for " + hit);
			return "?";

		case 1:

			// check if all motif incorporated amino acids have an annotation
			if (annotationCounter == hit.getAminoAcids().size()) {
				return new ArrayList<>(hAnnotations).get(0);
			} else {

				Starter.LOG.fine("some amino acids of " + hit + " have no EC annotation");
				return "?";
			}
		default:

			Starter.LOG.fine("ambiguous EC annotations available for " + hit);
			return "?";
		}
	}

	/**
	 * assigns a Pfam annotation from a list of annotations to a hit
	 * 
	 * @param annotations
	 * @param hit
	 * @return pfamAcc
	 */
	public static String assignPfamAnnotation(List<PfamAnnotation> annotations, Hit hit) {

		Set<String> hAnnotations = new HashSet<>();

		int annotationCounter = 0;
		for (PfamAnnotation pa : annotations) {

			for (HitAminoAcid hAa : hit.getAminoAcids()) {

				int hResNum = hAa.getResidueNumber();

				if ((hAa.getChainId() == pa.getChainId()) && (hResNum >= pa.getStartPdb())
						&& (hResNum <= pa.getStopPdb())) {

					hAnnotations.add(pa.getAccNumber());
					annotationCounter++;
				}
			}
		}

		switch (hAnnotations.size()) {
		case 0:

			Starter.LOG.fine("no Pfam annotation available for " + hit);
			return "?";

		case 1:

			// check if all motif incorporated amino acids have an annotation
			if (annotationCounter == hit.getAminoAcids().size()) {
				return new ArrayList<>(hAnnotations).get(0);
			} else {

				Starter.LOG.fine("some amino acids of " + hit + " have no Pfam annotation");
				return "?";
			}
		default:

			Starter.LOG.fine("ambiguous Pfam annotations available for " + hit);
			return "?";
		}
	}

	/**
	 * calculates amino acid type distribution
	 * 
	 * @param aminoAcids
	 * @return distribution of given amino acids
	 */
	public static Map<Character, Integer> getAminoAcidDistribution(List<AminoAcid> aminoAcids) {

		// initialize HashMap<residue,occurrence>
		HashMap<Character, Integer> distribution = new HashMap<>();

		// iterate over all given amino acids
		for (AminoAcid aa : aminoAcids) {

			// if amino acids already has an entry
			if (distribution.containsKey(aa.getAminoType())) {

				// increase counter
				Integer count = distribution.get(aa.getAminoType());
				distribution.put(aa.getAminoType(), count + 1);
			} else {

				// initialize counter at first occurrence
				distribution.put(aa.getAminoType(), 1);
			}
		}

		return distribution;
	}

	public static List<AminoAcid> getAminoAcidsAroundCentroid(List<AminoAcid> aminoAcids, Atom centroid, double radius)
			throws StructureException {

		double powRadius = Math.pow(radius, 2);

		List<AminoAcid> temp = new ArrayList<>();
		for (AminoAcid aa : aminoAcids) {

			if (Calc.getDistanceFast(aa.getAtom(AtomTypes.CA.getAtomName()), centroid) < powRadius
					&& Calc.getDistanceFast(aa.getAtom(AtomTypes.CA.getAtomName()), centroid) != 0.0) {

				temp.add(aa);
			}
		}

		return temp;
	}

	/**
	 * gets BioJava amino acids from hit
	 * 
	 * @param hit
	 * @return bioJavaAminoAcids
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws StructureException
	 */
	public static List<AminoAcid> getBioJavaAminoAcidsFromHit(Hit hit)
			throws FileNotFoundException, IOException, StructureException {

		StructureParser parser = new StructureParser(hit.getPdbId(), null);

		List<AminoAcid> bioJavaAminoAcids = new ArrayList<>();
		List<AminoAcid> aminoAcids = parser.getAllAminoAcids();

		Map<String, AminoAcid> aminoAcidStorage = new HashMap<>();
		for (AminoAcid aa : aminoAcids) {

			aminoAcidStorage.put(InputOutputUtils.getAminoAcidString(aa), aa);
		}
		for (HitAminoAcid hAa : hit.getAminoAcids()) {

			bioJavaAminoAcids.add(aminoAcidStorage.get(hAa.toString()));
		}

		// for (HitAminoAcid hAa : hit.getAminoAcids()) {
		//
		// for (AminoAcid aa : aminoAcids) {
		//
		// if (aa.getChainId().equals(String.valueOf(hAa.getChainId()))
		// && aa.getResidueNumber()
		// .getSeqNum()
		// .equals(Integer.valueOf(hAa.getResidueNumber()))
		// && aa.getAminoType().equals(
		// Character.valueOf(hAa.getResidueType()))) {
		//
		// if (aa.getResidueNumber().getInsCode() == null
		// && hAa.getInsCode() == null) {
		//
		// bioJavaAminoAcids.add(aa);
		// }
		// if (aa.getResidueNumber().getInsCode() != null
		// && hAa.getInsCode() != null) {
		//
		// if (aa.getResidueNumber().getInsCode()
		// .equals(hAa.getInsCode())) {
		//
		// bioJavaAminoAcids.add(aa);
		// }
		// }
		// }
		// }
		//
		// }

		return bioJavaAminoAcids;
	}

	/**
	 * retrieves EC annotations of a target PDB-ID
	 * 
	 * @param pdbId
	 * @return
	 */
	public static List<EcAnnotaion> getEcAnnotation(String pdbId) {

		List<EcAnnotaion> annotations = new ArrayList<>();

		// TODO own PDB structures need to be converted to PDB-ID
		RCSBDescription description = RCSBDescriptionFactory.get(pdbId);
		if (description != null) {

			for (RCSBPolymer p : description.getPolymers()) {

				EcAnnotaion ea = new EcAnnotaion();

				if (p.getEnzClass() == null) {

					continue;
				}

				for (Character c : p.getChains()) {

					ea.addChainId(c);
				}

				ea.setEcClass(p.getEnzClass());

				// add annoation to list
				annotations.add(ea);
			}
		}

		return annotations;
	}

	/**
	 * retrieves RESTful data from RCSB and extracts EC number
	 * 
	 * @param pdbId
	 * @return ecNumber
	 */
	public static String getEcNumber(Hit h) {

		// get unique list of chains
		Set<Character> chainIds = StructureUtils.getUniqueChainList(h);

		// initialize storage for Pfam accessions
		Set<String> ecClasses = new HashSet<>();

		for (Character c : chainIds) {

			// TODO own PDB structures need to be converted to PDB-ID
			RCSBDescription description = RCSBDescriptionFactory.get(h.getPdbId());
			if (description == null) {

				Starter.LOG.warning("unable to get RESTful data for " + h);
				return "?";
			}

			for (RCSBPolymer p : description.getPolymers()) {

				if (p.getChains().contains(c)) {

					String ecNumber = p.getEnzClass();

					if (ecNumber == null) {

						return "?";
					}

					ecClasses.add(ecNumber);
				}
			}
		}

		// return unknown for ambiguous Pfam accession
		if (ecClasses.size() != 1) {

			Starter.LOG.fine("intermolecular hit " + h + " has ambiguous EC numbers");
			return "?";

		} else {

			// return Pfam accession
			return new ArrayList<>(ecClasses).get(0);
		}
	}

	/**
	 * calculates maximal squared extent in Angstroem of given amino acids
	 * 
	 * @param aminoAcids
	 * @return maximal expansion in Angstroem
	 * @throws StructureException
	 */
	public static double getMaxSquaredExtent(List<AminoAcid> aminoAcids) throws StructureException {

		double maxExpansion = 0.0;

		// measure distance from every amino acid to every amino acid
		for (AminoAcid aa1 : aminoAcids) {

			for (AminoAcid aa2 : aminoAcids) {

				double dist = Calc.getDistanceFast(aa1.getAtom(AtomTypes.CA.getAtomName()),
						aa2.getAtom(AtomTypes.CA.getAtomName()));

				// check if maximal distance increased
				if (maxExpansion < dist) {

					maxExpansion = dist;
				}
			}
		}

		return maxExpansion;
	}

	/**
	 * retrieves all available Pfam annotations for a given PDB ID
	 * 
	 * @param pdbId
	 * @return annotations
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public static List<PfamAnnotation> getPfamAnnotations(String pdbId) {

		// define pattern for residue numbers with insertion code
		final Pattern p = Pattern.compile("(\\d+)[A-Z]");

		// initialize storage for annotations
		List<PfamAnnotation> annotations = new ArrayList<>();

		try {

			// retrieve RESTful data
			URL url = new URL("http://rcsb.org/pdb/rest/hmmer?structureId=" + pdbId);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = dBuilder.parse(conn.getInputStream());

			NodeList nList = doc.getElementsByTagName("pfamHit");

			// iterate over all Pfam annotations
			for (int i = 0; i < nList.getLength(); i++) {

				Node n = nList.item(i);
				NamedNodeMap atts = n.getAttributes();

				PfamAnnotation pa = new PfamAnnotation();
				pa.setChainId(atts.getNamedItem("chainId").getNodeValue().charAt(0));

				// parse start and stop value of sequence
				String startPdbStringValue = atts.getNamedItem("pdbResNumStart").getNodeValue();
				Matcher m1 = p.matcher(startPdbStringValue);
				int startPdb;
				if (m1.matches()) {

					startPdb = Integer.valueOf(m1.group(1));
				} else {

					startPdb = Integer.valueOf(startPdbStringValue);
				}
				pa.setStartPdb(startPdb);
				String stopPdbStringValue = atts.getNamedItem("pdbResNumEnd").getNodeValue();
				int stopPdb;
				Matcher m2 = p.matcher(stopPdbStringValue);
				if (m2.matches()) {

					stopPdb = Integer.valueOf(m2.group(1));
				} else {

					stopPdb = Integer.valueOf(stopPdbStringValue);
				}
				pa.setStopPdb(stopPdb);

				pa.setAccNumber(atts.getNamedItem("pfamAcc").getNodeValue().split("\\.")[0]);

				annotations.add(pa);
			}

			conn.disconnect();

		} catch (ParserConfigurationException | SAXException | IOException e) {

			Starter.LOG.warning("could not map Pfam annotations for " + pdbId);
		}

		return annotations;
	}

	/**
	 * returns residue type of a given residue number if contained in amino acid
	 * list
	 * 
	 * @param residueNumber
	 * @param aminoAcids
	 * @return type of residue
	 */
	public static Character getResidueTypeFromResidueNumber(Integer residueNumber, List<AminoAcid> aminoAcids) {

		// iterate over all amino acids and compare sequence number
		for (AminoAcid aa : aminoAcids) {

			if (aa.getResidueNumber().getSeqNum().equals(residueNumber)) {

				return aa.getAminoType();
			}
		}

		return null;
	}

	/**
	 * gets a set of unique PDB atom identifiers from a list of amino acids
	 * 
	 * @param aminoAcids
	 * @return unique set of PDB atom identifiers
	 */
	public static Set<String> getUniqueAtomList(AminoAcid... aminoAcids) {

		// initialize list for atom names
		List<String> atomNames = new ArrayList<>();

		// iterate over all atoms of every amino acid
		for (AminoAcid aa : aminoAcids) {

			for (Atom a : aa.getAtoms()) {

				// add atom names to list
				atomNames.add(a.getName());
			}
		}

		// unify atom names
		Set<String> uniqueAtomNames = new LinkedHashSet<>(atomNames);

		return uniqueAtomNames;
	}

	/**
	 * @param h
	 * @return
	 */
	public static Set<Character> getUniqueChainList(Hit h) {
		// collect unique chain list
		Set<Character> chainIds = new HashSet<>();
		if (StructureUtils.isSameChain(h)) {

			chainIds.add(h.getAminoAcids().get(0).getChainId());
		} else {

			for (HitAminoAcid hAa : h.getAminoAcids()) {

				chainIds.add(hAa.getChainId());
			}
		}
		return chainIds;
	}

	/**
	 * gets a set of unique PDB atom identifiers from a list of hit amino acids
	 * 
	 * @param hitAminoAcids
	 * @return unique set of PDB atom identifiers
	 */
	public static Set<String> getUniqueHitAtomList(HitAminoAcid... hitAminoAcids) {

		// initialize list for atom names
		List<String> hitAtomNames = new ArrayList<>();

		// iterate over all atoms of every amino acid
		for (HitAminoAcid hAa : hitAminoAcids) {

			for (HitAtom hA : hAa.getAtoms()) {

				// add atom names to list
				hitAtomNames.add(hA.getName());
			}
		}

		// unify atom names
		Set<String> uniqueAtomNames = new LinkedHashSet<>(hitAtomNames);

		return uniqueAtomNames;
	}

	/**
	 * tests if all amino acid of a hit are located in the same chain
	 * 
	 * @param hit
	 * @return true if intra-molecular hit (same chain)
	 */
	public static boolean isSameChain(Hit hit) {

		Set<Character> chainIds = new HashSet<>();
		for (HitAminoAcid hAa : hit.getAminoAcids()) {

			chainIds.add(hAa.getChainId());
		}

		if (chainIds.size() > 1) {

			return false;
		}

		return true;
	}

	/**
	 * tests if atom name is defined by ParameterProvider
	 * 
	 * @param atomName
	 * @return true if atom type is valid
	 */
	public static boolean isValidAtomType(String atomName) {

		if (Arrays.asList(ParameterProvider.getInstance().getAtoms())
				.contains(InputOutputUtils.toFullAtomName(atomName))) {

			return true;
		}

		return false;
	}

	/**
	 * gets a BioJava atom representation of HitAtom
	 * 
	 * @param hA
	 * @return BioJava atoms
	 */
	public static Atom toBioJavaAtom(HitAtom hA) {

		Atom a = new AtomImpl();
		a.setName(hA.getName());
		a.setFullName(InputOutputUtils.toFullAtomName(hA.getName()));
		a.setX(hA.getX());
		a.setY(hA.getY());
		a.setZ(hA.getZ());

		return a;
	}
}
