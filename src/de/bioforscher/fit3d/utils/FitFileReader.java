/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
*/

package de.bioforscher.fit3d.utils;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.bioforscher.fit3d.Starter;
import de.bioforscher.fit3d.model.Hit;
import de.bioforscher.fit3d.model.HitAminoAcid;

public class FitFileReader {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws FileNotFoundException,
			IOException, ClassNotFoundException {

		List<Hit> totalResults = new ArrayList<>();

		for (String s : args) {

			System.err.println("reading " + s);

			ObjectInputStream ois = new ObjectInputStream(
					new FileInputStream(s));

			Object o = ois.readObject();
			ois.close();

			if (o instanceof List<?>) {

				// List<Hit> hits = (List<Hit>) o;
				// for (Hit h : hits) {
				//
				// h.setTitle("");
				// }
				totalResults.addAll((List<Hit>) o);
			}
		}

		System.err.println("sorting");
		Collections.sort(totalResults);

		System.err.println("printing");
		printToFile(totalResults);
	}

	/**
	 * gets header of result line
	 * 
	 * @param separator
	 * @return header
	 */
	private static String getHeaderLine(String separator) {

		StringBuilder header = new StringBuilder();
		header.append("RMSD");
		header.append(separator);
		header.append("PDB-ID");
		header.append(separator);
		header.append("occ");
		header.append(separator);
		header.append("seq");
		header.append(separator);
		header.append("motif");
		header.append(separator);
		header.append("title");

		return header.toString();
	}

	/**
	 * gets result line with specified separator
	 * 
	 * @param hit
	 * @return result line
	 */
	private static String getResultLine(Hit hit, String separator) {

		final NumberFormat nfRmsd = NumberFormat
				.getInstance(Starter.LOCALIZATION);
		final DecimalFormat dfRmsd = (DecimalFormat) nfRmsd;
		dfRmsd.applyPattern("0.0000");

		final NumberFormat nfPvalue = NumberFormat
				.getInstance(Starter.LOCALIZATION);
		final DecimalFormat dfPvalue = (DecimalFormat) nfPvalue;
		dfPvalue.applyPattern("0.000000000000E0");

		StringBuilder resultLine = new StringBuilder();
		resultLine.append(dfRmsd.format(hit.getRmsd()));
		resultLine.append(separator);
		resultLine.append(hit.getPdbId());
		resultLine.append(separator);

		if (StructureUtils.isSameChain(hit)) {

			resultLine.append("intra");
		} else {

			resultLine.append("inter");
		}

		resultLine.append(separator);
		resultLine.append(hit.getAlignedSeq());
		resultLine.append(separator);

		// TODO sort amino acids of hit?
		// sort amino acids for output
		// if (this.sortHits) {
		//
		// Collections.sort(hit.getAminoAcids());
		// }

		for (HitAminoAcid hAa : hit.getAminoAcids()) {

			resultLine.append(hAa);
			resultLine.append(" ");
		}

		resultLine.setLength(resultLine.length() - 1);

		resultLine.append(separator);
		resultLine.append("\"");
		if (hit.getTitle() != null) {
			resultLine.append(hit.getTitle());
		} else {

			resultLine.append("?");
		}
		resultLine.append("\"");

		return resultLine.toString();
	}

	/**
	 * prints all results to result file
	 * 
	 * @throws IOException
	 */
	private static void printToFile(List<Hit> totalResults) throws IOException {

		BufferedWriter out = new BufferedWriter(new FileWriter(
				"FitFileReader_out.csv"));

		out.write(getHeaderLine(","));
		out.newLine();

		for (Hit hit : totalResults) {

			out.write(getResultLine(hit, ","));
			out.newLine();
		}

		out.close();
	}

}
