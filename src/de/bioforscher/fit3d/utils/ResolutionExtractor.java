/*
Copyright 2013-2014 Florian Kaiser
bioinformatics group Mittweida

This file is part of Fit3D.

Fit3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Fit3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fit3D. If not, see <http://www.gnu.org/licenses/>.
 */

package de.bioforscher.fit3d.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.StructureException;
import org.biojava.bio.structure.io.PDBFileReader;

import de.bioforscher.fit3d.io.parser.InputListParser;

public class ResolutionExtractor {

	public static void main(String[] args) throws StructureException,
			NumberFormatException, FileNotFoundException, ParseException,
			IOException {

		String structureListPath = args[0];
		String outputPathRes = args[1];
		String outputPathMethod = args[2];

		InputListParser lp = new InputListParser(new File(structureListPath));

		try {

			List<String> structureList = lp.readInput();

			List<String> resolutionList = new ArrayList<>();
			List<String> methodList = new ArrayList<>();

			for (String pdbId : structureList) {

				System.out.println(pdbId);

				try {
					PDBFileReader pfr = new PDBFileReader();
					pfr.setPath("/opt/pdb/");
					// pfr.setAutoFetch(true);
					pfr.setPdbDirectorySplit(true);

					Structure s = pfr.getStructureById(pdbId.toLowerCase());

					System.out.println(s.getPDBHeader());
					float res = s.getPDBHeader().getResolution();
					if (res != 99.0) {

						resolutionList.add(String.valueOf(res));
					}

					methodList.add(s.getPDBHeader().getTechnique());
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			System.out.println("writing results");
			FileUtils.writeLines(new File(outputPathRes), resolutionList);
			FileUtils.writeLines(new File(outputPathMethod), methodList);

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
