#!/bin/bash

for f in 2014-05-27_CSA_motifs/*
	do java -jar ../Fit3D.jar -m $f -l 2014-11-12_modbase_dengue_virus/dengue_virus_proteins.txt -f results/$f.csv -o result_structures/$f/ -g
done
